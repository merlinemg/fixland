# coding: utf-8
from decimal import Decimal
from django.db import models
from django.utils.translation import ugettext_lazy as _
from . import utils


class Partner(models.Model):
    user = models.OneToOneField('accounts.User', related_name='partner')
    registration_comment = models.TextField(_('Комментарий'), help_text=_('Комментарий который был введен при регистрации'), blank=True, null=True)
    partner_code = models.CharField(_('Код партнера'), max_length=255, default=utils.generate_partner_code)
    commission = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal(0), verbose_name=_('Комиссия партнера'))

    class Meta:
        verbose_name = _('Партнер')
        verbose_name_plural = _('Партнеры')

    def __str__(self):
        return self.user.get_full_name()

    def get_balance(self):
        if getattr(self, '_balance', None) is None:
            self._balance = self.user.recipient_transactions.aggregate(amount=models.Sum('amount')).get('amount')
        return self._balance or Decimal('0')
