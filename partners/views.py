from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from partners.decorators import partner_required


@login_required
@partner_required
def requisites(request):
    return render(request, 'partners/requisites.html')
