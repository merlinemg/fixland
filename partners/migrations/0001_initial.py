# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('registration_comment', models.TextField(null=True, help_text='Комментарий который был введен при регистрации', blank=True, verbose_name='Комментарий')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='partner')),
                ('commission', models.DecimalField(decimal_places=2, default=Decimal('0'), verbose_name='Комиссия партнера',
                                      max_digits=20)),
            ],
        ),

    ]

