# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import partners.utils
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0003_auto_20150918_1630'),
    ]

    operations = [

        migrations.AlterField(
            model_name='partner',
            name='partner_code',
            field=models.CharField(default=partners.utils.generate_partner_code, max_length=255, verbose_name='Код партнера'),
        ),
    ]
