from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns(
    'partners.views',
    url(r'^$', TemplateView.as_view(template_name='partners/index.html'), name='partner'),
    url(r'^balance/requisites/$', 'requisites', name='partner_requisites'),
)
