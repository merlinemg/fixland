from django.conf import settings
from django.conf.urls import include, url, patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter
from solid_i18n.urls import solid_i18n_patterns

from accounts.api import views as accounts_api
from core.api.views import TransactionViewSet
from masters.api import views as masters_api
from partners.api import views as partners_api
from devices.api import views as devices_api
from orders.api import views as orders_api

router = DefaultRouter()
router.register(r'users', accounts_api.UserViewSet)
router.register(r'regions', accounts_api.RegionViewSet)
router.register(r'cities', accounts_api.CityViewSet)
router.register(r'metro-stations', accounts_api.MetroStationViewSet)

router.register(r'masters', masters_api.MasterViewSet)
router.register(r'master-courses', masters_api.CourseLessonEventView)
router.register(r'master-can-work', masters_api.MasterCanWorkViewSet)
router.register(r'master-course-event', masters_api.MasterCoursesView)
router.register(r'master-places', masters_api.MasterPlaceViewSet)
router.register(r'master-parts', masters_api.MasterPartViewSet)
router.register(r'master-reserved-parts', masters_api.PartOrderItemViewSet)

router.register(r'partners', partners_api.PartnerViewSet)

router.register(r'parts', devices_api.PartViewSet)
router.register(r'vendor-parts', devices_api.VendorPartViewSet)

router.register(r'repair-orders', orders_api.RepairOrdersView)
router.register(r'repair-order-users', orders_api.RepairOrderUserView)
router.register(r'complete-repair-order', orders_api.CompleteRepairOrderView)

router.register(r'transactions', TransactionViewSet)

js_info_dict = {
    'packages': ('fixland.static.js.dist',)
}

urlpatterns = patterns(
    '',
    # url(r'^jsi8n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
    (r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    (r'^sitemap\.xml$', TemplateView.as_view(template_name='sitemap.xml', content_type='application/xml')),
    url(r'^soap1s-callback/$', 'integrations.views.soap1s_callback', name='soap1s_callback'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^googleae16e7ad9c1f191e\.html$', TemplateView.as_view(template_name='misc/googleae16e7ad9c1f191e.html')),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/reset-password/$', accounts_api.ResetPasswordView.as_view()),
    url(r'^api/v1/reject-repair-order/(?P<order_id>\d+\-\d+)/$', 'orders.views.reject_repair_order'),
    url(r'^api/v1/parts-order/$', 'masters.views.parts_order'),
    url(r'^api/v1/update-parts-availability/$', devices_api.UpdatePartsAvailability.as_view()),
    url(r'^api/v1/check-update-parts-finish/$', devices_api.UpdatePartsStatus.as_view()),
    url(r'^api/v1/not-finished-order/$', orders_api.not_finished_order),
    url(r'^login_api/$', 'accounts.views.login', name='login_api'),
    url(r'^verify_sms_code/(?P<code>\w+)/$',
        'accounts.views.verify_sms_code', name='activate_account_sms'),
    url(r'^send_new_sms_code/(?P<user_id>\w+)/$',
        'accounts.views.send_new_sms_code', name='send_new_sms_code'),
    url(r'^change_avatar/$', 'accounts.views.change_avatar', name='change_avatar'),
    url(r'^create_feedback/$', 'fixland.views.create_feedback', name='create_feedback'),
    url(r'^register_master/$', 'accounts.views.register_master', name='register_master'),
    url(r'^register_partner/$', 'accounts.views.register_partner', name='register_partner'),
    url(r'^orders/', include('orders.urls')),
    url(r'^payment_notify/$', 'core.views.payment_notify', name='payment_notify'),
    url(r'^select-repair-order/(?P<order_id>\d+\-\d+)/$', 'orders.views.select_repair_order', name='select_repair_order'),
    url(r'^part-charge-off/(?P<part>\d+)/(?P<qty>\d+)/$', 'masters.views.part_charge_off', name='part_charge_off'),
    url(r'^offer/$', 'core.views.offer', name='offer'),
    url(r'^set-lang/$', 'fixland.views.set_language', name='set_lang'),
    (r'^tinymce/', include('tinymce.urls')),
    url(r'^import_data_from_onedrive', 'core.views.import_data_from_onedrive', name='import_data_from_onedrive'),
    url(r'^import_parts_from_remote_excel_file', 'core.views.import_parts_from_remote_excel_file', name='import_parts_from_remote_excel_file'),
    url(r'^import_parts_from_remote_xml_file', 'core.views.import_parts_from_remote_xml_file', name='import_parts_from_remote_xml_file'),
    url(r'^recalculate_masters_rating', 'core.views.recalculate_masters_rating', name='recalculate_masters_rating'),
    url(r'^fix_masters_rating', 'core.views.fix_masters_rating', name='fix_masters_rating'),
    url(r'^create_order_review', 'orders.views.create_order_review', name='create_order_review'),
    url(r'^notify_masters_about_new_order/(?P<repair_order_id>\d+)/$', 'core.views.notify_masters_about_new_order', name='notify_masters_about_new_order'),
    url(r'^import_metro_stations', 'core.views.import_metro_stations', name='import_metro_stations'),
    url(r'^import_vendor_articles_from_onedrive', 'core.views.import_vendor_articles_from_onedrive', name='import_vendor_articles_from_onedrive'),
    url(r'^cancel_parts_order/$', 'masters.views.cancel_parts_order', name='cancel_parts_order'),
    url(r'^cancel_parts_order_item/$', 'masters.views.cancel_parts_order_item', name='cancel_parts_order_item'),
)

urlpatterns += solid_i18n_patterns(
    '',
    url(r'^$', 'fixland.views.index', name='index'),
    url(r'^login/$', 'accounts.views.login_page', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^reset-password/$', 'accounts.views.reset_password', name='reset'),
    url(r'^repair/$', 'fixland.views.repair', name='repair'),
    url(r'^repeate_repair/$', 'orders.views.repeate_repair', name='repeate_repair'),
    url(r'^sell/$', 'fixland.views.sell', name='sell'),
    url(r'^warranty/$', 'fixland.views.warranty', name='warranty'),
    url(r'^master/', include('masters.urls')),
    url(r'^partner/', include('partners.urls')),
    url(r'^about/$', 'fixland.views.about', name='about'),
    url(r'^master_signup/$', 'accounts.views.master_signup', name='master_signup'),
    url(r'^partner_signup/$', 'accounts.views.partner_signup', name='partner_signup'),
    url(r'^verify_email/(?P<code>\w+)/$',
        'accounts.views.verify_email', name='activate_account'),
    url(r'^feedback/$', 'fixland.views.feedback', name='feedback'),
    url(r'^or/(?P<code>\w+)/$', 'orders.views.order_review', name='order_review'),
    url(r'^order_review/(?P<code>\w+)/$', 'orders.views.order_review', name='order_review_old'),
)

admin.autodiscover()

urlpatterns += solid_i18n_patterns(
    '',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^support/management/', include(admin.site.urls)),
    url(r"^support/management/login/user/(?P<user_id>.+)/$", "loginas.views.user_login", name="loginas-user-login"),
    url(r'^report_builder/', include('report_builder.urls')),
    url(r'^support/management/rosetta/', include('rosetta.urls')),
    url(r'^explorer/(?P<query_id>\d+)/download$', 'core.explorer.views.download_query', name='query_download'),
    url(r'^explorer/(?P<query_id>\d+)/csv', 'core.explorer.views.view_csv_query', name='query_csv'),
    url(r'^explorer/', include('explorer.urls')),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
