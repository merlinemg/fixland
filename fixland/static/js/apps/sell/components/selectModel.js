import React from 'react';
import actions from '../actions';
import store from '../store';
import Reflux from 'reflux';

import SelectModelMixin from '../../common/components/selectModelMixin';

var SelectModel = React.createClass({
    actions: actions,
    nextRoute: 'selectMemory',
    mixins: [Reflux.connect(store), SelectModelMixin]
});

module.exports = SelectModel;
