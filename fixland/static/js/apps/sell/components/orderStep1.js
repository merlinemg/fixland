import actions from '../actions';
import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import TimePicker from './timePicker';

import OrderStep1Mixin from '../../common/components/orderStep1Mixin';

module.exports = React.createClass({
    TimePicker: TimePicker,
    actions: actions,
    mixins: [Reflux.connect(store), OrderStep1Mixin]
});
