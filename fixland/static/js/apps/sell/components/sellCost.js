/* globals gettext */

import _ from 'underscore';
import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import actions from '../actions';
import DevicePhoto from '../../common/components/devicePhoto.js';

var Link = Router.Link;

var SellCost = React.createClass({
    mixins: [Reflux.connect(store)],
    getInitialState() {
        var params = this.props.params;
        var device = _.findWhere(window.DEVICES, {slug: params.device}),
            model = _.findWhere(device.models, {slug: params.model}),
            memory = _.findWhere(model.memory_list, {slug: params.memory}),
            status = _.findWhere(window.STATUSES, {slug: params.status});
        return {
            device: device,
            model: model,
            memory: memory,
            status: status
        };
    },
    componentWillMount() {
        actions.setModelSell(this.getModelSell());
    },
    getModelSell() {
        var filter = {
            memory: this.state.memory.slug,
            status: this.state.status.slug
        };
        if (this.props.params.version !== '-') {
            filter.version = this.props.params.version;
        }
        return _.findWhere(this.state.model.sell, filter);
    },
    getTotal() {
        return Math.round(this.getModelSell().price).toLocaleString().toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
    },
    render() {
        return (
            <div className="content step">
                <div>
                    <h3>{gettext('Цена продажи')}</h3>

                    <div className='one-half thumb-left devPhotoWrp'>
                        <DevicePhoto photo={this.state.model.model_photo} />
                    </div>
                    <div className='two-half last-column aboutClient'>
                        <h4 className="fullDeviceName">{this.state.device.name} {this.state.model.name} {this.state.memory.name}</h4>
                    </div>
                    <div className="wrapperUl">
                        <p className="totalAmount sellTotAmount"> {this.getTotal()} <span className="rub"> ₽ </span></p>
                    </div>
                </div>
                <Link to='orderStep1' className="coverpage-button callMaster">{gettext('Вызвать курьера')}</Link>
            </div>
        );
    }
});

module.exports = SellCost;
