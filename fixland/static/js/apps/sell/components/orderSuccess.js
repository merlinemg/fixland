/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';


var OrderStep3 = React.createClass({
    mixins: [Reflux.connect(store)],
    render() {
        return (
            <div className="content step">
                <h3>{gettext('Спасибо!')}</h3>
                <h4>{gettext('Наш курьер свяжется с Вами по указанному номера телефона в течение 20 минут в рабочее время')}</h4>
            </div>
        );
    }
});

module.exports = OrderStep3;
