/* globals gettext*/

import _ from 'underscore';
import React from 'react';
import Router from 'react-router';

var Link = Router.Link;

var SelectVersion = React.createClass({
    componentWillMount() {
        var modelMemoryVersions = this.getModelMemoryVersions();
        if (!modelMemoryVersions.length) {
            location.hash = '#/' + [this.props.params.device, this.props.params.model, this.props.params.memory, '-'].join('/');
        }
    },
    getInitialState() {
        var params = this.props.params;
        var modelVersions = this.getModelMemoryVersions();
        var selectedMovelVersions = _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).versions;
        return {
            versions: _.filter(selectedMovelVersions, (version) => {
                return modelVersions.indexOf(version.slug) !== -1;
            })
        };
    },
    getModelMemoryVersions() {
        var params = this.props.params;
        var modelsSell = _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).sell;
        return _.pluck(_.filter(modelsSell, modelSell => {
            return !!modelSell.version && modelSell.memory === params.memory;
        }), 'version');
    },
    getParams(version) {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            memory: this.props.params.memory,
            version: version.slug
        };
    },
    render() {
        return (
            <div className="content step">
                <h3>{gettext('Укажите версию')}</h3>
                <div>
                    {this.state.versions.map(version => {
                        return (
                            <Link to='selectStatus' params={this.getParams(version)} className="deviceModel selectBtn">
                                {version.name}
                            </Link>
                        );
                    })}
                </div>
            </div>
        );
    }
});

module.exports = SelectVersion;
