import React from 'react';
import ReactRouter from 'react-router';
import SelectDevice from './components/selectDevice';
import OrderStep1 from './components/orderStep1';
import OrderStep2 from './components/orderStep2';
import OrderStep3 from './components/orderStep3';
import OrderSuccess from './components/orderSuccess';
import SelectModel from './components/selectModel';
import SelectModelHelp from './components/selectModelHelp';
import SelectMemory from './components/selectMemory';
import SelectVersion from './components/selectVersion';
import SelectStatus from './components/selectStatus';
import SellCost from './components/sellCost';
import OrderServerProblem from '../common/components/orderServerProblem';

var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var sellRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='selectDevice' path='/' handler={SelectDevice}/>
        <ReactRouter.Route name='orderStep1' path='/order' handler={OrderStep1}/>
        <ReactRouter.Route name='orderStep2' path='/order/step2' handler={OrderStep2}/>
        <ReactRouter.Route name='orderStep3' path='/order/step3' handler={OrderStep3}/>
        <ReactRouter.Route name='orderSucess' path='/order/success' handler={OrderSuccess}/>
        <ReactRouter.Route name='orderServerProblem' path='/order/server-problem' handler={OrderServerProblem}/>
        <ReactRouter.Route name='selectModel' path='/:device' handler={SelectModel}/>
        <ReactRouter.Route name='selectModelHelp' path='/:device/modelhelp' handler={SelectModelHelp}/>
        <ReactRouter.Route name='selectMemory' path='/:device/:model' handler={SelectMemory}/>
        <ReactRouter.Route name='selectVersion' path='/:device/:model/:memory' handler={SelectVersion}/>
        <ReactRouter.Route name='selectStatus' path='/:device/:model/:memory/:version' handler={SelectStatus}/>
        <ReactRouter.Route name='statusDescription' path='/:device/:model/:memory/:version/:status' handler={SelectStatus}/>
        <ReactRouter.Route name='sellCost' path='/:device/:model/:memory/:version/:status/cost' handler={SellCost}/>
    </ReactRouter.Route>
);

module.exports = sellRoutes;
