import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getUserGeoLocation',
    'updateUserAddress',
    'setAsSoonAsPossible',
    'setOwnPhoneNumber',
    'setFriendPhoneNumber',
    'setUserName',
    'setUserPhone',
    'setUserEmail',
    'placeOrder',
    'cleanOrder',
    'setStartDateTime',
    'setEndDateTime',
    'setShowAllDevices',
    'setLastInputAddress',
    'setParsedAddress',
    'setParsedName',
    'setModelSell',
    'setModelCache',
    'setStatusCache'
]);

module.exports = actions;
