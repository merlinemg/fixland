import Reflux from 'reflux';
import utils from '../../../common/utils';


let actions = Reflux.createActions({
    init: {children: ['completed', 'failed']},
});

let loadPartner = () => {
    return utils.ajaxGet({
        url: '/api/v1/partners/me/',
    });
};

actions.init.listen(() => {
    loadPartner()
    .then(actions.init.completed)
    .fail(actions.init.failed);
});

module.exports = actions;