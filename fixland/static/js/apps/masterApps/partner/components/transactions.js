/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import Transactions from '../../../transactions/components/transactionsTable';


module.exports = React.createClass({
    render() {
        return <Transactions showOrderLink={false} title={gettext('Мои вознаграждения')}/>
    }
});
