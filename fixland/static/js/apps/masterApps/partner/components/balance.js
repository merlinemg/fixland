/* globals gettext */

import React from 'react';
import { Link } from 'react-router';
import actions from '../actions/partnerActions';
import Reflux from 'reflux';
import store from '../stores/partnerStore';
import Loading from '../../../common/components/loading';
import Money from '../../../common/components/money';


module.exports = React.createClass({
    mixins: [Reflux.connect(store, 'partner')],
    componentDidMount() {
        actions.init();
    },
    render() {
        let partner = this.state.partner
        if (partner) {
            return (
                <div>
                    <h3>{gettext('Мои финансы')}</h3>
                    <div>
                        <h5>{gettext('Мой баланс: ')}
                            <Link to='transactions' className="myFinancesBalanceLink">
                                <Money amount={partner.balance}/>
                            </Link>
                        </h5>
                    </div>
                    <a className="button button-blue center-button selectRepair" href="balance/requisites">{gettext('Ввести реквизиты')}</a><br/>
                    <Link to="withdrawMoney" className="button button-green selectRepair">{gettext('Вывести')}</Link>
                </div>
            );
        } else {
            return <Loading />
        }
    }
});
