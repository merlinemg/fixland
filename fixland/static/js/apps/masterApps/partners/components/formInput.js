import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Router from 'react-router';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';

var FormInput = React.createClass({
     getDefaultProps(){
         return {
         type: 'text'
        };
     },
    render() {
        return (
             <div className="wrpInptError identError">
                <input type={this.props.type} onBlur={this.props.onBlur} ref={this.props.inputRef} placeholder={this.props.placeholder} id = {this.props.id} className="inputData" />
                {this.props.error ? <ErrorMessage message={this.props.error} /> : ''}
             </div>
        );
    }

});

module.exports = FormInput;