
import React from 'react';
import {naturalPersonStore} from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Router from 'react-router';
import Loading from '../../../common/components/loading.js';
import FormInput from '../components/formInput.js'

var Link = React.Link;

let store = naturalPersonStore;

var NaturalPersonRequisites = React.createClass({
    mixins: [Reflux.connect(store, 'store')],

    saveCheckboxValueToStore(name, event){
        actions.setFormFieldValue(name, event.target.checked);
    },
    handleInputBlur(name, event) {
        actions.setFormFieldValue(name, event.target.value)
    },
    submit(){
        actions.submit();
    },
    render() {
        let {firstName, errors} = this.state.store;
        if (!errors) errors = {};
        let form = this.state.form;
        return (
            <div className='content step wrpMasSign'>
                <h3>{gettext('Ввод реквизитов для физического лица')}</h3>
                <div className='wrapperInputAddr'>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'lastName')} placeholder='Фамилия' id='lastName' value={store.lastName} error={errors.lastName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'firstName')} placeholder='Имя' id='firstName' value={store.firstName} error={errors.firstName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'middleName')} placeholder='Отчество' id='middleName' value={store.middleName} error={errors.middleName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'email')} placeholder='E-mail' id='email' value={store.email} error={errors.email}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'phoneNumber')} placeholder='Мобильный телефон' ref='phoneComponent' inputRef='phoneNumber' id='phoneNumber' value={store.phoneNumber} error={errors.phoneNumber}/>
                    <h4>{gettext('Паспортные данные')}</h4>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'passportSeries')} placeholder='Серия' id='passportSeries' value={store.passportSeries} error={errors.passportSeries}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'passportNumber')} placeholder='Номер' id='passportNumber' value={store.passportNumber} error={errors.passportNumber}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'issuedBy')} placeholder='Кем выдан' id='issuedBy' value={store.issuedBy} error={errors.issuedBy}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'dateOfIssue')} placeholder='Дата выдачи (дд/мм/гггг)' inputRef='dateOfIssue' ref='dateOfIssueComponent' id='dateOfIssue' value={store.dateOfIssue} error={errors.dateOfIssue}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'pensionCertificateNumber')} placeholder='номер пенсионного свидетельства' id='pensionCertificateNumber' value={store.pensionCertificateNumber} error={errors.pensionCertificateNumber}/>
                    <h4>{gettext('Адрес регистрации')}</h4>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationCountry')} placeholder='Страна' id='registrationCountry' value={store.registrationCountry} error={errors.registrationCountry}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationRegion')} placeholder='Регион' id='registrationRegion' value={store.registrationRegion} error={errors.registrationRegion}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationCity')} placeholder='Город' id='registrationCity' value={store.registrationCity} error={errors.registrationCity}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationZipCode')} placeholder='Индекс' id='registrationZipCode' value={store.registrationZipCode} error={errors.registrationZipCode}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationStreet')} placeholder='Улица' id='registrationStreet' value={store.registrationStreet} error={errors.registrationStreet}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationHouse')} placeholder='Дом' id='registrationHouse' value={store.registrationHouse} error={errors.registrationHouse}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'registrationApartment')} placeholder='Квартира' id='registrationApartment' value={store.registrationApartment} error={errors.registrationApartment}/>
                    <h4>{gettext('Адрес проживания')}</h4>
                    <span><input type="checkbox" onBlur={this.saveCheckboxValueToStore.bind(this, 'isTheSameAddress')}  id='isTheSameAddress' checked={store.isTheSameAddress}/> {gettext('Адрес проживания совпадает с адресом регистрации')}</span>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceCity')} placeholder='Город' id='residenceCity' value={store.residenceCity} error={errors.residenceCity}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceCountry')} placeholder='Страна' id='residenceCountry' value={store.residenceCountry} error={errors.residenceCountry}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceRegion')} placeholder='Регион' id='rresidenceRegion' value={store.residenceRegion} error={errors.residenceRegion}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceCity')} placeholder='Город' id='residenceCity' value={store.residenceCity} error={errors.residenceCity}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceZipCode')} placeholder='Индекс' id='residenceZipCode' value={store.residenceZipCode} error={errors.residenceZipCode}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceStreet')} placeholder='Улица' id='residenceStreet' value={store.residenceStreet} error={errors.residenceStreet}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceHouse')} placeholder='Дом' id='residenceHouse' value={store.residenceHouse} error={errors.residenceHouse}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'residenceApartment')} placeholder='Квартира' id='residenceApartment' value={store.residenceApartment} error={errors.residenceApartment}/>
                    <button className="button button-green center-button selectRepair" onClick={this.submit}>{gettext('Сохранить')}</button>
                </div>
            </div>
        );
    },
    componentDidMount() {
        jQuery(this.refs.phoneComponent.refs.phoneNumber.getDOMNode()).inputmask({
            mask: '+7(999) 999-9999'
        });
        jQuery(this.refs.dateOfIssueComponent.refs.dateOfIssue.getDOMNode()).inputmask({
            mask:"99/99/9999",
        });
    }
});

module.exports = NaturalPersonRequisites;