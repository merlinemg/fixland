import React from 'react';
import ReactRouter from 'react-router';
import LegalPersonRequisitesForm from './components/legalPersonRequisitesForm.js';
import NaturalPersonRequisites from './components/naturalPersonRequisites.js';
import PersonChoiceButtons from './components/personChoiceButtons.js';
import OrderServerProblem from '../../common/components/orderServerProblem';


var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var partnerRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='person_choice' path='/' handler={PersonChoiceButtons}/>
        <ReactRouter.Route name='naturalPersonRequisites' path='/natural_person_requisites' handler={NaturalPersonRequisites}/>
        <ReactRouter.Route name='legalPersonRequisites' path='/legal_person_requisites' handler={LegalPersonRequisitesForm}/>
        <ReactRouter.Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </ReactRouter.Route>
);

module.exports = partnerRoutes;
