import Reflux from 'reflux';

var actions = Reflux.createActions([
    'setFormFieldValue',
    'getInitialState',
    'cleanForm',
    'validate',
    'submit'
]);

module.exports = actions;
