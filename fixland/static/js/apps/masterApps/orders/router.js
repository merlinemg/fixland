import React from 'react';
import ReactRouter from 'react-router';
import Orders from './components/orders.js';
import DoneOrders from './components/doneOrders.js';
import OrderDetails from './components/orderDetails.js';
import RejectOrder from './components/rejectOrder.js';
import OrderServerProblem from '../../common/components/orderServerProblem';

var {Route, RouteHandler} = ReactRouter;


var App = React.createClass({

    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },

    render: function() {
        return (
            <RouteHandler />
        );
    }
});

var masterOrdersRoutes = (
    <Route name='app' handler={App}>
        <Route name='orders' path='/' handler={Orders} />
        <Route name='doneOrders' path='/done' handler={DoneOrders} />
        <Route name='orderDetails' path='/order/:orderId' handler={OrderDetails} />
        <Route name='rejectOrder' path='/order/:orderId/reject' handler={RejectOrder} />
        <Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </Route>
);

module.exports = masterOrdersRoutes;
