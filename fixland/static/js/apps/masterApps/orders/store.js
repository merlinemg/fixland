import Reflux from 'reflux';
import moment from 'moment';
import _ from 'underscore';
import Storage from '../../common/storage';

import actions from './actions';
import Utils from '../../common/utils';

var storage = new Storage();


var store = Reflux.createStore({
    listenables: [actions],
    storage: storage,
    store: null,

    getInitialState() {
        this.store = this.storage.getItem('masterOrdersStore');
        if (!this.store) {
            this.store = {
                lastUpdated: {}
            }
        }
        if (!this.store.lastUpdated) {
            this.store.lastUpdated = {};
        }
        return this.store;
    },

    setStoreItem(key, value, trigger=true) {
        this.store[key] = value;
        this.storage.setItem('masterOrdersStore', this.store);
        if (trigger) this.trigger(this.store);
    },

    onGetMasterOrder(orderId) {
        Utils.ajaxGet({
            url: '/api/v1/repair-orders/' + orderId + '/'
        }).then((response) => {
            this.getMasterOrderUser(response.need_update_vendor_parts, orderId);
            this.trigger({success: true, order: response});
        }).fail(() => {
            this.trigger({permissionDenied: true});
        });
    },

    getOrders(data) {
        Utils.ajaxGet({
            url: '/api/v1/repair-orders',
            data: data
        }).then((response) => {
            this.trigger({success: true, orders: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetMasterOrders() {
        let data = {
            status: 'new',
            ordering: 'starttime'
        };
        this.getOrders(data);
    },

    onGetMasterDoneOrders() {
        let data = {
            status: 'done'
        };
        this.getOrders(data);
    },

    onCompleteOrder(orderId) {
        Utils.ajaxPut({url: '/api/v1/complete-repair-order/' + orderId + '/'})
        .then((response) => {
            this.trigger({success: true, order: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onRejectOrder(orderId, comment) {
        Utils.ajaxPost({
            url: '/api/v1/reject-repair-order/' + orderId + '/',
            data: {comment: comment}
        }).then(() => {
            this.trigger({
                ajaxRequest: false,
                rejected: true
            });
        }).fail(() => {
            this.trigger({rejected: false});
        });
    },

    checkIfUpdatedAndGetMasterOrderUser() {
        if (!this.store.updatePartsAvailabilityId) {
            return
        }
        Utils.ajaxGet({
            url:'/api/v1/check-update-parts-finish',
            data: {
                taskId: this.store.updatePartsAvailabilityId
            },
            decamelize: true
        }).then((response) => {
            if (response.result === 'success') {
                this.setStoreItem('updatePartsAvailabilityId', null, false);
                this.getMasterOrderUser();
            } else if (response.result === 'failed') {
                setTimeout(this.updatePartsAvailability, 1000);
            } else {
                setTimeout(this.checkIfUpdatedAndGetMasterOrderUser, 1000);
            }
        });
    },

    updatePartsAvailability() {
        let lastUpdated = this.store.lastUpdated[this.orderNumber]
        if (!lastUpdated) {
            this.store.lastUpdated[this.orderNumber] = moment();
            this.setStoreItem('lastUpdated', this.store.lastUpdated);
        } else {
            if (moment.duration(moment().diff(lastUpdated)).minutes() < 10) {
                this.getMasterOrderUser();
                return
            } else {
                this.store.lastUpdated[this.orderNumber] = moment();
                this.setStoreItem('lastUpdated', this.store.lastUpdated);
            }
        }
        Utils.ajaxGet({
            url:'/api/v1/update-parts-availability',
            data: {
                forOrder: this.orderNumber,
            },
            decamelize: true
        }).then((response) => {
            if (response.result && response.task_id) {
                this.setStoreItem('updatePartsAvailabilityId', response.task_id, false);
                this.checkIfUpdatedAndGetMasterOrderUser();
            }
        });
    },

    getMasterOrderUser(update, orderNumber) {
        if (orderNumber) this.orderNumber = orderNumber;
        if (update) {
            this.updatePartsAvailability();
        } else {
            Utils.ajaxGet({
                url: '/api/v1/repair-order-users/' + this.orderNumber + '/',
            }).then((response) => {
                this.trigger({
                    userLoaded: true,
                    ...response
                });
            }).fail(() => {
                this.trigger({userLoaded: true});
            });
        }
    },

    cleanUpdatePartsAvailabilityId() {
        this.setStoreItem('updatePartsAvailabilityId', null, false);
    }
});

module.exports = store;
