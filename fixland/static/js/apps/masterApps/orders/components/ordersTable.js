/* globals gettext */

import React from 'react';
import Router from 'react-router';
import OrderTime from '../../common/components/orderTime';
import OrderAddress from '../../common/components/orderAddress';

var Link = Router.Link;

var OrderTableRow = React.createClass({
    numberWithApostrophe: function() {
        return this.props.order.total_amount_with_discount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1\'');
    },
    render() {
        return (
            <tr>
                <td className='table-sub-title'><OrderTime starttime={this.props.order.starttime} endtime={this.props.order.endtime} asSoonAsPossible={this.props.order.as_soon_as_possible} createdAt={this.props.order.created_at} /></td>
                <td className='linkTd'>
                    <Link to='orderDetails' params={{orderId: this.props.order.number}}>{this.props.order.order_name}</Link>
                </td>
                <td><OrderAddress metro={this.props.order.metro} /></td>
                <td>{this.numberWithApostrophe()} <span className='rub'> ₽ </span></td>
            </tr>
        );
    }
});

var Table = React.createClass({
    render() {
        return (
            <div>
                { this.props.orders.length ? <table cellSpacing='0' className='table'>
                    <thead></thead>
                    <tbody>
                        {this.props.orders.map((order) => {
                            return <OrderTableRow order={order}/>;
                        })}
                    </tbody>
                </table> : <div><span className="whiteColor">{ this.props.emptyDataText || gettext('Нет данных')}</span></div>}
            </div>
        );
    }
});


module.exports = Table;
