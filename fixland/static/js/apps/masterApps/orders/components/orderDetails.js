import React from 'react';
import actions from '../actions';
import store from '../store';
import Reflux from 'reflux';
import OrderDetailsMixin from '../../common/components/orderDetailsMixin';

var OrderDetails = React.createClass({
    mixins: [Reflux.connect(store), OrderDetailsMixin],

    componentWillMount() {
        actions.getMasterOrder(this.props.params.orderId);
    },

    componentWillUnmount() {
        actions.cleanUpdatePartsAvailabilityId();
    },

    completeOrder() {
        actions.completeOrder(this.state.order.number);
    },
});

module.exports = OrderDetails;
