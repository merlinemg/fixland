/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import store from '../store';

import actions from '../actions';

module.exports = React.createClass({
    mixins: [Reflux.listenTo(store, 'storeTrigger'), Router.Navigation],
    getInitialState() {
        return {
            showError: false,
            ajaxRequest: false,
            comment: null,
            rejected: null
        };
    },
    storeTrigger(data) {
        if (this.state.rejected === null && data.rejected === true) {
            this.transitionTo('orders');
        }
        if (this.state.rejected === null && data.rejected === false) {
            this.transitionTo('serverProblem');
        }
    },
    validate() {
        return this.state.comment;
    },
    handleCommentChange(event) {
        this.setState({comment: event.target.value});
    },
    rejectOrder() {
        if (this.validate()) {
            this.setState({ajaxRequest: true});
            actions.rejectOrder(this.props.params.orderId, this.state.comment);
        } else {
            this.setState({showError: true});
        }
    },
    render() {
        return (
            <div className='content step'>
                <h3>{gettext('Отказаться от заказа')}</h3>
                {this.state.showError && !this.validate() && <ErrorMessage message={gettext('Введите комментраий')} onChange={this.handleCommentChange}/>}
                <textarea ref="comment" className="inputData feedtext" placeholder={gettext('Введите комментраий')} onChange={this.handleCommentChange} maxLength="255"></textarea>
                {!this.state.ajaxRequest ? <button onClick={this.rejectOrder} className='button button-red center-button selectRepair' type='button'>{gettext('Отказаться')}</button> : <Loading />}
            </div>
        );
    }
});
