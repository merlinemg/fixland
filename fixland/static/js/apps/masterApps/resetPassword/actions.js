import Reflux from 'reflux';

var actions = Reflux.createActions([
    'resetPassword'
]);

module.exports = actions;
