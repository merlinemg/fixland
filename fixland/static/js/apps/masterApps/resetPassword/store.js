import actions from './actions';
import Reflux from 'reflux';
import Utils from '../../common/utils';

var store = Reflux.createStore({
    listenables: [actions],

    onResetPassword(phone) {
        Utils.ajaxPost({
            url: '/api/v1/reset-password/',
            data: {phone: phone}
        }).then(() => {
            location.hash = '#/done';
        }).fail(() => {
            location.hash = '#/server-problem';
        });
    }
});

module.exports = store;
