/* globals gettext */

import React from 'react';
import actions from '../actions';
import Reflux from 'reflux';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import PhoneInput from '../../../common/components/phoneInput';


var ResetPassword = React.createClass({
    mixins: [Reflux.connect(store)],

    getInitialState() {
        return {phone: '', errors: []};
    },

    validatePhone() {
        if (this.state.phone && !!this.state.phone.length) {
            return true;
        } else {
            return false;
        }
    },

    setPhone(value) {
        this.setState({phone: value});
    },

    reset() {
        if (this.validatePhone()) {
            this.setState({
                ajaxRequest: true
            });
            actions.resetPassword(this.state.phone);
        } else {
            this.setState({
                ajaxRequest: false
            });
            this.setState({showError: true});
        }
    },

    render() {
        return (
            <div className="content step">
                <h3>{gettext('Сброс пароля')}</h3>
                {this.state.errors.map(error => {
                    return (
                        <ErrorMessage message={error} />
                    );
                })}
                <div className="wrapperDataInput">
                    <PhoneInput value={this.state.phone} showError={this.state.showError} validate={this.validatePhone} completed={this.setPhone} />
                </div>
                <div className="wrapperDataInput">
                    {this.state.ajaxRequest ? <Loading /> : <button className="button button-green selectRepair" onClick={this.reset}>{gettext('Сбросить')}</button>}
                </div>
            </div>
        );
    }
});

module.exports = ResetPassword;
