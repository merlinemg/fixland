/* globals gettext */

import React from 'react';


var ResetPasswordDone = React.createClass({
    render() {
        return (
            <div className="content step problem">
                <h4>{gettext('Вам было отправлено смс с новым паролем!')}</h4>
            </div>
        );
    }
});

module.exports = ResetPasswordDone;
