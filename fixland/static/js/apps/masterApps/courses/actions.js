import Reflux from 'reflux';

var actions = Reflux.createActions([
    'setCourse',
    'getCourses',
    'selectEventLesson',
    'cancelEventLesson'
]);

module.exports = actions;
