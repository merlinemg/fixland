import actions from './actions';
import Reflux from 'reflux';
import reqwest from 'reqwest';
import Utils from '../../common/utils';

var store = Reflux.createStore({
    listenables: [actions],

    onGetCourses() {
        reqwest({
            url: '/api/v1/master-courses/',
            method: 'get',
            contentType: 'application/json',
            type: 'json'
        }).then((response) => {
            this.trigger({success: true, lessonEvents: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onSelectEventLesson(eventLesson) {
        Utils.ajaxPost({
            url: '/api/v1/master-course-event/',
            data: {'lesson_event': eventLesson.id}
        }).then(() => {
            actions.getCourses();
        }).fail(() => {
            location.hash = '#/server-problem';
        });
    },

    onCancelEventLesson(eventLesson) {
        Utils.ajaxDelete({
            url: '/api/v1/master-course-event/' + eventLesson.master_course + '/'
        }).then(() => {
            actions.getCourses();
        }).fail(() => {
            location.hash = '#/server-problem';
        });
    }
});

module.exports = store;
