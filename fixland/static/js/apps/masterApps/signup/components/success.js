/* globals gettext */

import React from 'react';


var SignupSuccess = React.createClass({
    render() {
        return (
            <div className='content step'>
                <h4>{gettext('Спасибо, ждите звонка от нас в ближайшие несколько дней')}</h4>
            </div>
        );
    }
});

module.exports = SignupSuccess;
