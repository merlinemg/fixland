/* globals jQuery, gettext */

import React from 'react';
import Router from 'react-router';
import Reflux from 'reflux';

import Loading from '../../../common/components/loading.js';
import actions from '../actions';
import store from '../store';


module.exports = React.createClass({
    mixins: [Reflux.connect(store), Router.Navigation],
    componentDidMount() {
        actions.getOffer();
        var element = jQuery('.offer').closest('#app').get(0);
        jQuery(element).addClass('whiteBg');
    },
    handleAcceptClick() {
        actions.setOffer(true);
        this.transitionTo('signup');
    },
    handleCancelClick() {
        this.transitionTo('signup');
    },
    render() {
        return (
            <div className='content step offer'>
                {this.state.offerHtml ? (
                    <div>
                        <div className="static-text" dangerouslySetInnerHTML={{__html: this.state.offerHtml}}></div>
                        <div className="wrapperDataInput login-buttons">
                            <div className="one-half">
                                <button className="button button-green selectRepair" onClick={this.handleAcceptClick}>{gettext('Я согласен')}</button>
                            </div>
                            <div className="one-half last-column">
                                <button className="button button-blue selectRepair" onClick={this.handleCancelClick}>{gettext('Отмена')}</button>
                            </div>
                        </div>
                    </div>
                ) : <Loading />}
            </div>
        );
    }
});
