/* globals gettext*/

import actions from '../actions';
import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import ConfirmMessage from '../../../common/components/confirmMessage';
import Utils from '../../../common/utils';


var ConfirmAccount = React.createClass({
    mixins: [Reflux.connect(store)],

    componentWillMount() {
        if (!this.state.userId) {
            location.replace('/master_signup/');
        }
        actions.cleanUserWhithoutId();
    },

    getInitialState: function() {
        return {
            showSendNewSmsCodeError: false,
            showConfirmSmsCodeError: false,
            smsCodeConfirmed: false,
            newCodeSended: false
        };
    },

    confirmSmsCode: function(){
        if (this.validateCode()) {
            var code = this.state.code;
            if (!code) {
                return;
            }
            this.setState({
                ajaxRequest: true,
                showSendNewSmsCodeError: false,
                showConfirmSmsCodeError: false,
                smsCodeConfirmed: false,
                newCodeSended: false
            });
            Utils.ajaxPost({
                url: '/verify_sms_code/' + code + '/'
            }).then((response) => {
                if (response.result) {
                    actions.cleanUser();
                    location.hash = '#/success';
                } else {
                    this.setState({
                        showConfirmSmsCodeError: true,
                        ajaxRequest: false
                    });
                }
            });
        } else {
            this.setState({showError: true});
        }
    },

    sendNewSmsCode: function(){
        this.setState({
            ajaxRequest: true,
            showSendNewSmsCodeError: false,
            showConfirmSmsCodeError: false,
            smsCodeConfirmed: false,
            newCodeSended: false
        });

        Utils.ajaxPost({
            url: '/send_new_sms_code/' + this.state.userId + '/'
        }).then((response) => {
            if (response.result) {
                this.setState({
                    newCodeSended: true,
                    ajaxRequest: false
                });
            } else {
                this.setState({
                    showSendNewSmsCodeError: true,
                    ajaxRequest: false
                });
            }
        });
    },

    validateCode() {
        if (this.state.code && !!this.state.code.length) {
            return true;
        } else {
            return false;
        }
    },

    handleCodeInputChange(event) {
        this.setState({code: event.target.value});
    },

    updateInputValue(refName, stateName) {
        var state = {};
        var value = this.refs[refName].getDOMNode().value;
        state[stateName] = value.replace(/[^\d]/g, '');
        this.setState(state);
    },

    render() {
        return (
            <div className='content step'>
                <h4>{gettext('Вам было отправлено письмо с ссылкой для активации учетной записи!')}</h4>
                <div className="wrapperDataInput">
                    <div>
                        <div className="wrpInptError">
                            <input ref="code" type='text' value={this.state.code} onChange={this.updateInputValue.bind(this, 'code', 'code')} onBlur={this.handleCodeInputChange} placeholder={gettext('Код')} id = "code" className="inputData" />
                            {this.state.showError && !this.validateCode() ? <ErrorMessage message={gettext('Введите код')} /> : null}
                        </div>
                        <div className="regWrapBtn">
                            {this.state.ajaxRequest ? <div className="wrapLoading"><Loading /></div> : <button className="button button-green center-button selectRepair slider-btn" onClick={this.confirmSmsCode}>{gettext('Подтвердить')}</button>}
                            <button className="button button-blue center-button selectRepair slider-btn" onClick={this.sendNewSmsCode}>{gettext('Отправить еще раз')}</button>
                            <a href="/" className="button center-button selectRepair slider-btn transparrentBtn">{gettext('Пропустить')}</a>
                            { this.state.newCodeSended ? <ConfirmMessage message={gettext('новое сообщение отправлено')} /> : ''}
                            { this.state.showSendNewSmsCodeError ? <ErrorMessage message={gettext('не чаще 5 минут')} /> : ''}
                            { this.state.showConfirmSmsCodeError ? <ErrorMessage message={gettext('неверный код')} /> : ''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = ConfirmAccount;
