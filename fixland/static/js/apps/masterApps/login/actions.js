import Reflux from 'reflux';

var actions = Reflux.createActions([
    'setPhone',
    'setPassword',
    'login',
    'cleanForm'
]);

module.exports = actions;
