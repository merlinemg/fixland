/* global jQuery */

import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';
import Storage from '../../common/storage';
import Utils from '../../common/utils';

var storage = new Storage();

var store = Reflux.createStore({
    storage: storage,
    listenables: [actions],

    onCleanForm() {
        this.storage.removeItem('form');
    },

    getInitialState() {
        this.Form = this.storage.getItem('form');
        if (!this.form) {
            this.form = {
                phone: '',
                password: ''
            };
        }
        return this.form;
    },

    setFormItem(key, value) {
        this.form[key] = value;
        this.storage.setItem('form', this.form);
        this.trigger(this.form);
    },

    onSetPhone(value) {
        this.setFormItem('phone', value);
    },

    onSetPassword(value) {
        this.setFormItem('password', value);
    },

    login() {
        if (!this.form.phone) {
            this.trigger({ajaxRequest: false});
            return;
        }
        var data = jQuery.extend(true, {}, this.form);
        Utils.ajaxPost({
            url: '/login_api/',
            data: data
        }).then(() => {
            var redirectTo = '/';
            if (Utils.getParam('next')) {
                redirectTo = Utils.getParam('next') + location.hash;
            }
            actions.cleanForm();
            window.location = redirectTo;
        }).fail((responseData) => {
            var errors = [];
            try {
                errors = JSON.parse(responseData.response);
            } catch (e) {}
            if (_.isObject(errors)) {
                this.trigger({errors: _.flatten(_.values(errors))});
            } else {
                this.trigger({errors: errors});
            }
            this.trigger({ajaxRequest: false});
        });
    }
});

module.exports = store;
