import React from 'react';
import ReactRouter from 'react-router';
import SelectDevice from './components/selectDevice.js';
import SelectModel from './components/selectModel.js';
import Parts from './components/parts.js';
import PartChargeOff from './components/partChargeOff.js';
import PartOrderForOrder from './components/partOrderForOrder.js';
import PartOrderStep1 from './components/partOrderStep1.js';
import PartOrderStep2 from './components/partOrderStep2.js';
import PartOrderStep3 from './components/partOrderStep3.js';
import SelectModelHelp from './components/selectModelHelp';
import OrderServerProblem from '../../common/components/orderServerProblem';
import ReservedParts from './components/reservedParts'

var {Route, RouteHandler} = ReactRouter;


var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <RouteHandler />
        );
    }
});

var partsRoutes = (
    <Route name='app' handler={App}>
        <Route name='reservedParts' path='/' handler={ReservedParts} />
        <Route name='selectDevice' path='/device/' handler={SelectDevice} />
        <Route name='selectModel' path='/device/:device' handler={SelectModel} />
        <Route name='selectModelHelp' path='/device/:device/modelhelp' handler={SelectModelHelp}/>
        <Route name='parts' path='/device/:device/:model' handler={Parts} />
        <Route name='partOrderForOrder' path='/orderfor/:order' handler={PartOrderForOrder} />
        <Route name='partOrderStep1' path='/device/:device/:model/order' handler={PartOrderStep1} />
        <Route name='partOrderStep2' path='/device/:device/:model/order/step2' handler={PartOrderStep2} />
        <Route name='partOrderStep3' path='/device/:device/:model/order/step3' handler={PartOrderStep3} />
        <Route name='partChargeOff' path='/device/:device/:model/:part' handler={PartChargeOff} />
        <Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </Route>
);

module.exports = partsRoutes;
