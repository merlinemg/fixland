import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getMasterParts',
    'getMasterPart',
    'getModelParts',
    'getVendorsParts',
    'partChargeOff',
    'addPartToPartsCart',
    'addPartToVendorPartsCart',
    'removeVendorPartsCartItem',
    'cleanPartsCart',
    'cleanVendorsPartsCart',
    'setPartQty',
    'selectVendorCartPartToggle',
    'createOrder',
    'getMasterOrder',
    'cleanStore',
    'cleanUpdatePartsAvailabilityId',
]);

module.exports = actions;
