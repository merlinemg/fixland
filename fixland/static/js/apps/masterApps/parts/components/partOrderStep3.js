/* global gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';

import _ from 'underscore';
import actions from '../actions';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import Money from '../../../common/components/money.js';
import ErrorMessage from '../../../common/components/errorMessage.js';


var OrderVendorPart = React.createClass({
    mixins: [Reflux.connect(store)],
    getAmount() {
        return this.getQty() * parseInt(this.props.vendorPart.price);
    },
    getQty() {
        return _.findWhere(this.state.vendorsPartsCart, {partId: this.props.vendorPart.id}).qty;
    },
    render() {
        return (
            <div className="order-part-row">
                <span>{this.props.counter}. {this.props.vendorPart.name}</span>
                <br/>
                <div className="calculateRow">
                    <span className="calculateRow">
                        <span>{`${this.getQty()} x `}</span>
                        <Money amount={this.props.vendorPart.price} currency="rub"/>
                        <i className="dashedSeparator"></i>
                        <span className="countIndividualPart"><Money amount={this.getAmount()} currency="rub"/></span>
                    </span>
                </div>
            </div>
        );
    }
});

var OrderVendorTotal = React.createClass({
    render() {
        return (
            <div className="order-part-total-row">
                <div><Money amount={this.props.totalAmount} currency="rub"/></div>
            </div>
        );
    }
});

var OrderVendorParts = React.createClass({
    mixins: [Reflux.connect(store)],
    getAmount(vendorPart) {
        let qty = _.findWhere(this.state.vendorsPartsCart, {partId: vendorPart.id}).qty;
        return parseInt(vendorPart.price) * parseInt(qty);
    },
    render() {
        let totalAmount = 0;
        let counter = 0;
        return (
            <div>
                {this.props.vendorParts.map( (vendorPart) => {
                    totalAmount = totalAmount + this.getAmount(vendorPart);
                    counter++;
                    return <OrderVendorPart vendorPart={vendorPart} counter={counter}/>;
                })}
                <span className="countIndividualVendor"><OrderVendorTotal totalAmount={totalAmount}/></span>
            </div>
        );
    }
});

var Order = React.createClass({
    getWarehouseTitle(vendorParts) {
        const vendorName = vendorParts[0].vendor.name,
              warehouseCode = vendorParts[0].warehouse.code;
        return `${warehouseCode} ${vendorName}`;
    },
    render() {
        return (
            <div>
                {this.props.vendorsParts.map( (vendorParts) => {
                    return (
                        <div>
                            <h5>{this.getWarehouseTitle(vendorParts)}</h5>
                            <OrderVendorParts vendorParts={vendorParts} />
                        </div>
                    );
                })}
            </div>
        );
    }
});

module.exports = React.createClass({
    mixins: [Reflux.connect(store), Reflux.listenTo(store, 'storeTrigger'), Router.Navigation],
    componentWillMount() {
        if (!this.state.vendorsPartsCart.length) {
            this.transitionTo('partOrderStep1', this.props.params);
        } else if (!_.filter(this.state.vendorsPartsCart, (item) => {
            return item.selected === true && item.qty;
        }).length) {
            this.transitionTo('partOrderStep2', this.props.params);
        } else {
            actions.getVendorsParts();
        }
    },
    storeTrigger(data) {
        if (data.orderCreated === true) {
            this.transitionTo('parts', this.props.params);
            return
        }
        if (!this.state.orderCreated && data.orderCreated === false) {
            if (this.state.partsOrder.unsuccesfull_parts && this.state.partsOrder.unsuccesfull_parts.length) {
                this.transitionTo('partOrderStep2', this.props.params);
            } else {
                this.transitionTo('serverProblem');
            } 
            return
        }
    },
    getGroupedParts() {
        let selectedVenddorsCartParts = _.filter(this.state.vendorsPartsCart, (item) => {
            return item.selected === true && item.qty;
        });
        let selectedVenddorsParts = _.filter(this.state.vendorsParts, (item) => {
            return _.findWhere(selectedVenddorsCartParts, {partId: item.id});
        });
        let vendorsParts = _.groupBy(selectedVenddorsParts, function(item) {
            return item.warehouse.id;
        });
        return _.values(vendorsParts);
    },

    createOrder() {
        this.setState({ajaxRequest: true});
        actions.createOrder();
    },

    render() {
        return (
            <div className="content step wrpParts partsStepThree">
                <h3>{gettext('Мой заказ')}</h3>
                {this.state.errors && <ErrorMessage message={this.state.errors}/>}
                {this.state.success ? (
                    this.state.vendorsParts && (
                        <div>
                            <Order vendorsParts={this.getGroupedParts()} />
                            {this.state.ajaxRequest ?  <Loading /> : <button className="button button-green selectRepair" onClick={this.createOrder}>{gettext('Отправить заказ')}</button>}
                        </div>
                    )
                ) : <Loading />}
            </div>
        );
    }
});
