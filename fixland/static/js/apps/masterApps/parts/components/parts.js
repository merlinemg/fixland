/* global gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';

import _ from 'underscore';
import actions from '../actions';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ConfirmMessage from '../../../common/components/confirmMessage.js';
import ErrorMessage from '../../../common/components/errorMessage.js';

var Link = Router.Link;

var TableRow = React.createClass({
    getParams() {
        return {
            device: this.props.device,
            model: this.props.model,
            part: this.props.masterPart.part
        };
    },
    render() {
        return (
            <tr>
                <td className='table-sub-title'><span>{this.props.masterPart.part__name}</span></td>
                <td><span>{this.props.masterPart.quantity}</span></td>
                <td>
                    <Link to='partChargeOff' className='button delete-list delete-btn' params={this.getParams()}></Link>
                </td>
            </tr>
        );
    }
});

var Table = React.createClass({
    render() {
        return (
            <div>
                <table cellSpacing='0' className='table parts-table'>
                    <thead>
                        <tr>
                            <th><span>{gettext('Запчасть')}</span></th>
                            <th><span>{gettext('Числится')}</span></th>
                            <th><span>{gettext('Списать')}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.masterParts.map((masterPart) => {
                            return <TableRow masterPart={masterPart} device={this.props.device} model={this.props.model}/>;
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
});

var Parts = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.getMasterParts(this.props.params.model);
    },
    componentWillUnmount() {
        actions.cleanStore();
    },
    getDeviceName() {
        var device = _.findWhere(window.DEVICES, {slug: this.props.params.device});
        var model = _.findWhere(device.models, {slug: this.props.params.model});
        return `${device.name} ${model.name}`;
    },

    render() {
        let confirmMessageText = '',
            errorMessageText = '';
        if (this.state.partsOrder.succesfull_parts && this.state.partsOrder.succesfull_parts.length) {
            confirmMessageText = gettext('Заказ успешно создан');
        }
        if (this.state.partsOrder.unsuccesfull_parts && this.state.partsOrder.unsuccesfull_parts.length) {
            errorMessageText = gettext('Некоторые запчасти не были заказаны: ');
            errorMessageText +=  _.pluck(this.state.partsOrder.unsuccesfull_parts, 'article').join(', ');
        }
        return (
            <div className="content step wrpParts">
                {confirmMessageText && <ConfirmMessage message={confirmMessageText}/>}
                {errorMessageText && <ErrorMessage message={errorMessageText}/>}
                <h3>{`${gettext('Запчасти для')} ${this.getDeviceName()}`}</h3>
                { this.state.successMasterParts ? (
                    this.state.masterParts.length ? <Table masterParts={this.state.masterParts} device={this.props.params.device} model={this.props.params.model} /> : <p>{gettext('У Вас нет запчастей к данной модели')}</p>
                ) : <Loading />}
                <Link to='partOrderStep1' className='button button-green selectRepair' params={this.props.params}>{gettext('Заказать ещё')}</Link>
            </div>
        );
    }
});

module.exports = Parts;
