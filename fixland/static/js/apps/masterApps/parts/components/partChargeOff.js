/* global jQuery, gettext */

import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';


var PartChargeOff = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.getMasterPart(this.props.params.part);
    },
    validate() {
        return this.validateQuantity() && this.validateOffer();
    },
    validateQuantity() {
        return this.state.quantity && this.state.quantity <= this.state.masterPart.quantity;
    },
    validateOffer() {
        return this.state.offer;
    },
    handleQuantityChange(event) {
        this.setState({'quantity': event.target.value});
    },
    handeOfferClick(event) {
        this.setState({'offer': jQuery(event.target).prop('checked')});
    },
    submit() {
        if (this.validate()) {
            this.setState({ajaxRequest: true});
            var successRedirect = `device/${this.props.params.device}/${this.props.params.model}`;
            actions.partChargeOff(this.props.params.model, this.state.masterPart.part, this.state.quantity, successRedirect);
        } else {
            this.setState({showError: true});
        }
    },
    render() {
        return (
            <div className="content step part-off">
                <h3>{gettext('Списание запчасти')}</h3>
                <h5>{gettext('Я хочу списать потеряную, сломаную или бракованую запчасть')}</h5>
                {this.state.errors && this.state.errors.map( (error) => {
                    return <ErrorMessage message={error} />;
                })}
                {this.state.showError && !this.validateQuantity() && <ErrorMessage message={gettext('Введите верное количество')} />}
                {this.state.showError && !this.validateOffer() && <ErrorMessage message={gettext('Вы не подтвердили оферту')} />}
                {this.state.success && this.state.masterPart ? (
                    <div>
                        <div className="parfOffForm">
                            <div className="one-half wrpFormInputs">
                                <div className="partName"><span>{this.state.masterPart.part__name}</span></div>
                            </div>
                            <div className="two-half last-column">
                                <input ref="quantity" className="inputData" name="payment_amount" onChange={this.handleQuantityChange} placeholder={gettext('Введите количество')}/>
                            </div>
                        </div>
                        <div className="wrpCheckbox">
                            <input ref="offer" type="checkbox" onChange={this.handeOfferClick}/>
                            <span>{gettext('Я понимаю, что в случае обнаружения этой детали, я не смогу использовать её для ремонта через fixland.ru')}</span>
                        </div>
                        {this.state.ajaxRequest ? <Loading /> : (
                            <button onClick={this.submit} className="button button-green button-center selectRepair">{gettext('Списать')}</button>
                        )}
                    </div>
                ) : <Loading /> }
            </div>
        );
    }
});

module.exports = PartChargeOff;
