import React from 'react';
import Reflux from 'reflux';
import _ from 'underscore'
import moment from 'moment';

import Loading from '../../../common/components/loading.js';
import Money from '../../../common/components/money.js';
import store from '../stores/reservedPartsStore'
import actions from '../actions/reservedPartsActions'

let PartsOrder = React.createClass({
    mixins: [Reflux.connect(store)],
    cancelOrder() {
        actions.cancelOrder(this.props.orderParts[0].order_number);
    },
    cancelOrderItem() {
        actions.cancelOrderItem(this.props.orderParts[0].id);
    },
    getBalance(part) {
        let masterPart = _.findWhere(this.props.masterParts, {part: part.part.id});
        return masterPart ? masterPart.quantity : 0;
    },
    render() {
        let first = this.props.orderParts[0],
            createdAt = moment(first.created_at).format('DD-MM-YYYY H:SS')
        let title = (
            <span>
                <span>{`${createdAt} #${first.order_number}`}</span>
                {first.for_order_number && <span>{` (${gettext('заказ на ремонт')} #${first.for_order_number})`} </span>}
                <br className="mobileBr" />
                <span className="dispInlBl"> {first.warehouse.address}</span>
            </span>
        )
        let rows = this.props.orderParts.map((item) => {
            return (
                <tr>
                    <td>
                        <span>{item.part.name}</span>
                    </td>
                    <td className="td-qty"><span>{item.quantity}</span></td>
                    {this.props.showMasterBalance && <td className="td-qty"><span>{this.getBalance(item)}</span></td>}
                    <td className="td-price"><Money amount={item.price} /></td>
                    <td className="cancel-order-td">{!this.state.ajaxRequest && <a className='button delete-list delete-btn' onClick={this.cancelOrderItem}></a>}</td>
                </tr>
            )
        })
        return (
            <div className="partsOrder">
                <h5 className="pull-left partsInfo">
                    {title}
                <div className="del-btn">{!this.state.ajaxRequest && <a className='button delete-list delete-btn' onClick={this.cancelOrder}></a>}</div>
                </h5>
                <table className="bordered table resTable">
                    {this.props.showth && (
                        <thead>
                            <tr>
                                <th><span>{gettext('Запчасть')}</span></th>
                                <th><span>{gettext('Зак.')}</span></th>
                                {this.props.showMasterBalance && <th className="td-qty"><span>{gettext('Нал.')}</span></th>}
                                <th><span>{gettext('Цена')}</span></th>
                                <th className="cancel-order-td"></th>
                            </tr>
                        </thead>
                    )}
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </div>
        )
    }
})

module.exports = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.load(this.props.orderNumber, this.props.showMasterBalance)
    },
    groupByOrder() {
        return _.values(_.groupBy(this.state.reservedParts, 'order_number'));
    },
    render() {
        return (
            <div className="container">
                <span><h5>{gettext('Заказы на закупку:')}</h5>{this.state.ajaxRequest && <Loading />}</span>
                {this.state.reservedPartsLoaded && (
                    this.state.reservedParts.length > 0 ? (
                        this.groupByOrder().map((item) => {
                            return <PartsOrder orderParts={item} masterParts={this.state.masterParts} {...this.props}/>
                        })
                    ) : (
                        <span className="text-white">{gettext('нет')}</span>
                    )
                )}
            </div>
        );
    }
});