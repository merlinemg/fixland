import React from 'react';

module.exports = React.createClass({

    render() {
        var lineColor = 'ffffff';
        var addressText = null;
        if (this.props.metro) {
            if (this.props.metro.line_color) {
                lineColor = this.props.metro.line_color;
            }
            if (this.props.metro.name) {
                addressText = this.props.metro.name;
            }
        }

        var style = {
            borderColor: '#' + lineColor
        };

        if (!addressText) {
            return null;
        } else {
            return (<p className='ordersAddresP' style={style}>{addressText.replace(/(^|\s)метро(\s|$)/img, '')}</p>);
        }
    }
});
