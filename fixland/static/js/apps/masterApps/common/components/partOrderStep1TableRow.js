import React from 'react';
import Reflux from 'reflux';

import _ from 'underscore';
import actions from '../../parts/actions';
import store from '../../parts/store';

module.exports = React.createClass({
    mixins: [Reflux.connect(store)],
    componentDidMount() {
        if (this.props.needs) {
            const needPart = _.findWhere(this.props.needs, {part: this.props.part.id});
            if (needPart) {
                actions.addPartToPartsCart(this.props.part, parseInt(needPart.qty));
            }
        }
    },
    handleQtyChange(part, event) {
        actions.addPartToPartsCart(part, parseInt(event.target.value));
    },
    getMasterQty(part) {
        const masterPart = _.findWhere(this.props.masterParts, {part: part.id});
        if (masterPart) {
            return masterPart.quantity;
        }
        return 0;
    },
    getCartQty(part) {
        if (this.props.needs) {
            const needPart = _.findWhere(this.props.needs, {part: part.id});
            if (needPart) {
                return needPart.qty;
            }
        } else {
            const cartPart = _.findWhere(this.state.partsCart, {partId: part.id});
            if (cartPart) {
                return cartPart.qty;
            }
        }
        return 0;
    },
    render() {
        var selectItem = [];
        for (var i = 0; i <= 20; i++) {
            selectItem.push(<option>{i}</option>);
        }
        return (
            <tr>
                <td className='table-sub-title'><span>{this.props.part.name}</span></td>
                <td><span>{this.getMasterQty(this.props.part)}</span></td>
                <td>
                    <select className="inputData qtyInput centered selCount" type='text' defaultValue={this.getCartQty(this.props.part)} onChange={this.handleQtyChange.bind(this, this.props.part)}>
                        {selectItem}
                    </select>
                </td>
            </tr>
        );
    }
});
