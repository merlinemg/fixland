/* global gettext */

import React from 'react';
import _ from 'underscore';

import TableRow from '../../common/components/partOrderStep1TableRow.js';

module.exports = React.createClass({
    getPartsList() {
        if (this.props.onlyNeeds) {
            return _.filter(this.props.parts, (item) => {
                return _.findWhere(this.props.needs, {part: item.id}) ? true : false;
            });
        }
        return this.props.parts;
    },
    render() {
        return (
            <div>
                <table cellSpacing='0' className='table parts-table smallInputInTable'>
                    <thead>
                        <tr>
                            <th><span>{gettext('Запчасть')}</span></th>
                            <th><span>{gettext('Есть')}</span></th>
                            <th><span>{gettext('Заказ')}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.parts && this.getPartsList().map((part) => {
                            return <TableRow part={part} masterParts={this.props.masterParts} needs={this.props.needs}/>;
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
});
