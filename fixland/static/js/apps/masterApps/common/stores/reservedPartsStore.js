/* globals  jQuery */

import Reflux from 'reflux';
import actions from '../actions/reservedPartsActions';
import Utils from '../../../common/utils';

module.exports = Reflux.createStore({
    listenables: [actions],
    orderNumber: null,

    load(orderNumber, loadMasterParts) {
        this.orderNumber = orderNumber;
        this.trigger({ajaxRequest: true});

        let promises = [this.getReservedPartsForOrder(orderNumber)];
        if (loadMasterParts) {
            promises.push(this.getMasterParts())
        }
        jQuery.when.apply(null, promises).then( (response1, response2) => {
            let masterParts = response2 ? response2 : [];
            this.trigger({reservedPartsLoaded: true, reservedParts: response1, masterParts: masterParts, ajaxRequest: false});
        });
    },

    getMasterParts() {
        let deferred = new jQuery.Deferred();
        Utils.ajaxGet({
            url: '/api/v1/master-parts/',
        }).then((response) => {
            deferred.resolve(response);
        }).fail(() => {
            deferred.reject();
        });
        return deferred;
    },

    getReservedPartsForOrder(orderNumber) {
        let deferred = new jQuery.Deferred();
        Utils.ajaxGet({
            url: '/api/v1/master-reserved-parts/',
            data: {forOrder: orderNumber},
            decamelize: true
        }).then((response) => {
            deferred.resolve(response);
        }).fail(() => {
            deferred.reject();
        });
        return deferred;
    },

    updateReservedParts(orderNumber) {
        let promise = this.getReservedPartsForOrder(orderNumber);
        promise.then((response) => {
            this.trigger({ajaxRequest: false, reservedParts: response});
        });
    },

    cancelOrder(orderNumber) {
        this.trigger({
            ajaxRequest: true
        });
        Utils.ajaxPost({
            url: '/cancel_parts_order/',
            data: {orderNumber: orderNumber},
            decamelize: true
        }).then((response) => {
            this.updateReservedParts(this.orderNumber);
        });
    },
    cancelOrderItem(orderId) {
        this.trigger({
            ajaxRequest: true
        });
        Utils.ajaxPost({
            url: '/cancel_parts_order_item/',
            data: {orderId: orderId},
            decamelize: true
        }).then((response) => {
            this.updateReservedParts(this.orderNumber);
        });
    }
});