import React from 'react';
import ReactRouter from 'react-router';

import repairRoutes from './repair/router';
import sellRoutes from './sell/router';
import feedbackRoutes from './feedback/router';
import repairBreadcrumbs from './repair/components/breadcrumbs';
import sellBreadcrumbs from './sell/components/breadcrumbs';
import transactionsRoutes from './transactions/router';
import masterReviewRoutes from './masterReview/router';
import warrantyRoutes from './warranty/router';

window.warrantyRoutes = warrantyRoutes;
window.masterReviewRoutes = masterReviewRoutes;
window.repairRoutes = repairRoutes;
window.transactionsRoutes = transactionsRoutes;
window.repairBreadcrumbs = repairBreadcrumbs;
window.sellRoutes = sellRoutes;
window.sellBreadcrumbs = sellBreadcrumbs;
window.feedbackRoutes = feedbackRoutes;
window.ReactRouter = ReactRouter;
window.React = React;
