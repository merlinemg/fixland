import React from 'react';
import ReactRouter from 'react-router';
import Review from './components/review';
import OrderServerProblem from '../common/components/orderServerProblem';



var App = React.createClass({

    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },

    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var reviewRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='' path='/' handler={Review}/>
        <ReactRouter.Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </ReactRouter.Route>
);

module.exports = reviewRoutes;
