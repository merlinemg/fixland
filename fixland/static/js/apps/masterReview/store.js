import Reflux from 'reflux';
import _ from 'underscore';
import actions from './actions';
import Storage from '../common/storage';
import Utils from '../common/utils';

var storage = new Storage();

var store = Reflux.createStore({
    storage: storage,
    listenables: [actions],
    review: null,

    getInitialState() {
        this.review = {
            score: null,
            comment: null,
            asInOrder: null,
            phone: null,
            email: null
        };
        return this.review;
    },
    cleanReview() {
        this.storage.removeItem('review');
    },
    onSetReviewItem(key, value) {
        this.review[key] = value;
        this.trigger(this.review);
    },

    createReview(inviteCode) {
        var data = _.extend(this.review, {inviteCode: inviteCode});
        Utils.ajaxPost({
            url: '/create_order_review/',
            data: data,
            decamelize: true
        }).then(() => {
            this.cleanReview();
            location.href = '/';
        }).fail(() => {
            this.trigger({'ajaxRequest': false});
            location.hash = '#/server-problem';
        });
    }
});

module.exports = store;
