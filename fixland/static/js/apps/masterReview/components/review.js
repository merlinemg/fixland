/* globals jQuery, gettext */

import React from 'react';
import Reflux from 'reflux';
import addons from 'react-addons';
import store from '../store';
import actions from '../actions';
import Utils from '../../common/utils';
import Loading from '../../common/components/loading.js';
import Money from '../../common/components/money.js';
import PhoneInput from '../../common/components/phoneInput';
import EmailInput from '../../common/components/emailInput';
import ErrorMessage from '../../common/components/errorMessage';


let BonusBlock = React.createClass({
    getInitialState() {
        return {
            showInput: !this.showButtons()
        };
    },
    setShowInput() {
        this.setState({showInput: true});
        this.props.setReviewItem('asInOrder', false);
    },
    setAsInOrder() {
        this.setState({showInput: false});
        this.props.setReviewItem('asInOrder', true);
        this.props.setReviewItem('phone', null);
        this.props.setReviewItem('email', null);
    },
    setPhone(value) {
        this.props.setReviewItem('phone', value);
    },
    handleEmailInputChange(event) {
        this.props.setReviewItem('email', event.target.value);
    },
    showButtons() {
        return this.props.showTelBonus ? (
            window.DATA_FOR_REVIEW.own_phone ? true : false
        ) : (
            window.DATA_FOR_REVIEW.email ? true : false
        )
    },
    render() {
        let cx = addons.classSet;
        let asInOrderBtn = cx({
            'coverpage-button': true,
            sellTimeBtn: true,
            active: this.props.asInOrder
        });
        let showOtherBtn = cx({
            'coverpage-button': true,
            sellTimeBtn: true,
            active: this.state.showInput
        });
        return (
            <div className="reviewBonusBlock">
                {this.props.showTelBonus ? (
                    <h4>{gettext('На какой номер зачислить бонусные')} <Money amount={this.props.telBonusAmount} currency='rub'/>?</h4>
                ) : (
                    <h4>{gettext('На какой e-mail выслать купон со скидкой для будущих ремонтов?')}</h4>
                )}
                {this.showButtons() && (
                    <div className='wrapperselectTime'>
                        <button onClick={this.setAsInOrder} className={asInOrderBtn}>{gettext('Как в заказе')}</button>
                        <span className='spanSeparator'>{gettext('или')}</span>
                        <button onClick={this.setShowInput} className={showOtherBtn}>{gettext('Указать другой')}</button>
                    </div>
                )}
                {this.props.showTelBonus ? (
                    <div className='wrapperDataInput'>
                        {this.state.showInput && <PhoneInput value={this.props.phone} showError={this.props.showError} validate={this.props.validatePhone} completed={this.setPhone}/> }
                    </div>
                ) : (
                    this.state.showInput && <EmailInput update={this.handleEmailInputChange} value={this.props.email} showError={this.props.showError} validate={this.props.validateEmail}/>
                )}
            </div>
        );
    }
});


let Rating = React.createClass({
    getDefaultProps() {
        return {
            score: 3
        };
    },
    componentDidMount() {
        jQuery(React.findDOMNode(this.refs.score)).raty({
            score: this.props.score,
            starType: 'i',
            click: (score) => {
                this.props.scoreChanged(score);
            }
        });
    },
    render() {
        return (
            <div className="wrpInptError">
                <div ref="score" className="score"></div>
                {this.props.showError && !this.props.validate() && <ErrorMessage message={gettext('Дайте оценку мастеру')} />}
            </div>
        );
    }
});


module.exports = React.createClass({
    mixins: [Reflux.connect(store)],
    commentDidMount() {
        actions.setReviewItem(window.DATA_FOR_REVIEW.email ? true : false);
    },
    canShowTelBonus() {
        return window.DATA_FOR_REVIEW.bonus_type === 1;
    },
    validate() {
        return this.validateScore() && this.validatePhone() && this.validateEmail();
    },
    validateScore() {
        return this.state.score ? true : false;
    },
    validatePhone() {
        if (this.state.asInOrder || !this.canShowTelBonus()) {
            return true;
        }
        return this.state.phone ? true : false;
    },
    validateEmail() {
        if (this.state.asInOrder || this.canShowTelBonus()) {
            return true;
        }
        return this.state.email && Utils.validateEmail(this.state.email);
    },

    handleScoreInputChange(event) {
        actions.setReviewItem('score', event.target.value);
    },
    handleCommentInputChange(event) {
        actions.setReviewItem('comment', event.target.value);
    },
    scoreChanged(score) {
        actions.setReviewItem('score', score);
    },


    createReview() {
        if (this.validate()) {
            this.setState({ajaxRequest: true});
            actions.createReview(window.DATA_FOR_REVIEW.invite_code);
        } else {
            this.setState({ajaxRequest: false});
            this.setState({showError: true});
        }
    },

    render() {
        return (
            <div className='content step feedBackWrp'>
                <h3>{gettext('Оцените работу мастера?')}</h3>

                <div className='wrapperDataInput'>
                    <Rating score={this.state.score}
                        scoreChanged={this.scoreChanged}
                        validate={this.validateScore}
                        showError={this.state.showError}/>
                </div>

                <div className='wrapperDataInput'>
                    <textarea ref='comment' type='text' className="inputData feedtext" defaultValue={this.state.comment} maxLength = '255' onChange={this.handleCommentInputChange} placeholder="Ваш комментарий"></textarea>
                </div>

                <BonusBlock asInOrder={this.state.asInOrder}
                    telBonusAmount={window.DATA_FOR_REVIEW.tel_bonus_amount}
                    validatePhone={this.validatePhone}
                    validateEmail={this.validateEmail}
                    setReviewItem={actions.setReviewItem}
                    phone={this.state.phone}
                    email={this.state.email}
                    showTelBonus={this.canShowTelBonus()}
                    showError={this.state.showError}/>

                <div>
                    {this.state.ajaxRequest ? <div className='wrapLoading'><Loading /></div> : <button className='button button-green center-button selectRepair lhBtn' type='button' onClick={this.createReview}>{gettext('Отправить')}</button>}
                </div>
                <div>
                    <br/>
                    <h6 className="attentionComment">{gettext('Внимание! Напоминаем, что наша гарантия распространяется только на заказы, размещённые на сайте Fixland.ru. Если Вы договариваетесь о ремонте напрямую с мастером (в обход сайта), то мы не можем контролировать качество устанавливаемых запчастей и не можем предоставить гарантию.')}</h6>
                </div>
            </div>
        );
    }
});
