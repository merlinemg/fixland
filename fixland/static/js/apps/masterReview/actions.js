import Reflux from 'reflux';

var actions = Reflux.createActions([
    'setReviewItem',
    'createReview'
]);

module.exports = actions;
