/* globals gettext */

import React from 'react';
import ErrorMessage from '../../common/components/errorMessage';

var EmailInput = React.createClass({
    getDefaultProps() {
        return {
            value: '',
            showError: false,
            validate: () => {
                return true;
            }
        };
    },

    render() {
        return (
            <div className='wrapperEmail wrpSugData width80-on-660'>
                <input ref='email' id = 'email' type='email' placeholder='e-mail' className='inputData'
                    defaultValue={this.props.value} onChange={this.props.update}/>
                { this.props.showError && !this.props.validate() ? <ErrorMessage message={gettext('Введите верный email адрес')} /> : null}
                { this.props.error && <ErrorMessage message={this.props.error} />}
            </div>
        );
    }
});

module.exports = EmailInput;
