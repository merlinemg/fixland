/* globals gettext */

import React from 'react';


var OrderServerProblem = React.createClass({
    render() {
        return (
            <div className='content step problem'>
                <h3>{gettext('Возникла проблема на сервере!')}</h3>
                <h4>{gettext('Пожалуйста свяжитесь с нами по номеру телефона')}: +7 (903) 799-09-88!</h4>
            </div>
        );
    }
});

module.exports = OrderServerProblem;
