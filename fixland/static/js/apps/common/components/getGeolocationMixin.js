/* globals gettext */

import Loading from './loading.js';

var GetGeolocationMixin = {
    /*
    Required attributes:
        - state
        - actions
    */
    render() {
        return (
            <div>
                {this.state.geolocationLoading ? <div className="wrapLoading"><Loading /></div> : <button onClick={this.actions.getUserGeoLocation} className="button button-blue center-button not-active selectRepair mapsBtn addr">{gettext('Определить мое местоположение')}</button>}
            </div>
        );
    }
};

module.exports = GetGeolocationMixin;
