/* globals gettext */

import React from 'react';
import ErrorMessage from '../../common/components/errorMessage';

module.exports = React.createClass({
    render() {
        return (
            <div className='wrapperEmail width80-on-660'>
                <input ref='email' id = 'email' type='email' placeholder='e-mail' className='inputData'
                    defaultValue={this.props.value} onChange={this.props.onChange} onBlur={this.props.onBlur}/>
                { this.props.error && <ErrorMessage message={this.props.error} />}
            </div>
        );
    }
});
