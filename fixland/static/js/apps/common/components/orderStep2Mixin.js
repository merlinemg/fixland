/* globals jQuery, gettext */

import ErrorMessage from './errorMessage';
import Loading from './loading.js';
import React, { Component } from 'react'
import GooglePlacesSuggest from 'react-google-places-suggest'

var OrderStep2Mixin = {
    /*
    Required attributes:
        - state
        - actions
        - GetGeolocation
    */




    handleSearchChange(e){
        this.setState({ search: e.target.value })
    },

    handleSelectSuggest(suggestName, coordinate){
        this.setState({ search: suggestName, selectedCoordinate: coordinate })
        console.log(coordinate);
    },
    submitForm() {
        if (this.validateAddress()) {
            this.setState({
                orderSubmitting: true
            });
            this.actions.updateUserAddress(this.state.search, this.state.selectedCoordinate);
        } else {
            this.setState({showUserAddressError: true});
        }
    },
    validateAddress() {
        return this.refs.address.getDOMNode().value;
    },

    updateInputValue(refName, stateName) {
        var state = {
            search: '',
            selectedCoordinate: null,
        };
        state[stateName] = this.refs[refName].getDOMNode().value;
        this.setState(state);
    },

    render() {
        const { search } = this.state
        var errorMessage = (!this.state.showUserAddressError || this.validateAddress()) ? '' : gettext('Пожалуйста введите адрес');
        return (
            <div className='content step'>
                <h3>{gettext('Куда подъехать?')}</h3>
                <this.GetGeolocation {...this.props} />
                    <div className='wrappetError'>
                        {this.state.geolocationError ? <ErrorMessage message={gettext('Проблема с получением вашего местоположения')} /> : <span></span>}
                    </div>
                <p className='proposalAddr handAddr'>{gettext('или ввести адрес вручную')}:</p>
                <div className='wrapperInputAddr width80-on-660'>
                   <GooglePlacesSuggest onSelectSuggest={ this.handleSelectSuggest } search={ search } language='ru'>
                    <div className='wrpInput'>
                        <input ref='address' type="text" value={ search }  onChange={ this.handleSearchChange }  className='inputAddr' />
                    </div>
                    {errorMessage.length ? <ErrorMessage message={errorMessage} /> : <span></span>}
                   </GooglePlacesSuggest>
                </div>
                {this.state.sendingOrder ? <div className='wrapLoading'><Loading /></div> : <button className='button button-green center-button not-active selectRepair' onClick={this.submitForm}>{gettext('Отправить заказ')}</button>}
            </div>
        );
    }
};

module.exports = OrderStep2Mixin;
