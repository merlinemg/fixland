import React from 'react';

module.exports = React.createClass({
    render() {
        if (this.props.photo) {
            return <img className="no-border-img" src={this.props.photo} alt='img' />;
        } else {
            return <img className="no-border-img" src='/static/images/pictures/convivence.jpg' alt='img' />;
        }
    }
});
