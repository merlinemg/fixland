/* globals gettext */

import ErrorMessage from './errorMessage';
import addons from 'react-addons';
import Utils from '../utils';
import Loading from './loading.js';
import EmailInput from './emailInput';
import PhoneInput from './phoneInput';
import _ from 'underscore';
import Money from '../../common/components/money.js';
import utils from '../../repair/utils';


var OrderStep3Mixin = {
    /*
    Required attributes:
        - state
        - actions
        - Router.Navigation
    */
    getInitialState() {
        return {
            name: {
                firstName: '',
                middleName: '',
                lastName: ''
            }
        };
    },
    componentDidMount() {
        if (this.state.repeated_order_coupon) {
            this.setState({ajaxRequest: true});
            this.actions.checkForRepeatedOrderCoupon();
        }
        //facebook pixel
        if (window.is_production){
            fbq('track', 'InitiateCheckout');
        }
    },
    handleNameInputChange(event) {
        this.actions.setUserName(event.target.value);
    },
    handlePhoneInputChange(event) {
        this.actions.setUserPhone(event.target.value);
    },
    handleEmailInputChange(event) {
        this.actions.setUserEmail(event.target.value);
    },
    validateForm() {
        if (!!this.validateUserName() && this.validateUserPhone() && this.validatePhoneNumberOwner() && this.validateEmail()) {
            return true;
        } else {
            return false;
        }
    },
    validateUserName() {
        return this.refs.fullname.getDOMNode().value;
    },
    validateUserPhone() {
        if (this.state.userPhone && !!this.state.userPhone.length && (this.state.userPhone.length === 16)) {
            return true;
        } else {
            return false;
        }
    },
    validateEmail() {
        if (!this.state.userEmail) {
            return true;
        } else if (this.state.userEmail && (!this.state.userEmail.length || (!!this.state.userEmail.length && Utils.validateEmail(this.state.userEmail)))) {
            return true;
        } else {
            return false;
        }
    },
    submit() {
        if (this.validateForm()) {
            this.actions.placeOrder(true)
        } else {
            this.setState({showError: true});
        }
    },
    validatePhoneNumberOwner() {
        return !!this.state.phoneType;
    },
    getPrice() {
        let model = _.findWhere(_.findWhere(window.DEVICES, {slug: this.state.userDevice}).models, {slug: this.state.userModel});
        let grouped_repairs = model.grouped_repairs;
        if (!grouped_repairs.length) {
            return _.reduce(this.state.selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0);
        } else {
            let selectedRepairs = this.state.selectedRepairs;
            _.forEach(grouped_repairs, (grouped_repair) => {
                let items_in_group = _.filter(this.state.selectedRepairs, (item) => {
                    return grouped_repair.repair_list.indexOf(item.repair_id) != -1;
                });
                if (items_in_group.length === grouped_repair.repair_list.length) {
                    selectedRepairs = _.without(selectedRepairs, ...items_in_group);
                    selectedRepairs.push(grouped_repair);
                }
            });
            return _.reduce(selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0);
        }
    },
    render() {
        var cx = addons.classSet;
        var ownPhoneClasses = cx({
            'coverpage-button': true,
            active: this.state.phoneType === 'OWN'
        });
        var friendPhoneClasses = cx({
            'coverpage-button': true,
            active: this.state.phoneType === 'FRIEND'
        });
        return (
            <div className='content step stepThree'>
                <h3>{gettext('Как с Вами связаться?')}</h3>
                <div className='wrapperDataInput width80-on-660'>
                    <div className='wrpInptError identError'>
                        <input ref='fullname' type='text' defaultValue={this.state.userName} onChange={this.handleNameInputChange} placeholder={gettext('Как Вас зовут?')} id = 'fullname' className='inputData' />
                        {this.state.showError && !this.validateUserName() ? <ErrorMessage message={gettext('Введите ваше имя')} /> : null}
                    </div>
                    <PhoneInput update={this.handlePhoneInputChange} value={this.state.userPhone} showError={this.state.showError} validate={this.validateUserPhone} completed={this.actions.setUserPhone}/>
                </div>
                <h4 className='typeDescript'>{gettext('Это номер ваш или вашего друга?')}</h4>
                <div className='wrapperTelephone'>
                    <button onClick={this.actions.setOwnPhoneNumber} className={ownPhoneClasses}>{gettext('Мой')}</button>
                    <button onClick={this.actions.setFriendPhoneNumber} className={friendPhoneClasses}>{gettext('Друга')}</button>
                    {this.state.showError && !this.validatePhoneNumberOwner() ? <ErrorMessage message={gettext('Выберите один из вариантов')} /> : <span></span>}
                </div>
                <EmailInput update={this.handleEmailInputChange} value={this.state.userEmail} showError={this.state.showError} validate={this.validateEmail}/>
                {this.Сoupon && <h4>{gettext('Общая сумма заказа')}: <Money amount={this.getPrice()} currency='rub'/></h4>}
                {this.Сoupon && !window.is_partner_from_link_exists &&  !window.is_partner_user && <this.Сoupon coupon={this.state.coupon} />}
                {this.state.orderSubmitting ? <div className='wrapLoading'><Loading /></div> : <button className='button button-green center-button not-active selectRepair' onClick={this.submit}>{gettext('Далее')}</button>}
            </div>
        );
    }
};

module.exports = OrderStep3Mixin;
