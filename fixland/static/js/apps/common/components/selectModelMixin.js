/* globals gettext */

import _ from 'underscore';
import Router from 'react-router';

var Link = Router.Link;

var SelectModelMixin = {
    /*
    Required attributes:
        - state
        - nextRoute
    */
    getInitialState() {
        var device = _.findWhere(window.DEVICES, {slug: this.props.params.device});
        var obj = {
            models: device.models,
            deviceName: device.name
        };
        return obj;
    },
    render() {
        return (
            <div className="content step wrpDeviceModel">
                <h3 className="devModelh3">{gettext('Выберите модель')} {this.state.deviceName}</h3>
                <div>
                    <Link to='selectModelHelp' params={{device: this.props.params.device}} className="helpLink">{gettext('А как ее узнать?')}</Link>
                    {this.state.models.map(model => {
                        return (
                            <Link to={this.nextRoute} params={{device: this.props.params.device, model: model.slug}} className="deviceModel selectBtn">{this.state.deviceName} {model.name}</Link>
                        );
                    })}
                </div>
            </div>
        );
    }
};

module.exports = SelectModelMixin;
