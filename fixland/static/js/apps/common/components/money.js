import React from 'react';


module.exports = React.createClass({
    getDefaultProps() {
        return {
            currency: 'usd',
            amount: 0,
            minus: false,
            className: ''
        }
    },
    getCurrencySign() {
        if (!this.props.currency) {
            return '';
        }
        if (this.props.currency === 'usd') {
            return '$';
        }
        if (this.props.currency === 'rub') {
            return '₽';
        }
    },
    getValue() {
        const amount = parseInt(this.props.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
        const prefix = this.props.minus && parseInt(this.props.amount) > 0 ? '- ' : '';

        if (this.props.currency === 'usd'){
            return `${this.getCurrencySign()} ${prefix}${amount}`;
        } else {
            return `${prefix}${amount} ${this.getCurrencySign()}`;
        };
    },
    render() {
        return <span className={this.props.className}>{this.getValue()}</span>;
    }
});
