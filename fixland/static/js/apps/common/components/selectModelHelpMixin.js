/* globals gettext */

import _ from 'underscore';
import Router from 'react-router';

var Link = Router.Link;

var SelectModelHelpMixin = {
    /*
    Required attributes:
        - state
        - nextRoute
    */
    getInitialState() {

        var device = _.findWhere(window.DEVICES, {slug: this.props.params.device});
        var obj = {
            models: device.models,
            deviceName: device.name
        };
        return obj;
    },
    render() {
        return (
            <div className='content step wrpDeviceModel'>
                <h3 className='devModelh3'>{gettext('Как определить модель?')}</h3>
                <p className='pHelp'>{gettext('Посмотрите, какая модель указана на задней крышке Вашего устройства внизу после слова &quot;Model&quot;, далее выберите её из списка ниже')}</p>
                {this.state.models.map(model => {
                    return (
                        <div className='wrpModelHelp'>
                            <button className='selectBtn'>{this.state.deviceName} {model.name}</button>
                            <p className='deviceModelNumber'>{model.model_number}</p>
                            <Link to={this.nextRoute} params={{device: this.props.params.device, model: model.slug}}><button className='coverage-button' type='button'>{gettext('Выбрать')}</button></Link>
                        </div>
                    );
                })}
            </div>
        );
    }
};

module.exports = SelectModelHelpMixin;
