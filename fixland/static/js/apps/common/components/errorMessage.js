import React from 'react';


var ErrorMessage = React.createClass({
    propTypes: {
        message: React.PropTypes.string.isRequired
    },
    render() {
        return (
            <div className="error static-notification-red clearBoth tap-dismiss-notification">
                <p className="center-text uppercase">{this.props.message}</p>
            </div>
        );
    }
});

module.exports = ErrorMessage;
