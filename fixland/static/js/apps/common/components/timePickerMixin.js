/* globals jQuery, gettext */

import React from 'react';
import moment from 'moment';
import addons from 'react-addons';

var days = [gettext('Воскресенье'), gettext('Понедельник'), gettext('Вторник'), gettext('Среда'), gettext('Четверг'), gettext('Пятница'), gettext('Суббота')];
var months = [gettext('Янв'), gettext('Фев'), gettext('Мар'), gettext('Апр'), gettext('Май'), gettext('Июн'), gettext('Июл'), gettext('Авг'), gettext('Сен'), gettext('Окт'), gettext('Ноя'), gettext('Дек')];

var TimePickerMixin = {
    /*
    Required attributes:
        - state
        - actions
    */

    componentDidMount() {
        var date, day, endTime;
        if (!this.state.startDateTime || !this.state.endDateTime) {
            date = moment();
            day = date.hour() > 20 ? 1 : 0;
            var startHour = this.getFromHour(day);
            var endHour = startHour + 2;
            this.setState({
                day: day,
                startTime: startHour,
                endTime: endHour
            });
            this.actions.setStartDateTime(moment({hour: startHour}).format('YYYY-MM-DD HH:mm'));
            this.actions.setEndDateTime(moment({hour: endHour}).format('YYYY-MM-DD HH:mm'));
            if (day > 0) {
                jQuery(React.findDOMNode(this.refs.todayBtn)).prop( 'disabled', true );
            }
        } else {
            date = moment();
            var startDateTime = moment(this.state.startDateTime);
            var endDateTime = moment(this.state.endDateTime);
            day = date.date() < startDateTime.date() ? startDateTime.date() - date.date() : (date.hour() > 20 ? 1 : 0);
            var startTime = startDateTime.hour();
            if (day === 0) {
                startTime = startDateTime.hour() < date.hour() ? this.getFromHour(day) : startDateTime.hour();
            }
            if (startTime === startDateTime.hour()) {
                endTime = endDateTime.hour();
            } else {
                endTime = startTime + 2;
            }
            this.setState({
                day: day,
                startTime: startTime,
                endTime: endTime
            });
            if (day === 0) {
                this.actions.setStartDateTime(moment({hour: startTime}).format('YYYY-MM-DD HH:mm'));
                this.actions.setEndDateTime(moment({hour: endTime}).format('YYYY-MM-DD HH:mm'));
            }
            if (day > 0 && date.hour() > 20) {
                jQuery(React.findDOMNode(this.refs.todayBtn)).prop( 'disabled', true );
            }
            if (day > 1) {
                this.setState({showDays: true});
            }
        }
    },

    generateTimeArray(from, to) {
        var arr = [];
        for (var i = from; i <= to; i++) {
            arr.push({
                'value': i,
                'text': i + ':00'
            });
        }
        return arr;
    },

    getFromHour(day) {
        var fromHour = 9;
        var nowHour = moment().hour();
        if (day === 0) {
            fromHour = nowHour < 9 ? 9 : nowHour + 1;
        }
        return fromHour;
    },

    getTimeFrom() {
        return this.generateTimeArray(this.getFromHour(this.state.day), 21);
    },

    getTimeTo() {
        if (this.state.startTime >= 20){
            return this.generateTimeArray(parseInt(this.state.startTime) + 2, parseInt(this.state.startTime) + 2);
        } else {
            return this.generateTimeArray(parseInt(this.state.startTime) + 2, 21);
        }
    },

    getDays() {
        var arr = [];
        for (var i = 2; i <= 9; i++) {
            var date = moment().add(i, 'd');
            arr.push({
                'value': i,
                'text': `${days[date.day()]}, ${date.date()} ${months[date.month()]}`
            });
        }
        return arr;
    },

    updateStartDayTime(hour, day) {
        var dateTime = moment({hour: parseInt(hour)}).add(parseInt(day), 'd');
        this.actions.setStartDateTime(dateTime.format('YYYY-MM-DD HH:mm'));
    },

    updateEndDayTime(hour, day) {
        var dateTime = moment({hour: parseInt(hour)}).add(parseInt(day), 'd');
        this.actions.setEndDateTime(dateTime.format('YYYY-MM-DD HH:mm'));
    },

    handleTodayClick() {
        this.setState({
            startTime: this.getFromHour(0),
            endTime: this.getFromHour(0) + 2,
            showDays: false,
            day: 0
        });
        this.updateStartDayTime(this.getFromHour(0), 0);
        this.updateEndDayTime(this.getFromHour(0) + 2, 0);
    },
    handleTomorrowClick() {
        this.setState({
            showDays: false,
            day: 1
        });
        this.updateStartDayTime(this.state.startTime, 1);
        this.updateEndDayTime(this.state.endTime, 1);
    },
    handleLaterClick() {
        this.setState({
            showDays: true,
            day: 2
        });
        this.updateStartDayTime(this.state.startTime, 2);
        this.updateEndDayTime(this.state.endTime, 2);
    },

    handleDayChange(event) {
        this.setState({day: event.target.value});
        this.updateStartDayTime(this.state.startTime, event.target.value);
        this.updateEndDayTime(this.state.endTime, event.target.value);
    },

    handleStartTimeChange(event) {
        var hour = event.target.value;
        this.setState({startTime: hour});
        this.updateStartDayTime(hour, this.state.day);
        if (hour > 19 || parseInt(this.state.endTime) > 21 || (parseInt(this.state.endTime) - parseInt(hour)) < 2) {
            this.setState({endTime: parseInt(hour) + 2});
            this.updateEndDayTime(parseInt(hour) + 2, this.state.day);
        }
    },

    handleEndTimeChange(event) {
        this.setState({endTime: event.target.value});
        this.updateEndDayTime(event.target.value, this.state.day);
    },

    render() {
        var cx = addons.classSet;
        var todayButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.day === 0 && !this.state.showDays
        });
        var tomorrowButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.day === 1 && !this.state.showDays
        });
        var laterButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.showDays
        });
        return (
            <div className='wrappTimePicker'>
                <div className='wrapperselectTime wrpSellTime'>
                    <button ref='todayBtn' className={todayButtonClasses} onClick={this.handleTodayClick}>{gettext('Сегодня')}</button>
                    <button className={tomorrowButtonClasses} onClick={this.handleTomorrowClick}>{gettext('Завтра')}</button>
                    <button className={laterButtonClasses} onClick={this.handleLaterClick}>{gettext('Позже')}</button>
                </div>
                <div className='demos'>
                    {this.state.showDays && <select ref='days' className='inputData sellDate' value={this.state.day} onChange={this.handleDayChange}>
                        {this.getDays().map(day => {
                            return (
                                <option value={day.value}>{day.text}</option>
                            );
                        })}
                    </select>}
                    <div className="pseudoSelect pseudoStartTime">
                        <select ref='startDate' className='inputData startTimeSell' value={this.state.startTime} onChange={this.handleStartTimeChange}>
                            {this.getTimeFrom().map(hour => {
                                return (
                                    <option value={hour.value}>{hour.text}</option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="pseudoSelect pseudoEndtTime">
                        <select ref='endDate' className='inputData endTimeSell' value={this.state.endTime} onChange={this.handleEndTimeChange}>
                            {this.getTimeTo().map(hour => {
                                return (<option value={hour.value}>{hour.text}</option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </div>
        );
    }
};

module.exports = TimePickerMixin;
