import React from 'react';

var Loading = React.createClass({
    render() {
        return (
            <div className='fa fa-spinner fa-spin loadingSpiner'></div>
        );
    }
});

module.exports = Loading;
