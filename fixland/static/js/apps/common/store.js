import Storage from './storage';
import Navigator from './geo';
import Utils from '../common/utils';
import moment from 'moment';
import _ from 'underscore';


var storage = new Storage();

var baseStore = {
    storage: storage,
    order: null,

    getInitialState() {
        this.order = this.storage.getItem(this.orderKey);
        if (this.order && (!this.order.createOrderDate || parseInt(moment.duration(moment().diff(moment(this.order.createOrderDate))).asHours()) > 72)) {
            this.order = null;
            this.storage.removeItem(this.orderKey);
        }
        if (!this.order) {
            this.order = {
                selectedRepairs: [],
                userAddress: '',
                userName: '',
                parsedName: {
                    value: ''
                },
                parsedAddress: {},
                userPhone: '',
                userEmail: '',
                createOrderDate: moment().format()
            };
        }
        return this.order;
    },

    setOrderItem(key, value) {
        this.order[key] = value;
        this.storage.setItem(this.orderKey, this.order);
        this.trigger(this.order);
    },

    removeOrderKey(key) {
        delete this.order[key];
        this.storage.setItem(this.orderKey, this.order);
        this.trigger(this.order);
    },

    onGetUserGeoLocation() {
        this.trigger({geolocationLoading: true});
        Navigator.getUserAddress().then((address) => {
            if(this.order.userAddress.length === 0){
                this.order.userAddress = address;
            }
            this.onSetLastInputAddress(address);
            this.trigger({
                geolocationLoading: false,
                geolocationError: false
            });
        }, () => {
            this.trigger({
                geolocationLoading: false
            });
        });
    },

    onUpdateUserAddress(lastUserInputAddress, parsedAddress) {
        var data = {'query': lastUserInputAddress, 'count': 5};
        if ( !parsedAddress || (!!parsedAddress || parsedAddress.raw.toUpperCase() !== lastUserInputAddress.toUpperCase())){
            Utils.ajaxDadataPost({
                url: 'https://dadata.ru/api/v2/suggest/address',
                data: data
            }).then((response) => {
                if (response.suggestions.length !== 0){
                    var houseType = response.suggestions[0].data.house_type,
                        flatType = response.suggestions[0].data.flat_type ? response.suggestions[0].data.flat_type + ' ' : '',
                        blockType = response.suggestions[0].data.block_type,
                        block = response.suggestions[0].data.block,
                        house = response.suggestions[0].data.house,
                        houseBlockArray = [houseType, house, blockType, block],
                        houseBlock = houseBlockArray.filter((item) => { return !!item; }).join(' ');
                    parsedAddress = {
                        raw: lastUserInputAddress,
                        country: response.suggestions[0].data.country || '',
                        region: response.suggestions[0].data.region || '',
                        city: response.suggestions[0].data.city || response.suggestions[0].data.settlement || '',
                        'zip_code': response.suggestions[0].data.postal_code || '',
                        street: response.suggestions[0].data.street || '',
                        house: houseBlock,
                        appartment: response.suggestions[0].data.flat ? (flatType + response.suggestions[0].data.flat) : '' //eslint-disable-line space-infix-ops
                    };
                } else {
                    parsedAddress = {};
                }
                this.setOrderItem('userAddress', lastUserInputAddress);
                this.setOrderItem('parsedAddress', parsedAddress);
                this.trigger({'addressUpdated': true});
            }).fail(() => {
                this.setOrderItem('userAddress', lastUserInputAddress);
                this.setOrderItem('parsedAddress', {});
                this.trigger({'addressUpdated': true});
            });
        } else {
            parsedAddress.raw = lastUserInputAddress;
            this.setOrderItem('userAddress', parsedAddress.raw);
            this.setOrderItem('parsedAddress', parsedAddress);
            this.trigger({'addressUpdated': true});
        }
    },

    onSetAsSoonAsPossible() {
        this.setOrderItem('isSoonAsPossible', true);
        this.removeOrderKey('startDateTime');
        this.removeOrderKey('endDateTime');
        location.hash = '#/order/step3';
    },

    onSetOwnPhoneNumber() {
        this.setOrderItem('phoneType', 'OWN');
    },

    onSetFriendPhoneNumber() {
        this.setOrderItem('phoneType', 'FRIEND');
    },

    onSetLastInputAddress (value) {
        this.setOrderItem('lastInputAddress', value);
    },

    onSetUserName(value) {
        this.setOrderItem('userName', value);
    },

    onSetParsedName(value) {
        this.setOrderItem('parsedName', value);
    },

    onSetParsedAddress(value) {
        this.setOrderItem('parsedAddress', value);
    },

    onSetUserPhone(value) {
        this.setOrderItem('userPhone', value);
    },

    onSetUserEmail(value) {
        this.setOrderItem('userEmail', value);
    },

    onSetStartDateTime(value) {
        this.setOrderItem('startDateTime', value);
        this.removeOrderKey('isSoonAsPossible');
    },

    onSetEndDateTime(value) {
        this.setOrderItem('endDateTime', value);
        this.removeOrderKey('isSoonAsPossible');
    },

    onSetModelCache(value) {
        this.setOrderItem('modelCache', value);
    },

    setGender(value) {
        this.setOrderItem('userGender', value);
    },

    trackOnFb() {
        const amount = _.reduce(this.order.selectedRepairs, (memo, obj) => {return memo + obj.price; }, 0);
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6029135709402', {'value':amount,'currency':'RUB'}]);
    },

    createOrder(notFinish) {
        let url = this.placeOrderUrl;
        if (notFinish) {
            url = '/api/v1/not-finished-order/'
        }
        Utils.ajaxPost({
            url: url,
            data: {...this.order, type: this.orderKey}
        }).then((response) => {
            if (notFinish) {
                this.trigger({notFinishedOrderSent: true});
            } else {
                if (response.repeated_order_coupon) {
                    this.setOrderItem('repeated_order_coupon', true);
                } else if (this.order.repeated_order_coupon) {
                    this.removeOrderKey('repeated_order_coupon');
                }
                if (response.comment_key) {
                    this.setOrderItem('commentKey', response.comment_key);
                }
                this.trackOnFb();
                this.onCleanOrder();
                location.hash = '#/order/success';
            }
        }).fail(() => {
            if (notFinish){
                this.trigger({notFinishedOrderSent: true});
            } else {
                location.hash = '#/order/server-problem';
            }
        });
    },

    onPlaceOrder(notFinish=false) {
        var query = {'query': this.order.userName, 'count': 5};
        Utils.ajaxDadataPost({
            url: 'https://dadata.ru/api/v2/suggest/fio',
            data: query
        }).then( (response) => {
            if (response.suggestions.length !== 0) {
                var suggestion = response.suggestions[0];
                this.setGender(suggestion.data.gender.toLowerCase());
            }
            this.onSetParsedName({
                firstName: this.order.userName,
                value: this.order.userName
            });
            this.createOrder(notFinish);
        }).fail( () => {
            this.onSetParsedName({
                firstName: this.order.userName,
                value: this.order.userName
            });
            this.createOrder(notFinish);
        });
    },

    onSetShowAllDevices() {
        this.setOrderItem('isAllDisplayed', true);
    },

    removeCoupon() {
        this.setOrderItem('coupon', null);
    },

    removeRepeatedOrderCoupon() {
        this.setOrderItem('repeatedOrderCoupon', null);
    },

    applyCoupon(coupon) {
        this.setOrderItem('coupon', coupon);
    },

    applyRepeatedOrderCoupon(coupon) {
        this.setOrderItem('repeatedOrderCoupon', coupon);
    },

    getDataForCoupon() {
        return {
            repairs: _.pluck(this.order.selectedRepairs, 'repair_id').join(','),
            modelSlug: this.order.userModel
        };
    },

    checkCouponCode(code) {
        let data = this.getDataForCoupon();
        data = {code: code, ...data};
        Utils.ajaxGet({
            url: `/orders/check-coupon/`,
            data: data,
            decamelize: true
        }).then((response) => {
            this.applyCoupon({code: code, discount: response.discount, percent: response.percent});
            this.trigger({
                ajaxRequest: false,
                badCode: false
            });
        }).fail(() => {
            this.trigger({
                ajaxRequest: false,
                badCode: true
            });
        });
    },

    checkForRepeatedOrderCoupon() {
        Utils.ajaxGet({
            url: `/orders/check-repeated-order-coupon/`,
        }).then((response) => {
            this.applyRepeatedOrderCoupon({discount: response.discount, percent: response.percent});
            this.trigger({
                ajaxRequest: false
            });
        }).fail(() => {
            this.removeRepeatedOrderCoupon();
            this.trigger({
                ajaxRequest: false
            });
        })
    },

    saveComment(comment) {
        let data = {
            comment: comment,
            commentKey: this.order.commentKey
        }
        Utils.ajaxPost({
            url: `/orders/save-comment/`,
            data:data,
            decamelize: true
        }).then(() => {
            this.trigger({
                commentSaved: true,
                ajaxRequest: false
            });
        }).fail(() => {
            this.trigger({
                ajaxRequest: false
            });
        });
    }
};

module.exports = baseStore;
