/* global jQuery */

import _ from 'underscore';
import reqwest from 'reqwest';
import decamelize from 'decamelize';

var Utils = {
    validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    },
    getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    },

    hashCode (str){
        let hash = 0;
        if (str.length == 0) return hash;
        for (let i = 0; i < str.length; i++) {
            let char = str.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    },

    decamelizeObjectKeys(object) {
        let newObject = {};
        _.each(_.keys(object), (key)=> {
            newObject[decamelize(key)] = object[key];
        });
        return newObject;
    },

    ajaxReqwest(params) {
        if (params.decamelize) {
            params.data = this.decamelizeObjectKeys(params.data);
        }
        if (params.stringify) {
            params.data = JSON.stringify(params.data);
        }
        var newParams = _.extend(params, {
            headers: {
                'X-CSRFToken': this.getCookie('csrftoken')
            },
            contentType: 'application/json',
            type: 'json'
        });
        return reqwest(newParams);
    },

    ajaxPut(params) {
        params.stringify = true;
        var newParams = _.extend(params, {method: 'put'});
        return this.ajaxReqwest(newParams);
    },

    ajaxPost(params) {
        params.stringify = true;
        var newParams = _.extend(params, {method: 'post'});
        return this.ajaxReqwest(newParams);
    },

    ajaxDelete(params) {
        var newParams = _.extend(params, {method: 'delete'});
        return this.ajaxReqwest(newParams);
    },

    ajaxPatch(params) {
        params.stringify = true;
        var newParams = _.extend(params, {method: 'patch'});
        return this.ajaxReqwest(newParams);
    },

    ajaxGet(params) {
        var newParams = _.extend(params, {method: 'get'});
        return this.ajaxReqwest(newParams);
    },

    ajaxDadataPost(params) {
        params.data = JSON.stringify(params.data);
        var newParams = _.extend(params, {
            method: 'post',
            type: 'json',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/json',
                'Authorization': 'Token ' + window.DADATA_TOKEN
            }
        });
        return reqwest(newParams);
    },

    getParam(val) {
        var result = null,
            tmp = [];

        location.search
        .substr(1)
            .split('&')
            .forEach(function (item) {
            tmp = item.split('=');
            if (tmp[0] === val) {
                result = decodeURIComponent(tmp[1]);
            }
        });
        return result;
    }
};

module.exports = Utils;
