/* globals gettext */

import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Loading from '../../common/components/loading.js';
import Money from '../../common/components/money.js';

var TableRow = React.createClass({
    getOrderLink(orderId) {
        return `/master/orders/#/order/${orderId}`;
    },
    render() {
        return (
            <tr>
                <td>
                    {this.props.transaction.repair_order_id && (
                        this.props.showOrderLink ? (
                            <a href={this.getOrderLink(this.props.transaction.repair_order_id)}>#{this.props.transaction.repair_order_id}</a>
                        ) : <span>#{this.props.transaction.repair_order_id}</span>
                    )}
                    <span className="noneIfPC">{this.props.transaction.created_at}</span>
                </td>
                <td className="noneIfMob"><span>{this.props.transaction.created_at}</span></td>
                <td className="transAmount">{this.props.transaction.sender ?
                    <span className="red">-<Money amount={this.props.transaction.amount} currency={this.props.transaction.currency}/></span> :
                    <span className="green">+<Money amount={this.props.transaction.amount} currency={this.props.transaction.currency}/></span> }</td>
                <td><span>{this.props.transaction.description}</span></td>
            </tr>
        );
    }
});

var Pagination = React.createClass({
    handlePrev() {
        var offset = this.props.offset - this.props.limit;
        if (offset < 0) {
            offset = 0;
        }
        actions.setFilterItem('offset', offset);
    },
    handleNext() {
        var offset = this.props.offset + this.props.limit;
        if (offset <= this.props.count) {
            actions.setFilterItem('offset', offset);
        }
    },

    render() {
        if (this.props.count > this.props.limit) {
            var idDisabledNext = (this.props.offset + this.props.limit) > this.props.count;
            var idDisabledPrev = (this.props.offset - this.props.limit) < 0;
            return (
                <div className='pagination'>
                    {!idDisabledPrev && <button className='coverpage-button' onClick={this.handlePrev}>&lt;&lt;</button>}
                    {!idDisabledNext && <button className='coverpage-button' onClick={this.handleNext}>&gt;&gt;</button>}
                </div>
            );
        } else {
            return null;
        }
    }
});

var Table = React.createClass({
    render() {
        return (
            <div>
                { this.props.transactions.count
                    ? (
                        <div>
                            <table cellSpacing='0' className='table transactionTable'>
                                <thead></thead>
                                <tbody>
                                    {this.props.transactions.results.map((transaction) => {
                                        return <TableRow transaction={transaction} showOrderLink={this.props.showOrderLink}/>;
                                    })}
                                </tbody>
                            </table>
                            <Pagination count={this.props.transactions.count} page={this.props.page} limit={this.props.limit} offset={this.props.offset}/>
                        </div>
                    ) : (
                        <div>{this.props.emptyDataText || gettext('Нет данных')}</div>
                    )}
            </div>
        );
    }
});


var TransactionsTable = React.createClass({
    mixins: [Reflux.connect(store)],
    getDefaultProps() {
        return {
            title: gettext('Движение денег'),
            showOrderLink:true
        }
    },

    getFilter() {
        return {};
    },

    componentWillMount() {
        actions.getTransactions(this.getFilter());
    },

    render() {
        const tableProps = {
            transactions: this.state.transactions,
            page: this.state.page,
            limit: this.state.limit,
            offset: this.state.offset,
            showOrderLink: this.props.showOrderLink,
        }
        return (
            <div className='content step orderTable'>
                <h3>{this.props.title}</h3>
                <div className='container'>
                    {this.state.success ? <Table {...tableProps}/> : <Loading />}
                </div>
            </div>
        );
    }

});

module.exports = TransactionsTable;
