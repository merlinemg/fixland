import Reflux from 'reflux';
import Storage from '../common/storage';
import actions from './actions';
import Utils from '../common/utils';

var storage = new Storage();


var store = Reflux.createStore({
    listenables: [actions],
    storage: storage,
    filter: {},

    getInitialState() {
        this.filter = this.storage.getItem('filter');
        if (!this.filter) {
            this.filter = {
                limit: 10,
                offset: 0
            };
        }
        return this.filter;
    },

    onSetFilterItem(key, value) {
        this.filter[key] = value;
        this.storage.setItem('filter', this.filter);
        this.trigger(this.filter);
        this.onGetTransactions();
    },

    onGetTransactions() {
        Utils.ajaxGet({
            url: '/api/v1/transactions/',
            data: this.filter
        }).then((response) => {
            this.trigger({success: true, transactions: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    }
});

module.exports = store;
