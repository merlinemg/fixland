/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import _ from 'underscore';
import BreadcrumbsMixin from '../../breadcrumbs/breadcrumbsMixin';
import utils from '../utils';

var Breadcrumbs = React.createClass({
    mixins: [Reflux.connect(store), BreadcrumbsMixin],
    getPrice() {
        let model = _.findWhere(_.findWhere(window.DEVICES, {slug: this.state.userDevice}).models, {slug: this.state.userModel});
        let grouped_repairs = model.grouped_repairs;
        if (!grouped_repairs.length) {
            return _.reduce(this.state.selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
        } else {
            let selectedRepairs = this.state.selectedRepairs;
            _.forEach(grouped_repairs, (grouped_repair) => {
                let items_in_group = _.filter(this.state.selectedRepairs, (item) => {
                    return grouped_repair.repair_list.indexOf(item.repair_id) != -1;
                });
                if (items_in_group.length === grouped_repair.repair_list.length) {
                    selectedRepairs = _.without(selectedRepairs, ...items_in_group);
                    selectedRepairs.push(grouped_repair);
                }
            });
            let price = _.reduce(selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
            window.orderPrice = price.replace("'","");
            return price;
        }
    },
    getFullDevice() {
        var params = this.props.params;
        var deviceSlug, modelSlug, colorSlug;
        if (['orderStep1', 'orderStep2', 'orderStep3', 'regionDiff'].indexOf(this.props.route) !== -1) {
            deviceSlug = this.state.userDevice;
            modelSlug = this.state.userModel;
            colorSlug = this.state.userColor;
        } else {
            deviceSlug = params.device;
            modelSlug = params.model;
            colorSlug = params.color;
        }
        var device = _.findWhere(window.DEVICES, {slug: deviceSlug}),
            model = _.findWhere(device.models, {slug: modelSlug}),
            color = _.findWhere(model.colors, {slug: colorSlug});
        return {
            title: gettext('Устройство'),
            text: `${[device.name, model.name, color.name].join(' ')}`,
            link: `/${device.slug}/${model.slug}/${color.slug}`
        };
    }
});

module.exports = Breadcrumbs;
