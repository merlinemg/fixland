/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import addons from 'react-addons';
import store from '../store';
import actions from '../actions';
import Utils from '../../common/utils';
import ErrorMessage from '../../common/components/errorMessage'
import Loading from '../../common/components/loading'



module.exports = React.createClass({
    mixins: [Reflux.connect(store), addons.LinkedStateMixin],
    getInitialState() {
        return {
            comment: null
        }
    },
    showCommentInput() {
        this.setState({isShowCommentInput: true});
    },
    showRepeatedOrder() {
        return this.state.repeated_order_coupon;
    },
    validateComment() {
        return this.state.comment && this.state.comment.length;
    },
    saveComment() {
        if (this.validateComment()) {
            this.setState({ajaxRequest: true});
            actions.saveComment(this.state.comment);
        } else {
            this.setState({showError: true});
        }
    },
    componentDidMount() {
        // facebook pixel
        if (window.is_production) {
            fbq('track', 'Purchase', {value: window.orderPrice, currency:'RUB'});
        }
    },
    render() {
        return (
            <div className="content step">
                <h3>{gettext('Спасибо!')}</h3>
                <br/>
                <h4 className="aboutMaster textLighter">{gettext('Наш мастер свяжется с Вами по указанному номеру телефона в течение 20 минут в рабочее время')}</h4>
                <br/>
                {!this.state.commentSaved && <div>
                    {this.state.isShowCommentInput ? (
                        <div className='wrapperDataInput'>
                            <textarea className="inputData feedtext" maxLength='255' valueLink={this.linkState('comment')} placeholder={gettext('Укажите удобное время для связи с Вами по телефону')}></textarea>
                            {this.state.showError && !this.validateComment() ? <ErrorMessage message='Введите удобное время для связи' /> : null}
                            <br/>
                            {this.state.ajaxRequest ? <Loading /> : <button className="button button-green selectRepair fontSize15" onClick={this.saveComment}>{gettext('Готово')}</button>}
                        </div>
                    ) : (
                        <div>
                            <h4 className="aboutMaster">{gettext('Вы можете указать удобное время')}</h4>
                            <button className="button button-green selectRepair fontSize15" onClick={this.showCommentInput}>{gettext('Ввести время')}</button>
                        </div>
                    )}
                </div>}
                <br/>
                {this.showRepeatedOrder() && !window.is_partner_user &&
                    <div className="btnSuccessOrder">
                        <a className="button button-blue selectRepair fontSize15" href="/repeate_repair">{gettext('Еще заказ со скидкой')}</a>
                        <br/>
                    </div>
                }
                <h4 className="aboutMaster textLighter"><span className="colorGreen boldText">{gettext('ВНИМАНИЕ')}:</span> {gettext('3х-летняя гарантия Fixland.ru распространяется только на запчасти и работы, заказанные через сайт. Если Вы договариваетесь с мастером напрямую, то на такие работы и запчасти гарантия не распространяется!')}</h4>

            </div>
        );
    }
});
