import actions from '../actions';
import React from 'react';
import Reflux from 'reflux';
import store from '../store';

import CouponMixin from '../../common/components/couponMixin';

module.exports = React.createClass({
    actions: actions,
    mixins: [Reflux.connect(store), CouponMixin]
});
