import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import actions from '../actions';

import OrderStep3Mixin from '../../common/components/orderStep3Mixin';
import RepairsRequiredMixin from '../mixins';
import Coupon from './coupon';


var OrderStep3 = React.createClass({
    actions: actions,
    Сoupon: Coupon,
    mixins: [
        Reflux.connect(store),
        Reflux.listenTo(store, 'storeTrigger'),
        OrderStep3Mixin,
        Router.Navigation,
        RepairsRequiredMixin
    ],
    storeTrigger(data) {
        if (data.notFinishedOrderSent) {
            this.transitionTo('orderStep2');
        }
    }
});

module.exports = OrderStep3;
