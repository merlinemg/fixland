/* globals gettext */

import _ from 'underscore';
import React from 'react';
import Router from 'react-router';
import DevicePhoto from '../../common/components/devicePhoto.js';


var Link = Router.Link;

var SelectColor = React.createClass({
    getInitialState() {
        var params = this.props.params;
        return {
            colors: _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).colors,
            hover: false
        };
    },
    getParams(color) {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            color: color.slug
        };
    },
    mouseOver: function () {
        this.setState({hover: true});
    },

    mouseOut: function () {
        this.setState({hover: false});
    },
    render() {
        return (
            <div className="content step">
                <h3>{gettext('Выберите цвет')}</h3>
                {this.state.colors.map(color => {
                    var styleBgColor = {
                      'background-color': `#${color.hex}` + `!important`,
                      opacity: 1,
                      color: `#000`
                    };

                    if (this.state.hover) {
                        styleBgColor = {
                            'background-color': `#${color.hex}` + `!important`,
                            opacity: 1,
                            color: `#000`
                        };
                    }

                    var linkStyles = {
                        color: `#${color.text_color}` + `!important`
                    };

                    if (this.state.hover) {
                        linkStyles = {
                          color: `#${color.text_color}` + `!important`
                        };
                    }
                    return (
                        <div className="devColorWrp">
                            <Link onMouseOver={this.mouseOver} onMouseOut={this.mouseOut} to='selectRepair' params={this.getParams(color)} style={linkStyles} >
                                <DevicePhoto photo={color.model_photo}/>
                            </Link>
                            <div onMouseOver={this.mouseOver} onMouseOut={this.mouseOut} style={styleBgColor} className="deviceColor selectBtn">
                                <Link onMouseOver={this.mouseOver} onMouseOut={this.mouseOut} to='selectRepair' params={this.getParams(color)} style={linkStyles} >
                                    {color.name}
                                </Link>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
});

module.exports = SelectColor;
