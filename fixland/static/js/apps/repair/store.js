import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';
import Utils from '../common/utils';

import baseStore from '../common/store';

var store = Reflux.createStore({
    placeOrderUrl: '/orders/repair/',
    listenables: [actions],
    orderKey: 'repairOrder',
    mixins: [baseStore],

    onCleanOrder() {
        this.storage.removeItem(this.orderKey);
        this.storage.setItem(this.orderKey, _.extend(this.order, {
            selectedRepairs: [],
            startDateTime: null,
            endDateTime: null,
            isSoonAsPossible: null,
            userDevice: null,
            userModel: null,
            userColor: null,
            modelCache: null,
            coupon: null,
            repeatedOrderCoupon: null,
            partnerCode: null,
        }));
    },

    onCleanRepairs() {
        this.setOrderItem('selectedRepairs', []);
    },

    onUpdateRepair(repair) {
        var selectedRepairs = this.order.selectedRepairs;
        if (_.findWhere(selectedRepairs, {'slug': repair.slug}) !== undefined) {
            var newRepairs = _.filter(selectedRepairs, function(item) {
                return item.slug !== repair.slug;
            });
            selectedRepairs = newRepairs;
        } else {
            selectedRepairs.push(repair);
        }
        this.setOrderItem('selectedRepairs', selectedRepairs);
        this.removeCoupon();
    },

    onSetDevice(data) {
        if (this.order.userModel != data.model) {
            this.removeCoupon();
        }
        this.setOrderItem('userDevice', data.device);
        this.setOrderItem('userModel', data.model);
        this.setOrderItem('userColor', data.color);
    }

});

module.exports = store;
