import Reflux from 'reflux';

let actions = Reflux.createActions({
    'setFormItem': {},
    'findOrders': {sync: true, children: ['completed','failed']},
    'fillOrder': {sync: true, children: ['completed','failed']},
    'updateRepair': {},
    'updateUserAddress': {},
    setParsedAddress: {},
    setLastInputAddress: {},
    'setAsSoonAsPossible': {},
    'setStartDateTime': {},
    'setEndDateTime': {},
    'cleanOrder': {},
    'selectAsSoonAsPossible': {sync: true, children: ['completed','failed']},
    'createWarantyOrder': {sync: true, children: ['completed','failed']},
});

module.exports = actions;
