/* globals gettext */

import actions from '../actions';
import React from 'react';
import addons from 'react-addons';
import Reflux from 'reflux';
import Router from 'react-router';
import _ from 'underscore';

import orderStore from '../orderStore';
import Money from '../../common/components/money';
import SelectRepair from '../../common/components/selectRepair';

let Link = Router.Link;
let cx = addons.classSet;

let ButtonNext = React.createClass({
    render() {
        let classes = cx({
            'sellRapair-btn': true,
            'sellRapair-btn-nonActive': !this.props.active
        });
        return (
            <Link className={classes} to='selectTime'>{gettext('Далее')}</Link>
        )
    }
});

module.exports = React.createClass({
    mixins: [
        Reflux.connect(orderStore),
        Router.Navigation
    ],
    componentWillMount() {
        if (!this.state.userModel) {
            this.transitionTo('warranty');
        }else{
            this.model = _.findWhere(_.findWhere(window.DEVICES, {slug: this.state.userDevice}).models, {slug: this.state.userModel});
        }
    },
    isButtonNextActive() {
        return this.state.selectedRepairs && this.state.selectedRepairs.length > 0 ? true : false;
    },
    render() {
        let selectRepairProps = {
            model: this.model,
            selectedRepairs: this.state.selectedRepairs,
            showSelectedOnly: true,
            showPrice: false,
            showAllRepairsTitle: gettext('Сломалась что-то другое?'),
            repairSelected: actions.updateRepair,
        }
        return (
            <div className="content step wrpSelRepair">
                <h3>{gettext('Что у вас сломалось?')}</h3>
                <SelectRepair {...selectRepairProps}/>
                <ButtonNext active={this.isButtonNextActive()}/>
            </div>
        );
    }
});
