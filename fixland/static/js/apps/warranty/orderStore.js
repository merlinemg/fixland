import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';
import moment from 'moment';

import baseStore from '../common/store';
import Utils from '../common/utils';

let store = Reflux.createStore({
    placeOrderUrl: '/orders/warranty-repair/',
    listenables: [actions],
    orderKey: 'warrantyOrder',
    mixins: [baseStore],

    cleanOrder() {
        this.order = {};
        this.storage.removeItem(this.orderKey);
    },

    updateRepair(repair) {
        let selectedRepairs = this.order.selectedRepairs || [];
        if (_.findWhere(selectedRepairs, {'slug': repair.slug}) !== undefined) {
            let newRepairs = _.filter(selectedRepairs, function(item) {
                return item.slug !== repair.slug;
            });
            selectedRepairs = newRepairs;
        } else {
            selectedRepairs.push(repair);
        }
        this.setOrderItem('selectedRepairs', selectedRepairs);
        this.removeCoupon();
    },
    selectAsSoonAsPossible() {
        this.setOrderItem('isSoonAsPossible', true);
        this.removeOrderKey('startDateTime');
        this.removeOrderKey('endDateTime');
        actions.selectAsSoonAsPossible.completed();
    },
    fillOrder(data) {
        this.setOrderItem('number', data.number);
        this.setOrderItem('userDevice', data.device);
        this.setOrderItem('userModel', data.model);
        this.setOrderItem('userColor', data.color);
        this.setOrderItem('lastInputAddress', data.address);
        this.setOrderItem('as_soon_as_possible', data.as_soon_as_possible);
        if (!data.as_soon_as_possible) {
            this.setOrderItem('starttime', data.starttime);
            this.setOrderItem('endtime', data.endtime);
        }
        let model = _.findWhere(_.findWhere(window.DEVICES, {slug: data.device}).models, {slug: data.model});
        _.each(data.repairs, repair_id => {
            let model_repair = _.findWhere(model.repairs, {key: repair_id});
            if (model_repair) this.updateRepair(model_repair);
        });
        this.setOrderItem('createOrderDate', moment().format());
        actions.fillOrder.completed();
    },
    createWarantyOrder() {
        Utils.ajaxPost({
            url: '/orders/repair_warranty/',
            data: this.order,
        }).then((response) => {
            this.cleanOrder();
            actions.createWarantyOrder.completed();
        }).fail(() => {
            actions.createWarantyOrder.failed();
        });
    }
});

module.exports = store;
