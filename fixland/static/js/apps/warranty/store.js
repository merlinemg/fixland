import Reflux from 'reflux';
import actions from './actions';
import Utils from '../common/utils';

var store = Reflux.createStore({
    listenables: [actions],
    form: null,
    submited: false,
    getInitialState() {
        if (!this.form) {
            this.form = {
                phone: null,
                email: null,
            }
        }
        return {...this.form, errors: []};
    },
    setFormItem(name, value) {
        this.form[name] = value;
        if (this.submited) {
            this.validate(true);
        }
    },
    validate(trigger) {
        let result = {
            isValid: true,
            errors: {}
        };

        if (this.form.email && !Utils.validateEmail(this.form.email)) {
            result.isValid = false;
            result.errors['email'] = gettext('Неверный email');
        }
        if (!this.form.phone || this.form.phone.length !== 16) {
            result.isValid = false;
            result.errors['phone'] = gettext('Неверный телефон');
        }
        if (trigger) {
            this.trigger({errors: result.errors});
        } else {
            return result;
        }
    },
    findOrders() {
        this.submited = true;
        let result = this.validate();
        if (result.isValid) {
            this.trigger({ajax: true});
            Utils.ajaxGet({
                url: '/api/v1/repair-orders/find-orders/',
                data: {
                    phone: this.form.phone,
                    email: this.form.email
                }
            }).then((response) => {
                actions.findOrders.completed();
                this.trigger({orders: response, ajax: false});
            }).fail(() => {
                actions.findOrders.failed();
                this.trigger({ajax: false});
            });
        } else {
            this.trigger({errors: result.errors});
        }
    }
});

module.exports = store;
