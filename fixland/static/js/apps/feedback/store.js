/* globals jQuery */

import actions from './actions';
import Reflux from 'reflux';
import Storage from '../common/storage';
import Utils from '../common/utils';

var storage = new Storage();

var store = Reflux.createStore({
    storage: storage,
    listenables: [actions],

    onCleanFeedBack() {
        this.storage.removeItem('feedBack');
    },

    getInitialState() {
        this.feedBack = this.storage.getItem('feedBack');
        if (!this.feedBack) {
            this.feedBack = {
                name: '',
                contact: '',
                feedtext: ''
            };
        }
        this.feedBack.errors = [];
        return this.feedBack;
    },

    setFeedBackItem(key, value) {
        this.feedBack[key] = value;
        this.storage.setItem('feedBack', this.feedBack);
        this.trigger(this.feedBack);
    },

    onSetName(value) {
        this.setFeedBackItem('name', value);
    },

    onSetContact(value) {
        this.setFeedBackItem('contact', value);
    },

    onSetFeedtext(value) {
        this.setFeedBackItem('feedtext', value);
    },

    sendFeedBack() {
        var data = jQuery.extend(true, {}, this.feedBack);
        Utils.ajaxPost({
            url: '/create_feedback/',
            data: data
        }).then(() => {
            actions.cleanFeedBack();
            location.hash = '#/success';
        }).fail(() => {
            location.hash = '#/server-problem';
            this.setFeedBackItem('ajaxRequest', false);
        });
    }
});

module.exports = store;
