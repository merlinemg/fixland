(function($) {
    $(document).ready(function($) {
        $('form').on('submit', function(event) {
            var contactData = $('#id_contact').val();
            var isTelephone = contactData.indexOf('@') == -1;
            if (isTelephone){
                var feedbackLength = $('#id_feedback_answer').val().length;
                if (feedbackLength>480) {
                    var smsCount = Math.ceil(feedbackLength/120);
                    var message = 'Сообщение будет состоять из '+ smsCount+' СМС. Отправить?';
                    if (confirm(message)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }.bind($));
    })
})(django.jQuery)