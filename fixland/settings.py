# coding: utf-8
"""
Django settings for fixland project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
from __future__ import absolute_import
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import logging

from decimal import Decimal

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get(
    'SECRET_KEY',
    'knh+u5-a7eg240pkm1kr_eai(b#+3=jrqq)hyyzt9%yixufo&5'
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
IS_PRODUCTION = False

TEMPLATE_DEBUG = False

HOST_URL = 'fixland.pro'

ALLOWED_HOSTS = [
    # 'fixland.lyapun.com',
    # 'fixland-release.lyapun.com',
    'fixland.ru',
    'fixland.pro',
    '127.0.0.1',
]


# Application definition

INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli_nested',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_jenkins',
    'sitegate',
    'rest_framework',
    'simple_seo',
    'rosetta',
    'modeltranslation',
    'statici18n',
    'loginas',
    'constance',
    'report_builder',
    'tinymce',
    'explorer',
    'easy_pdf',

    'compressor',
    'accounts',
    'devices',
    'orders',
    'masters',
    'core',
    'integrations',
    'partners',
)

SEO_MODEL_REGISTRY = (
    ('core.MyMetaData', 'ALL'),
)

# SEO_USE_CACHE = True

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'solid_i18n.middleware.SolidLocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'partners.middleware.PartnerMiddleware'
)

ROOT_URLCONF = 'fixland.urls'

WSGI_APPLICATION = 'fixland.wsgi.application'


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'fixland/templates'),
)

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "fixland/static"),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.template.context_processors.request',
    'django.template.context_processors.debug',
    'django.template.context_processors.i18n',
    'django.template.context_processors.media',
    'django.template.context_processors.static',
    'django.template.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'fixland.context_processors.add_settings',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'constance.context_processors.config',
    'partners.context_processors.is_partner_from_link_exists',
)

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fixland',
        'USER': "postgres",
        'PASSWORD': "qwerty",
        'PORT': '5432',
        'HOST': 'localhost',
        'STORAGE_ENGINE': 'MYISAM',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'

# TIME_ZONE = 'Europe/Moscow'
TIME_ZONE = 'America/New_York'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
    # os.path.join(BASE_DIR, 'fixland', 'static', 'js', 'dist', 'locale'),
]

# STATICI18N_PACKAGES = (
#     'fixland',
# )

SOLID_I18N_USE_REDIRECTS = True

ROSETTA_MESSAGES_PER_PAGE = 50
ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
ROSETTA_GOOGLE_TRANSLATE = True
ROSETTA_MESSAGES_SOURCE_LANGUAGE_CODE = 'ru'

MODELTRANSLATION_DEFAULT_LANGUAGE = 'ru'
MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'ru'

AUTH_USER_MODEL = 'accounts.User'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')
STATIC_FULL_PATH = os.path.join(BASE_DIR, 'fixland/static')

EMAIL_SUBJECT_PREFIX = '[Fixland] '
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

ADMINS = (
    ('Roman Bondar', 'rombr5@gmail.com'),
)

BYTEHAND_URL = 'http://bytehand.com:3800/send'
BYTEHAND_URL_MULTI = 'http://bytehand.com:3800/send_multi'
SEND_SMS_TO = []
BYTEHAND_ID = None
BYTEHAND_KEY = None
BYTEHAND_FROM = None

GOOGLE_MAPS_API_KEY = None

GOOGLE_LOCATION_API_KEY ='AIzaSyDXrVM3srUWhc7_iETaTaZLapjn5sjH42g'

IS_USA = True
DADATA_TOKEN = '8e2a739dcd26019ff651ea9b5085d60d74e68ec2'

YANDEX_MAPS_API_URL = 'https://geocode-maps.yandex.ru/1.x/'
DADATA_ADDRESS_API_URL = 'https://dadata.ru/api/v2/suggest/address/'

GOOGLE_ADDRESS_API_URL = 'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false'
RADIUS = 3000

JENKINS_TASKS = (
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pyflakes',
)

RAVEN_CONFIG = {}

SENTRY_PUBLIC_DSN = None
IS_SERVER = False

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
        'rest_framework.filters.OrderingFilter',
    ),
    'DEFAULT_PAGINATION_CLASS': (
        'rest_framework.pagination.LimitOffsetPagination'
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),
}

GRAPPELLI_ADMIN_TITLE = HOST_URL
GRAPPELLI_AUTOCOMPLETE_LIMIT = 10
GRAPPELLI_SWITCH_USER = True
GRAPPELLI_INDEX_DASHBOARD = 'fixland.dashboard.CustomIndexDashboard'

REPAIR_ORDER_COMMISSION = 300

LOGIN_URL = '/login/'

CELERY_TIMEZONE = 'UTC'
BROKER_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379/0')
CELERY_RESULT_BACKEND = BROKER_URL
BROKER_TRANSPORT = 'redis'

GEO_IP_CITY_DB = os.path.join(BASE_DIR, 'data/GeoLiteCity.dat')

DEFAULT_USER_AVATAR = '/static/images/pictures/convivence.jpg'

WMI_MERCHANT_ID = os.environ.get('WMI_MERCHANT_ID', '')
W1_SECRET_KEY = os.environ.get('W1_SECRET_KEY', '')
MIN_PAYMENT_AMOUNT = 1
MAX_PAYMENT_AMOUNT = 15000

CURRENCY_RUB = 'rub'
CURRENCY_USD = 'usd'
CURRENCIES = (
    (CURRENCY_RUB, 'rub'),
    (CURRENCY_USD, 'usd'),
)

ORDER_REVIEW_BONUS_TEL_KEY = 1
ORDER_REVIEW_BONUS_COUPON_KEY = 2

CONSTANCE_CONFIG = {
    'SEND_SMS': (True, 'Отправлять смски?'),
    'DEFAULT_PAYMENT_AMOUNT': (100, 'Сумма пополнения по умолчанию'),
    'ONEDRIVE_DATA_XLS_LINK': ('https://onedrive.live.com/download?resid=B2D27C483900D103!17634&&authkey=!AI9PM7CXqGZdyqw', 'Ссылка на excel файл для импорта'),
    'ONEDRIVE_ARTICLES_XLS_LINK': ('https://onedrive.live.com/download?resid=B2D27C483900D103!17845&authkey=!AuEH7mquekcLaIQ', 'Ссылка на excel файл для импорта артикулов поставщиков'),
    'MAILGUN_API_KEY': ('', 'API ключ от mailgun'),
    'MAILGUN_DOMAIN': ('', 'Mailgun доменное имя'),
    'REPAIR_RATING_DURATION': (90, 'Кол-во дней начисления рейтинга за отремонтированные поломки'),
    'REVIEW_RATING_DURATION': (90, 'Кол-во дней начисления рейтинга за оценки'),
    'REJECTED_COEF_DURATION': (90, 'Кол-во дней коэфициента отказов'),
    'REPAIR_RATING_IPHONE_3': (120, 'Рейтинг за почику поломки iphone 3-ки'),
    'REPAIR_RATING_IPHONE_4': (120, 'Рейтинг за почику поломки iphone 4-ки'),
    'REPAIR_RATING_IPHONE_5': (90, 'Рейтинг за почику поломки iphone 5-ки'),
    'REPAIR_RATING_IPHONE_6': (70, 'Рейтинг за почику поломки iphone 6-ки'),
    'REPAIR_RATING_IPAD': (150, 'Рейтинг за почику поломки ipad'),
    'REPAIR_RATING_OTHER_DEVICES': (100, 'Рейтинг за почику поломки других устройств'),
    'REVIEW_RATING_FIVE_STAR': (50, 'Рейтинг за 5 звезд'),
    'REVIEW_RATING_FOUR_STAR': (10, 'Рейтинг за 4 звезды'),
    'REVIEW_RATING_THREE_STAR': (-500, 'Рейтинг за 3 звезды'),
    'REVIEW_RATING_TWO_STAR': (-1000, 'Рейтинг за 2 звезды'),
    'REVIEW_RATING_ONE_STAR': (-1000, 'Рейтинг за 1 звезду'),
    'PENALTY_PART_RATING': (-1, 'Рейтинг за покупку запчасти у других поставщиков'),
    'REJECTION_MAX_PERCENTAGE_LIMITS_1': (5, 'Максимальный процент отказов процентной границы 1'),
    'REJECTION_MAX_PERCENTAGE_LIMITS_2': (10, 'Максимальный процент отказов процентной границы 2'),
    'REJECTION_MAX_PERCENTAGE_LIMITS_3': (12, 'Максимальный процент отказов процентной границы 3'),
    'REJECTION_MAX_PERCENTAGE_LIMITS_4': (14, 'Максимальный процент отказов процентной границы 4'),
    'REJECTION_COEF_LIMITS_1': (1, 'Коэф отказа для процентной границы 1'),
    'REJECTION_COEF_LIMITS_2': (Decimal('0.95'), 'Коэф отказа для процентной границы 2'),
    'REJECTION_COEF_LIMITS_3': (Decimal('0.9'), 'Коэф отказа для процентной границы 3'),
    'REJECTION_COEF_LIMITS_4': (Decimal('0.8'), 'Коэф отказа для процентной границы 4'),
    'REJECTION_COEF_LIMITS_5': (Decimal('0.7'), 'Коэф отказа для процентной границы 5'),
    'ORDER_REVIEW_BONUS_TYPE': (ORDER_REVIEW_BONUS_TEL_KEY, 'Бонус за отзыв о заказе ({} - пополнение номера телефона, {} - купон на скидку)'.format(ORDER_REVIEW_BONUS_TEL_KEY, ORDER_REVIEW_BONUS_COUPON_KEY)),
    'ORDER_REVIEW_TEL_BONUS_AMOUNT': (100, 'Сумма бонуса пополнения телефона'),
    'COUPON_DISCOUNT': (100, 'Сумма скидки при использовании купона'),
    'MASTER_CREDIT_DAYS': (5, 'Количество дней на использование кредита'),
    'FEEDBACK_TELEPHONE': ('+7(499)643-4629', 'Телефон для Обратной связи'),
    'SECOND_NEW_ORDER_NOTIFICATION_TIME': (10, 'Интервал повторного поиска мастеров'),
    'LAST_NEW_ORDER_NOTIFICATION_TIME': (20, 'Интервал последнего поиска мастеров'),
    'FIRST_NEW_ORDER_NOTIFICATION_QUEUE': (3, 'Размер первой очереди оповещения мастеров'),
    'SECOND_NEW_ORDER_NOTIFICATION_QUEUE': (3, 'Размер второй очереди оповещения мастеров'),
    'REPEATED_ORDER_MINUTES': (10, 'Время создания повторного заказа для получения скидки (в минутах)'),
    'LIVE_TEX_ID': (101560, 'Идентификатор LiveTex'),
    'NEW_REPAIR_ORDER_NOTIFY_DELAY': (2, 'Задержка после создания заказа для рассылки (в минутах)'),
    'CANCEL_PARTS_ORDER_TIME': (1440, 'Время анулирования резерва запчастей (в минутах)'),
    'GET_SHEET_QUANTITY': (3, 'Количество артикулов на обновление за раз'),
    'LIFE_PERIOD_OF_PARTNER_LINK': (2, 'Время пригодности ссылки партнера (в часах)'),
    'SHOW_USER_IF_MASTER_HAVE_PARTS_FOR_ORDER': (True, 'Показываем контакты клиента по заказу если есть необходимые запчасти'),
    'PARTS_CONTROL_AFTER': (1440, 'Контроль закупки запчастей (в минутах)'),
    'DEFAULT_REGION_FOR_PRICE': ('Москва', 'Регион по дефолту для цен'),
    'SEND_SMS_IF_NO_RIVIEW': (True, 'Отсылать смс если нет отзыва?'),
    'SEND_SMS_IF_NO_RIVIEW_TIME': (1440, 'Время отправки смс если нет отзыва (в минутах)'),
}

CONSTANCE_BACKEND = 'constance.backends.redisd.RedisBackend'
CONSTANCE_REDIS_CONNECTION = {
    'host': 'localhost',
    'port': 6379,
    'db': 0,
}

TINYMCE_DEFAULT_CONFIG = {
    'plugins': (
        'advimage,advlink,table,searchreplace,contextmenu,'
        'template,paste,save,autosave,media'
    ),
    'mode': 'exact',
    'theme': 'advanced',
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 100,
    'theme_advanced_buttons1': (
        "bold,italic,underline,strikethrough,|,justifyleft,"
        "justifycenter,justifyright,justifyfull,|,styleselect,"
        "formatselect,fontsizeselect"
    ),
    'theme_advanced_buttons2': (
        "bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,"
        "link,unlink,anchor,image,cleanup,help,code,|,forecolor"
    ),
    'theme_advanced_buttons3': (
        "tablecontrols,|,removeformat,visualaid,|,sub,sup,|,charmap,"
        "emotions,iespell,media,advhr"
    ),
    'theme_advanced_toolbar_location': "top",
    'theme_advanced_toolbar_align': "center",
    'theme_advanced_statusbar_location': "bottom",
    'width': '100%',
    'height': 480,
    'gecko_spellcheck': True,
}

MAILGUN_KEY = os.environ.get('MAILGUN_KEY', '')

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}


from celery.schedules import crontab


CELERYBEAT_SCHEDULE = {
    'update-rating-for-all-masters': {
        'task': 'masters.tasks.update_rating_for_all_masters',
        'schedule': crontab(hour=1, minute=30),
    },
    'update-rating-for-masters-who-requires-update': {
        'task': 'masters.tasks.update_rating_for_master_who_requires_update',
        'schedule': crontab(minute='*/10'),
    },
    'import-vendor-parts-from-email-xml-attachment': {
        'task': 'core.tasks.task_import_parts_from_remote_xml_file',
        'schedule': crontab(minute='*/20'),
    }
}

CELERY_TIMEZONE = 'UTC'

try:
    from .local_settings import *
except ImportError:
    pass

if DEBUG:
    INSTALLED_APPS += (
        'debug_toolbar.apps.DebugToolbarConfig',
    )
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
else:
    INSTALLED_APPS += ('raven.contrib.django.raven_compat',)
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'root': {
            'level': 'WARNING',
            'handlers': ['sentry'],
        },
        'formatters': {
            'verbose': {
                'format': (
                    '%(levelname)s %(asctime)s %(module)s %(process)d '
                    '%(thread)d ''%(message)s'
                )
            },
        },
        'handlers': {
            'sentry': {
                'level': 'WARNING',
                'class': (
                    'raven.contrib.django.raven_compat.handlers.SentryHandler'
                ),
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            }
        },
        'loggers': {
            'django.db.backends': {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': False,
            },
            'raven': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'propagate': False,
            },
            'sentry.errors': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'propagate': False,
            },
        },
    }
    MIDDLEWARE_CLASSES = (
        ('raven.contrib.django.raven_compat.middleware.'
         'Sentry404CatchMiddleware'),
    ) + MIDDLEWARE_CLASSES
    SENTRY_CELERY_LOGLEVEL = logging.INFO
    RAVEN_CONFIG['CELERY_LOGLEVEL'] = logging.INFO
