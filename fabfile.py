# coding: utf-8
'''
Run example:
fab --roles=test deploy
'''
from __future__ import unicode_literals

from datetime import datetime
from fabric.api import (
    env, run, prefix, get, task, put,
)
from fabric.contrib.project import rsync_project


env.roledefs = {
    # 'production': ['ubuntu@52.28.52.22'],

    'test': ['ubuntu@52.29.231.238'],

    # 'staging': ['root@188.166.121.199'],
    # 'release': ['root@188.166.121.199'],
    # 'manicure': ['root@188.166.121.199'],
}


env.folder = {
    # 'production': 'fixland',

    'test': 'fixland',

    # 'staging': 'fixland',
    # 'release': 'fixland-release',
    # 'manicure': 'manicure',
}


# @deprecated: 2016-04-25
RSYNC_EXCLUDES = [
    'local_settings.py', '.git', '*.pyc', 'htmlcov',
    'mediafiles', 'server', 'celerybeat-schedule',
    'node_modules', '__pycache__', 'locale',
]


def _get_folder_name():
    return env['folder'][env['roles'][0]]


def rsync():
    '''
    @deprecated: 2016-04-25
    '''
    rsync_project(
        remote_dir='/opt/{}'.format(_get_folder_name()),
        exclude=RSYNC_EXCLUDES, delete=True
    )


def update_repo():
    with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
        run('git checkout master')
        run('git pull origin master')
        run('git log -1 --pretty')


def install_dependencies():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        run('/opt/{0}/venv/bin/pip install -U -r '
            '/opt/{0}/fixland/requirements.txt'.format(_get_folder_name()))


def create_virtualenv():
    run('virtualenv --python=python3 /opt/{}/venv'.format(_get_folder_name()))


def syncdb():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        run('/opt/{0}/venv/bin/python /opt/{0}/fixland/manage.py '
            'syncdb --noinput'.format(_get_folder_name()))


def build_frontend():
    with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
        run('gulp build_all')


def collect_static():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        run('/opt/{0}/venv/bin/python /opt/{0}/fixland/manage.py '
            'collectstatic --noinput --settings fixland.settings'.format(
                _get_folder_name()))
        run('/opt/{0}/venv/bin/python /opt/{0}/fixland/manage.py '
            'compress'.format(_get_folder_name()))


@task
def download_db_dump():
    run('pg_dump --no-owner -h localhost -p 5432 -U fixland -d fixland | '
        'gzip > /tmp/fixland.sql.gz')
    get('/tmp/fixland.sql.gz', './dump.sql.gz')


@task
def backup_database():
    run('pg_dump --no-owner -h localhost -p 5432 -U fixland -d fixland | '
        'gzip > /data/dumps/fixland-{}.sql.gz'.format(datetime.now()))


def update_translations():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        run('/opt/{0}/venv/bin/python /opt/{0}/fixland/manage.py '
            'update_translation_fields --settings fixland.settings'.format(
                _get_folder_name()))


@task
def backup_translations():
    run('cp -r /opt/{0}/fixland/locale /opt/{0}/locale_backup'.format(
        _get_folder_name()))


@task
def download_translations():
    with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
        run('tar -zcvf /opt/{0}/locale.tar.gz locale/'.format
            (_get_folder_name()))
        get('/opt/{0}/locale.tar.gz'.format(
            _get_folder_name()), './locale.tar.gz')


@task
def upload_translations():
    backup_translations()
    run('rm -rf /opt/{0}/fixland/locale'.format(_get_folder_name()))
    put('locale.tar.gz', '/opt/{0}/fixland/locale.tar.gz'.format(
        _get_folder_name()))
    with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
        run('tar -zxvf locale.tar.gz')
        run('rm locale.tar.gz')


def makemessages():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
            run('make makemessages')


@task
def compilemessages():
    with prefix('source /opt/{}/venv/bin/activate'.format(_get_folder_name())):
        with prefix('cd /opt/{}/fixland'.format(_get_folder_name())):
            run('make compilemessages')


@task
def compilemessages_and_restart():
    compilemessages()
    run('sudo supervisorctl restart {}'.format(_get_folder_name()))


@task
def install():
    create_virtualenv()
    deploy()


@task
def deploy():
    backup_translations()
    update_repo()
    install_dependencies()
    syncdb()
    build_frontend()
    collect_static()
    makemessages()
    compilemessages()
    run('sudo supervisorctl restart {}'.format(_get_folder_name()))
    run('sudo supervisorctl restart celery_{}'.format(_get_folder_name()))
    run('sudo supervisorctl restart celery_beat_{}'.format(_get_folder_name()))
    run('sudo supervisorctl status')
