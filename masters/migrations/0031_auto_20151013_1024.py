# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def by_key(items, key):
    by_key = {}
    for item in items:
        key_val = getattr(item, key)
        if key_val not in by_key:
            by_key[key_val] = []
        by_key[key_val].append(item)
    return by_key


def remove_master_rating_log_dublicates(apps, schema_editor):
    MasterRatingLog = apps.get_model("masters", "MasterRatingLog")
    logs = MasterRatingLog.objects.filter(rating_type='penalty_part', amount=-1)
    by_master = by_key(logs, 'master_id')
    for master_logs in by_master.values():
        if len(master_logs) < 2:
            continue
        by_part = by_key(master_logs, 'penalty_order_part_id')
        for logs in by_part.values():
            if len(logs) < 2:
                continue
            master = logs[0].master
            total_rating = len(logs) - 1
            master.rating = master.rating + total_rating
            master.save()
            for log in logs[:-1]:
                log.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0030_auto_20150925_1124'),
    ]

    operations = [
        migrations.RunPython(remove_master_rating_log_dublicates)
    ]
