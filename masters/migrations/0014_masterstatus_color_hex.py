# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0013_auto_20150706_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='masterstatus',
            name='color_hex',
            field=models.CharField(null=True, max_length=6, blank=True),
        ),
    ]
