# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Master',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('registration_comment', models.TextField(help_text='Комментарий который был введен при регистрации', verbose_name='Комментарий')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='master')),
            ],
            options={
                'verbose_name_plural': 'Мастера',
                'verbose_name': 'Мастер',
            },
        ),
    ]
