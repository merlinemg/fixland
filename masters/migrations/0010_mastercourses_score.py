# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0009_auto_20150624_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='mastercourses',
            name='score',
            field=models.IntegerField(blank=True, null=True, verbose_name='Оценка'),
        ),
    ]
