# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0015_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='masterpart',
            name='quantity',
        ),
        migrations.AddField(
            model_name='masterpart',
            name='status',
            field=models.CharField(max_length=255, choices=[('instock', 'В наличии'), ('charge_off', 'Списанная')], default='instock'),
        ),
    ]
