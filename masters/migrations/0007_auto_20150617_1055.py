# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0006_master_can_work'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterStatus',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Найменование')),
                ('rating', models.IntegerField(verbose_name='Порог рейтинга')),
            ],
            options={
                'verbose_name_plural': 'Статусы мастеров',
                'verbose_name': 'Статус мастера',
            },
        ),
        migrations.AddField(
            model_name='master',
            name='rating',
            field=models.IntegerField(default=0, verbose_name='Рейтинг'),
        ),
        migrations.AlterField(
            model_name='master',
            name='can_work',
            field=models.BooleanField(default=False, verbose_name='Может принимать заказы'),
        ),
    ]
