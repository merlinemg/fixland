# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название', help_text='Название курса')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Дата и время', help_text='Дата и время начала курса')),
                ('address', models.CharField(max_length=255, verbose_name='Адрес', help_text='Адрес проведения курса')),
                ('closed', models.BooleanField(default=False, verbose_name='Закрыт')),
            ],
            options={
                'verbose_name_plural': 'Курсы',
                'verbose_name': 'Курс',
            },
        ),
        migrations.CreateModel(
            name='MasterCourses',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('completed', models.BooleanField(default=False, verbose_name='Завершен')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('course', models.ForeignKey(to='masters.Course', related_name='masters_courses')),
            ],
        ),
        migrations.AddField(
            model_name='master',
            name='is_certificated',
            field=models.BooleanField(default=False, verbose_name='Сертифицированный'),
        ),
        migrations.AddField(
            model_name='mastercourses',
            name='master',
            field=models.ForeignKey(to='masters.Master', related_name='mastrer_courses'),
        ),
        migrations.AddField(
            model_name='master',
            name='courses',
            field=models.ManyToManyField(to='masters.Course', through='masters.MasterCourses'),
        ),
    ]
