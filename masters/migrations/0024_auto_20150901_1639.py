# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0023_auto_20150814_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='masterplace',
            name='all_metro',
            field=models.BooleanField(default=False, verbose_name='Все метро'),
        ),
        migrations.AlterField(
            model_name='masterplace',
            name='master',
            field=models.ForeignKey(related_name='places', to='masters.Master'),
        ),
    ]
