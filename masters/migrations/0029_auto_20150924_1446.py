# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0028_auto_20150915_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='master',
            name='parts_control',
            field=models.BooleanField(default=True, verbose_name='Контроль закупки запчастей'),
        ),
        migrations.AlterField(
            model_name='masterpart',
            name='master',
            field=models.ForeignKey(to='masters.Master', related_name='parts_status'),
        ),
        migrations.AlterField(
            model_name='partorderitem',
            name='master',
            field=models.ForeignKey(related_name='reserved_parts', verbose_name='Мастер', to='masters.Master', null=True),
        ),
    ]
