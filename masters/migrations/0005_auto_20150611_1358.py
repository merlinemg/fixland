# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0004_auto_20150611_1348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='master',
            name='user',
            field=models.OneToOneField(related_name='master', to=settings.AUTH_USER_MODEL),
        ),
    ]
