# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0018_auto_20150728_1619'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_type',
            field=models.CharField(choices=[('iphone4', 'Iphone 4-ки'), ('iphone5', 'Iphone 5-ки'), ('iphone6', 'Iphone 6-ки'), ('ipad', "Ipad'ы")], max_length=255, null=True, verbose_name='Тип курса'),
        ),
        migrations.AddField(
            model_name='courselesson',
            name='is_exam',
            field=models.BooleanField(verbose_name='Экзамен?', default=False),
        ),
    ]
