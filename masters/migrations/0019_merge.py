# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0018_auto_20150728_1619'),
        ('masters', '0018_masterplace'),
    ]

    operations = [
    ]
