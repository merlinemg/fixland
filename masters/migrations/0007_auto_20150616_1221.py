# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0006_master_can_work'),
    ]

    operations = [
        migrations.AlterField(
            model_name='master',
            name='can_work',
            field=models.BooleanField(verbose_name='Может принимать заказы', default=False),
        ),
    ]
