# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0037_auto_20150924_1724'),
        ('masters', '0029_auto_20150924_1446'),
    ]

    operations = [
        migrations.AddField(
            model_name='masterratinglog',
            name='penalty_order_part',
            field=models.ForeignKey(verbose_name='Штрафная запчасть', to='orders.RepairOrderItemPart', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='masterratinglog',
            name='rating_type',
            field=models.CharField(verbose_name='Тип рейтинга', max_length=255, choices=[('courses', 'Рейтинг за курсы'), ('repairs', 'Рейтинг за отремонтированные поломки'), ('reviews', 'Рейтинг за оценки'), ('bonus', 'Бонус Рейтинг'), ('penalty_part', 'Штраф за покупку запчасти у других поставщиков')]),
        ),
    ]
