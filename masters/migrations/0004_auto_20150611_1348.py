# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0003_auto_20150611_1340'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='courselesson',
            options={'verbose_name_plural': 'Уроки', 'ordering': ('number',), 'verbose_name': 'Урок'},
        ),
        migrations.AlterModelOptions(
            name='courselessonevent',
            options={'verbose_name_plural': 'Занятия', 'verbose_name': 'Занятие'},
        ),
        migrations.RemoveField(
            model_name='mastercourses',
            name='lesson',
        ),
        migrations.AddField(
            model_name='mastercourses',
            name='lesson_event',
            field=models.ForeignKey(related_name='masters', to='masters.CourseLessonEvent', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='master',
            name='lessons',
            field=models.ManyToManyField(through='masters.MasterCourses', to='masters.CourseLessonEvent'),
        ),
        migrations.AlterField(
            model_name='mastercourses',
            name='master',
            field=models.ForeignKey(related_name='courses', to='masters.Master'),
        ),
    ]
