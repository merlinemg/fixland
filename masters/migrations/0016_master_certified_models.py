# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0016_auto_20150720_1244'),
        ('masters', '0015_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='master',
            name='certified_models',
            field=models.ManyToManyField(verbose_name='Модели устройств которые мастер может брать в работу', to='devices.DeviceModel'),
        ),
    ]
