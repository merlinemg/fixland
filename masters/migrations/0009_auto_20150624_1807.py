# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0008_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mastercourses',
            name='lesson_event',
            field=models.ForeignKey(related_name='masters', verbose_name='Занятие', to='masters.CourseLessonEvent'),
        ),
        migrations.AlterField(
            model_name='mastercourses',
            name='master',
            field=models.ForeignKey(related_name='courses', verbose_name='Мастер', to='masters.Master'),
        ),
        migrations.AlterField(
            model_name='masterstatus',
            name='name',
            field=models.CharField(verbose_name='Наименование', max_length=255),
        ),
    ]
