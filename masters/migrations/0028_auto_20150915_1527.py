# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0027_auto_20150914_1503'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='partorderitem',
            options={'verbose_name': 'Заказ запчасти', 'verbose_name_plural': 'Заказы запчастей'},
        ),
        migrations.RemoveField(
            model_name='master',
            name='new_repair_order_notification',
        ),
        migrations.AddField(
            model_name='master',
            name='send_email_repair_order_notification',
            field=models.BooleanField(default=False, verbose_name='Сообщать о новом заказе по email?'),
        ),
        migrations.AddField(
            model_name='master',
            name='send_sms_repair_order_notification',
            field=models.BooleanField(default=False, verbose_name='Сообщать о новом заказе по sms?'),
        ),
    ]
