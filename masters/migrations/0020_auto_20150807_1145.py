# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0015_auto_20150807_1145'),
        ('masters', '0019_auto_20150806_1143'),
        ('masters', '0019_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterRatingLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('rating_type', models.CharField(max_length=255, choices=[('courses', 'Рейтинг за курсы'), ('repairs', 'Рейтинг за отремонтированные поломки'), ('reviews', 'Рейтинг за оценки')], verbose_name='Тип рейтинга')),
                ('amount', models.IntegerField()),
                ('active', models.BooleanField(default=True, verbose_name='Не просроченный')),
                ('created_at', models.DateTimeField(verbose_name='Cоздан', auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
            ],
            options={
                'verbose_name': 'Лог рейтинга',
                'verbose_name_plural': 'Логи рейтинга',
            },
        ),
        migrations.CreateModel(
            name='MasterReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('score', models.IntegerField(choices=[(5, '5 звезд'), (4, '4 звезды'), (3, '3 звезды'), (2, '2 звезды'), (1, '1 звезда')], verbose_name='Оценка')),
                ('created_at', models.DateTimeField(verbose_name='Cоздан', auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
            ],
        ),
        migrations.AddField(
            model_name='master',
            name='rejection_coef',
            field=models.DecimalField(default=Decimal('1'), decimal_places=2, max_digits=3),
        ),
        migrations.AlterField(
            model_name='master',
            name='certified_models',
            field=models.ManyToManyField(blank=True, verbose_name='Модели устройств которые мастер может брать в работу', to='devices.DeviceModel'),
        ),
        migrations.AddField(
            model_name='masterreview',
            name='master',
            field=models.ForeignKey(related_name='reviews', verbose_name='Мастер', to='masters.Master'),
        ),
        migrations.AddField(
            model_name='masterreview',
            name='repair_order',
            field=models.ForeignKey(verbose_name='Заказ', to='orders.RepairOrder'),
        ),
        migrations.AddField(
            model_name='masterratinglog',
            name='master',
            field=models.ForeignKey(related_name='rating_log', verbose_name='Мастер', to='masters.Master'),
        ),
        migrations.AddField(
            model_name='masterratinglog',
            name='master_course',
            field=models.ForeignKey(to='masters.MasterCourses', verbose_name='Курс Мастера', null=True),
        ),
        migrations.AddField(
            model_name='masterratinglog',
            name='master_review',
            field=models.ForeignKey(to='masters.MasterReview', verbose_name='Оценка', null=True),
        ),
        migrations.AddField(
            model_name='masterratinglog',
            name='repair_order_item',
            field=models.ForeignKey(to='orders.RepairOrderItem', verbose_name='Отремонтированная поломка', null=True),
        ),
    ]
