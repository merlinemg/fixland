import simplejson
import json
from decimal import Decimal
from constance import config
from annoying.decorators import ajax_request
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from django.http import HttpResponseForbidden, HttpResponse

from devices.bl import get_devices
from core.w1 import get_payment_form_context
from core.models import StaticText
from .decorators import master_required
from .models import MasterCourses, Course, Master, MasterPart
from .bl import get_courses, add_master_parts, get_agent_report_pdf_response, cancel_parts_orders, cancel_parts_orders_items
from .forms import PaymentForm, AgentReportForm


@login_required
@master_required
def master_index(request):
    template_name = 'masters/index.html' if request.user.master.is_certificated else 'masters/courses.html'
    return render(request, template_name, {'repair_order_commission': settings.REPAIR_ORDER_COMMISSION})


@require_http_methods(["POST"])
@ajax_request
def subscribe_course(request):
    master = Master.objects.get(user=request.user)
    try:
        data = simplejson.loads(request.body.decode('utf-8'))
        course = Course.objects.get(id=data['id'])
        master_courses = MasterCourses.objects.filter(master=master).filter(course=course)
        if (master_courses.count()) > 0:
            for master_course in master_courses:
                master_course.delete()
        else:
            MasterCourses.objects.create(master=master, course=course)
        courses = get_courses(request.user)
        return {'success': True, 'courses': courses}
    except Exception:
        return {'success': False}


@login_required
def master_orders(request):
    return render(request, 'masters/orders.html', {'repair_order_commission': settings.REPAIR_ORDER_COMMISSION})


def how_it_works(request):
    return render(request, 'masters/how_it_works.html')


@login_required
def master_profile(request):
    if not request.user.is_master():
        return redirect(reverse('index'))
    return render(request, 'masters/profile.html')


@login_required
@master_required
def balance(request):
    amount = None
    if request.GET.get('amount', None):
        try:
            amount = Decimal(request.GET.get('amount'))
        except:
            pass
    if not amount:
        amount = Decimal(config.DEFAULT_PAYMENT_AMOUNT)
    return render(request, 'masters/balance.html', {'amount': amount})


@login_required
@master_required
def parts(request):
    devices = get_devices()
    return render(request, 'masters/parts.html', {'devices': simplejson.dumps(devices), })


@login_required
@master_required
def transactions(request):
    return render(request, 'masters/transactions.html')


@login_required
@master_required
def payment(request):
    if request.method == 'POST':
        form = PaymentForm(request.POST)
        if form.is_valid():
            form_context = get_payment_form_context(request, form.cleaned_data.get('payment_amount'))
            return render(request, 'masters/payment.html', {'form_context': form_context})
        else:
            return render(request, 'masters/balance.html', {'form': form})
    else:
        return redirect(reverse('master_balance'))


@csrf_exempt
@require_http_methods(["POST"])
def payment_success(request):
    messages.add_message(request, messages.SUCCESS, _('Оплата прошла успешно'))
    return redirect(reverse('master_balance'))


@csrf_exempt
@require_http_methods(["POST"])
def payment_fail(request):
    messages.add_message(request, messages.ERROR, _('Оплата завершилась ошибкой'))
    return redirect(reverse('master_balance'))


@require_http_methods(["POST"])
@login_required
@master_required
@ajax_request
def part_charge_off(request, part, qty):
    master_parts = MasterPart.objects.filter(part_id=part, master=request.user.master, status=MasterPart.STATUS_INSTOCK)
    if int(qty) < 1 or len(master_parts) < int(qty):
        return HttpResponseForbidden(
            simplejson.dumps({'errors': [_('Неверное значение')]}),
            content_type="application/json")
    for master_part in master_parts[:int(qty)]:
        master_part.status = MasterPart.STATUS_CHARGE_OFF
        master_part.save()
    return HttpResponse()


@login_required
@master_required
def offer(request):
    offer = StaticText.objects.get(type=StaticText.TYPE_OFFER)
    return render(request, 'masters/offer.html', {'offer': offer.text})


@login_required
@master_required
def payment_rules(request):
    payment_rules = StaticText.objects.get(type=StaticText.TYPE_PAYMENT_RULES)
    return render(request, 'masters/payment_rules.html', {'payment_rules': payment_rules.text})


@require_http_methods(["POST"])
@login_required
@ajax_request
@master_required
def parts_order(request):
    post_data = simplejson.loads(request.body.decode('utf-8'))
    result = add_master_parts(post_data, request.user.master)
    return result


@login_required
@master_required
def territory(request):
    return render(request, 'masters/territory.html')


@login_required
@master_required
def agent_report(request):
    master = request.user.master
    if request.method == 'POST':
        form = AgentReportForm(data=request.POST, master=master)
        if form.is_valid():
            return get_agent_report_pdf_response(request, master, form.cleaned_data['year'], form.cleaned_data['month'])
    else:
        form = AgentReportForm(master=master)
    return render(request, 'masters/agent_report.html', {'form': form})


@require_http_methods(["POST"])
@ajax_request
@master_required
def cancel_parts_order(request):
    data = json.loads(request.body.decode('utf-8'))
    if not data.get('order_number'):
        return HttpResponseForbidden()
    cancel_parts_orders([data.get('order_number')], request.user.master, True)
    return {'result': True}


@require_http_methods(["POST"])
@ajax_request
@master_required
def cancel_parts_order_item(request):
    data = json.loads(request.body.decode('utf-8'))
    if not data.get('order_id'):
        return HttpResponseForbidden()
    cancel_parts_orders_items([data.get('order_id')], request.user.master)
    return {'result': True}
