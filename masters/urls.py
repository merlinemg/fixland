from django.conf.urls import patterns, url

urlpatterns = patterns(
    'masters.views',
    url(r'^$', 'master_index', name='master_index'),
    url(r'^subscribe_course/$', 'subscribe_course', name='subscribe_course'),
    url(r'^orders/$', 'master_orders', name='master_orders'),
    url(r'^how_this_work/$', 'how_it_works', name='how_it_works'),
    url(r'^profile/$', 'master_profile', name='master_profile'),
    url(r'^balance/$', 'balance', name='master_balance'),
    url(r'^parts/$', 'parts', name='master_parts'),
    url(r'^balance/transactions/$', 'transactions', name='master_transactions'),
    url(r'^balance/payment-rules/$', 'payment_rules', name='payment_rules'),
    url(r'^payment/$', 'payment', name='master_payment'),
    url(r'^payment_success/$', 'payment_success', name='payment_success'),
    url(r'^payment_fail/$', 'payment_fail', name='payment_fail'),
    url(r'^masteragreement/$', 'offer', name='offer'),
    url(r'^territory/$', 'territory', name='master_territory'),
    url(r'^agent_report/$', 'agent_report', name='agent_report'),
)
