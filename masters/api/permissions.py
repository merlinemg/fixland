from rest_framework import permissions


class IsMaster(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_master()

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and request.user.is_master()


class MasterPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_master()

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and request.user.is_master() and obj == request.user.master


class ObjMasterPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_master()

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and request.user.is_master() and obj.master == request.user.master
