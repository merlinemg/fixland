# coding: utf-8
from django.contrib import admin
from .models import Master, Course, MasterCourses, CourseLesson, CourseLessonEvent, MasterStatus, PartOrderItem, \
    MasterReview, MasterRatingLog, MasterPlace, MasterPart
from django.utils.translation import ugettext_lazy as _


class MasterCoursesInline(admin.TabularInline):
    model = MasterCourses
    extra = 1


class CourseLessonInline(admin.TabularInline):
    model = CourseLesson
    extra = 1


class CourseLessonEventInline(admin.TabularInline):
    model = CourseLessonEvent
    extra = 1


class MasterPartInline(admin.TabularInline):
    model = MasterPart
    extra = 1
    autocomplete_lookup_fields = {
        'fk': ['part'],
    }
    raw_id_fields = ('part',)


class MasterAdmin(admin.ModelAdmin):
    list_display = ('user', 'city', 'registration_comment', 'is_certificated')
    inlines = (MasterPartInline,)
    filter_horizontal = ('certified_models',)
    model = Master
    raw_id_fields = ('user',)
    autocomplete_lookup_fields = {
        'fk': ['user'],
    }
    readonly_fields = ('total_rating', 'balance')

    def get_queryset(self, request):
        qs = super(MasterAdmin, self).get_queryset(request)
        return qs.prefetch_related('parts_status', 'parts_status__part')


class CourseAdmin(admin.ModelAdmin):
    list_display = ('name',)
    model = Course
    inlines = (CourseLessonInline,)


class CourseLessonAdmin(admin.ModelAdmin):
    ordering = ('number',)
    list_display = ('name', 'number', 'course')
    list_editable = ('number',)
    list_filter = ('course__name',)
    inlines = (CourseLessonEventInline,)


class CourseLessonEventAdmin(admin.ModelAdmin):
    ordering = ('lesson__number',)
    list_display = ('lesson', 'starttime', 'address')
    list_filter = ('lesson__name',)
    inlines = (MasterCoursesInline,)


class MasterStatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'rating')
    list_display_links = ('name', 'rating')


class PartOrderItemInline(admin.TabularInline):
    model = PartOrderItem
    extra = 1


class PartOrderItemAdmin(admin.ModelAdmin):
    list_display = ('order_number', 'vendor_part', 'master', 'warehouse', 'quantity', 'price', 'created_at', 'status')
    list_filter = ('warehouse', 'master')


class MasterReviewAdmin(admin.ModelAdmin):
    list_display = ('master', 'repair_order_link', 'score', 'created_at',)


class MasterRatingLogAdmin(admin.ModelAdmin):
    list_display = ('master', 'rating_type', 'amount', 'active', 'created_at', '_repair_order')
    list_filter = ('rating_type', 'active')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ('amount', 'master', 'rating_type', 'master_course', 'repair_order_item', 'master_review', 'penalty_order_part', 'active',
                    'created_at', 'updated_at')
        else:
            return ()

    def _repair_order(self, obj):
        if obj.master_review:
            return obj.master_review.repair_order
        elif obj.repair_order_item:
            return obj.repair_order_item.repair_order
        elif obj.penalty_order_part:
            return obj.penalty_order_part.repair_order_item.repair_order
        else:
            return None

    _repair_order.short_description = _('Заказ за который мастер получил рейтинг')


class MasterPlaceAdmin(admin.ModelAdmin):
    title = _('Территория мастера')
    list_display = ('master', 'region', 'city')


admin.site.register(Master, MasterAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(CourseLesson, CourseLessonAdmin)
admin.site.register(CourseLessonEvent, CourseLessonEventAdmin)
admin.site.register(MasterStatus, MasterStatusAdmin)
admin.site.register(PartOrderItem, PartOrderItemAdmin)
admin.site.register(MasterReview, MasterReviewAdmin)
admin.site.register(MasterRatingLog, MasterRatingLogAdmin)
admin.site.register(MasterPlace, MasterPlaceAdmin)
