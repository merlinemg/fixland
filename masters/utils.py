from django_redis import get_redis_connection

AGENT_REPORT_NUMBER_KEY = 'agent-report-number'


def get_and_increment_agent_report_number():
    con = get_redis_connection('default')
    number = con.get(AGENT_REPORT_NUMBER_KEY)
    if not number:
        number = 1
        con.set(AGENT_REPORT_NUMBER_KEY, number)
    else:
        con.incr(AGENT_REPORT_NUMBER_KEY)
    return number
