var path = require('path');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var concat = require('gulp-concat');
var eslint = require('gulp-eslint');
var changed = require('gulp-changed');
var csslint = require('gulp-csslint');

var changedOpts = {
    hasChanged: changed.compareLastModifiedTime
};

function getBundle() {
    return browserify({
        entries: ['./fixland/static/js/apps/app.js'],
        transform: [babelify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    });
}

function getMasterBundle() {
    return browserify({
        entries: ['./fixland/static/js/apps/masterApps/app.js'],
        transform: [babelify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    });
}

var lintPath =  path.join(__dirname, 'fixland', 'static', 'js', 'apps', '**', '*');

gulp.task('csslint', function() {
  gulp.src('./fixland/static/css/main.css')
    .pipe(csslint())
    .pipe(csslint.reporter());
});

gulp.task('lint:all', function() {
    gulp.src(lintPath)
        .pipe(eslint())
        .pipe(eslint.formatEach())
        // .pipe(eslint.failOnError())
        ;
});

gulp.task('lint:strict', function() {
    gulp.src(lintPath)
        .pipe(eslint())
        .pipe(eslint.formatEach())
        .pipe(eslint.failOnError())
        ;
});

gulp.task('lint:one', function() {
    gulp.src(lintPath)
        .pipe(changed(lintPath, changedOpts))
        .pipe(eslint())
        .pipe(eslint.formatEach())
        //.pipe(eslint.failOnError())
        ;
});

gulp.task('browserify', function() {
    var bundler = getBundle();
    var watcher = watchify(bundler);

    return watcher.on('update', function () {
        var updateStart = Date.now();
        watcher.bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./fixland/static/js/dist'));
        console.log('Updated!', (Date.now() - updateStart) + 'ms');
        gulp.start('lint:one');
    })
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./fixland/static/js/dist'));
});

gulp.task('browserifyMaster', function() {
    var masterBundler = getMasterBundle();
    var watcher = watchify(masterBundler);

    return watcher.on('update', function () {
        var updateStart = Date.now();
        watcher.bundle()
        .pipe(source('masterApp.js'))
        .pipe(gulp.dest('./fixland/static/js/dist'));
        console.log('Updated master!', (Date.now() - updateStart) + 'ms');
        gulp.start('lint:one');
    })
    .bundle()
    .pipe(source('masterApp.js'))
    .pipe(gulp.dest('./fixland/static/js/dist'));
});

gulp.task('build', function() {
    var bundler = getBundle();
    return bundler.bundle().pipe(source('app.js')).pipe(gulp.dest('./fixland/static/js/dist'));
});

gulp.task('buildMaster', function() {
    var masterBundler = getMasterBundle();
    return masterBundler.bundle().pipe(source('masterApp.js')).pipe(gulp.dest('./fixland/static/js/dist'));
});

gulp.task('build_all', ['build', 'buildMaster']);

gulp.task('default', ['lint:all', 'build', 'browserify', 'buildMaster', 'browserifyMaster']);
