# coding: utf-8
from decimal import Decimal
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from accounts.models import Region


class Device(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке.'))

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Устройство')
        verbose_name_plural = _('Устройства')

    def __str__(self):
        return self.name


class DeviceColor(models.Model):
    TEXT_COLOR_CHOICES = (('000000', _('Черный')), ('ffffff', _('Белый')))
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    hex = models.CharField(max_length=6)
    text_color = models.CharField(max_length=6, choices=TEXT_COLOR_CHOICES, default='000000')

    class Meta:
        verbose_name = _('Цвет устройства')
        verbose_name_plural = _('Цвета устройств')

    def __str__(self):
        return self.name


class Repair(models.Model):
    name = models.CharField(max_length=255)
    visible = models.BooleanField(_('Отображать?'), default=True)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)

    class Meta:
        verbose_name = _('Поломка')
        verbose_name_plural = _('Поломки')

    def __str__(self):
        return self.name


class DeviceModel(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    device = models.ForeignKey(Device, related_name='models')
    model_colors = models.ManyToManyField(DeviceColor, through='DeviceModelColor', through_fields=('device_model', 'device_color'))
    repairs = models.ManyToManyField(Repair, through='DeviceModelRepair', through_fields=('device_model', 'repair'))
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке.'))
    model_number = models.CharField(_('Модели'), max_length=255, blank=True, help_text=_('Список моделей через запятую для подсказки пользователям'))
    visible = models.BooleanField(_('Отображать?'), default=True)

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Модель')
        verbose_name_plural = _('Модели')

    def __str__(self):
        return self.name

    def get_model_photo(self):
        model_color = self.device_model_colors.first()
        return model_color.photo_url if model_color else None

    def get_grouped_repairs(self):
        repairs = (
            self.model_repairs
            .prefetch_related('repairs')
            .filter(repair__isnull=True, repairs__isnull=False)
            .order_by('id')
            .distinct('id')
        )
        repairs_by_group_repair = []
        for item in repairs:
            repairs_by_group_repair.append({
                'repairs': [i.id for i in item.repairs.all()],
                'id': item.id,
            })
        return repairs_by_group_repair


class DeviceModelColor(models.Model):
    device_model = models.ForeignKey(DeviceModel, related_name='device_model_colors')
    device_color = models.ForeignKey(DeviceColor)
    photo = models.ImageField(_('Картинка'), upload_to='model_pictures')
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке. (меньше 50 - прячется под "еще цвета"'))

    def __str__(self):
        return '{} {} {}'.format(self.device_model.device, self.device_model, self.device_color)

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Цвет модели')
        verbose_name_plural = _('Цвет моделей')

    @property
    def photo_url(self):
        return self.photo.url if self.photo else None


class DeviceModelRepair(models.Model):
    device_model = models.ForeignKey(DeviceModel, related_name='model_repairs')
    repair = models.ForeignKey(Repair, verbose_name=_('Поломка'), null=True, blank=True)
    repairs = models.ManyToManyField(Repair, verbose_name=_('Поломки'), blank=True, related_name='model_repair_group')
    price = models.DecimalField(max_digits=14, decimal_places=2, null=True, editable=False)
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке. (меньше 50 - прячется под "еще поломки"'))
    parts = models.ManyToManyField('devices.Part', verbose_name='Запчасти', through='DeviceModelRepairPart', through_fields=('model_repair', 'part'))
    visible = models.BooleanField(_('Отображать?'), default=True)

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Поломка модели')
        verbose_name_plural = _('Поломки моделей')

    def __str__(self):
        if self.repair:
            return '{} - {}'.format(self.device_model, self.repair)
        elif self.repairs.all():
            return '{model} - {repairs}'.format(
                model=self.device_model,
                repairs=' + '.join(self.repairs.all().values_list('name', flat=True)))
        return ''

    def get_master_commission(self, master):
        return self.get_comission_by_status(master.get_status())

    def get_comission_by_status(self, status):
        default_region = Region.get_default()
        commission = self.commissions.filter(region=default_region, status=status).first()
        if not commission:
            return 0
        if commission.percentage:
            return self.price * (Decimal(commission.commission) / 100)
        else:
            return commission.commission


class DeviceModelRepairPart(models.Model):
    model_repair = models.ForeignKey(DeviceModelRepair, related_name='parts_quantity')
    part = models.ForeignKey('devices.Part', related_name='part_repairs')
    quantity = models.IntegerField(_('Количество'), default=0)

    class Meta:
        verbose_name = _('Запчасть')
        verbose_name_plural = _('Запчасти')


class DeviceModelRepairCommission(models.Model):
    model_repair = models.ForeignKey(DeviceModelRepair, related_name='commissions')
    status = models.ForeignKey('masters.MasterStatus', verbose_name=_('Статус мастера'))
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'))
    commission = models.IntegerField(_('Комиссия'))
    percentage = models.BooleanField(_('Процент'), default=False)

    class Meta:
        ordering = ('region', )
        verbose_name = _('Комиссия')
        verbose_name_plural = _('Комиссии')


class DeviceModelRepairPrice(models.Model):
    device_model_repair = models.ForeignKey(DeviceModelRepair, related_name='prices', verbose_name=_('Поломка модели'))
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'))
    price = models.DecimalField(max_digits=14, decimal_places=2, verbose_name=_('Цена'))

    class Meta:
        verbose_name = _('Цена поломки модели')
        verbose_name_plural = _('Цены поломок моделей')


class Status(models.Model):
    name = models.CharField(max_length=255)
    title = models.CharField(_('Заголовок'), max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    description = models.TextField(_('Описание'), help_text=_('Каждый пункт писать с новой строки.'))
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке. (меньше 50 - прячется под "еще статусы"'))

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Состояние')
        verbose_name_plural = _('Состояния')

    def __str__(self):
        return self.name

    @classmethod
    def get_max(cls):
        return cls.objects.order_by('-rating').first()

    def get_descripion_list(self):
        return self.description.splitlines()


class Memory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке. (меньше 50 - прячется под "еще"'))

    class Meta:
        ordering = ('-priority', )
        verbose_name = _('Объем памяти модели')
        verbose_name_plural = _('Объемы памяти моделей')

    def __str__(self):
        return self.name


class Version(models.Model):
    ordering = ('-priority', )
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, help_text=_('То что будет отоброжаться в URL. Должно быть уникально.'), unique=True)
    priority = models.IntegerField(help_text=_('Чем выше - тем выше в списке. (меньше 50 - прячется под "еще версии"'))

    class Meta:
        verbose_name = _('Версия модели')
        verbose_name_plural = _('Версии моделей')

    def __str__(self):
        return self.name


class DeviceModelSell(models.Model):
    device_model = models.ForeignKey(DeviceModel, related_name='model_sell')
    memory = models.ForeignKey(Memory, related_name='model_memory_sell', blank=True, null=True)
    version = models.ForeignKey(Version, related_name='model_version_sell', blank=True, null=True)
    status = models.ForeignKey(Status)
    price = models.DecimalField(max_digits=14, decimal_places=2, editable=False)

    class Meta:
        verbose_name = _('Состояние модели')
        verbose_name_plural = _('Состояния моделей')

    def __str__(self):
        return '{} - {} - {} - {}'.format(self.device_model, self.memory, self.version or '', self.status)

    def get_short_str(self):
        return '{} / {} / {}'.format(self.device_model, self.memory, self.version or '')


class DeviceModelSellPrice(models.Model):
    device_model_sell = models.ForeignKey(DeviceModelSell, related_name='prices', verbose_name=_('Состояние модели'))
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'))
    price = models.DecimalField(max_digits=14, decimal_places=2)

    class Meta:
        verbose_name = _('Цена продажи модели')
        verbose_name_plural = _('Цены продаж моделей')


class Part(models.Model):
    code = models.CharField(_('Код'), max_length=255, blank=True, null=True)
    article = models.CharField(_('Артикул'), max_length=255, blank=True, null=True, db_index=True)
    name = models.CharField(_('Название'), max_length=255)
    serial_number = models.CharField(_('Серийный номер'), max_length=255, blank=True, null=True)
    price = models.DecimalField(_('Цена'), max_digits=14, decimal_places=2, blank=True, null=True, editable=False)
    color = models.ForeignKey(DeviceColor, verbose_name=_('Цвет запчасти'), default=None, blank=True, null=True, editable=False)
    colors = models.ManyToManyField(DeviceColor, verbose_name=_('Цвета запчасти'), blank=True, related_name='parts')

    class Meta:
        verbose_name = _('Наша запчасть')
        verbose_name_plural = _('Наши запчасти')

    @staticmethod
    def autocomplete_search_fields():
        return ('name__icontains', )

    def __str__(self):
        return self.name

    @property
    def default_price(self):
        part_price = self.prices.filter(region=Region.get_default()).first() or self.prices.first()
        return part_price.price if part_price else 0


class PartPrice(models.Model):
    part = models.ForeignKey(Part, verbose_name=_('Запчасть'), related_name='prices')
    price = price = models.DecimalField(_('Цена'), max_digits=14, decimal_places=2, blank=True, null=True)
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'))

    class Meta:
        verbose_name = _('Цена запчасти')
        verbose_name_plural = _('Цены запчастей')


class Vendor(models.Model):
    name = models.CharField(_('Название'), max_length=255)
    integration_soap1s_url = models.URLField(_('SOAP URL 1С интеграции'), max_length=255, null=True, blank=True)
    integration_soap1s_username = models.CharField(_('SOAP username 1С интеграции'), max_length=255, blank=True, null=True)
    integration_soap1s_password = models.CharField(_('SOAP password 1С интеграции'), max_length=255, blank=True, null=True)
    integration_soap1s_api_key = models.CharField(_('SOAP API KEY 1С интеграции'), max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, db_index=True, null=True)
    is_import_new = models.BooleanField(_('Заливать новые запчасти?'), default=False)
    last_imported_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = _('Поставщик')
        verbose_name_plural = _('Поставщики')

    def __str__(self):
        return self.name

    def is_soap_integrated(self):
        return all((
            self.integration_soap1s_url, self.integration_soap1s_username,
            self.integration_soap1s_password, self.integration_soap1s_api_key,
        ))


class VendorRegion(models.Model):
    vendor = models.ForeignKey(Vendor, related_name='regions')
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'))
    city = models.ForeignKey('accounts.City', verbose_name=_('Город'), blank=True, null=True)

    class Meta:
        verbose_name = _('Регион поставщика')
        verbose_name_plural = _('Регионы поставщиков')

    def __str__(self):
        return self.vendor.name


class VendorWarehouse(models.Model):
    vendor = models.ForeignKey(Vendor, verbose_name=_('Поставщик'), related_name='warehouses', blank=True, null=True)
    code = models.CharField(_('Код склада'), max_length=255)
    name = models.CharField(_('Название'), max_length=255, blank=True, null=True)
    region = models.ForeignKey('accounts.Region', verbose_name=_('Регион'), blank=True, null=True)
    city = models.ForeignKey('accounts.City', verbose_name=_('Город'), blank=True, null=True)
    address = models.CharField(_('Адрес'), max_length=255, blank=True, null=True)
    lon = models.FloatField(_('Широта'), blank=True, null=True)
    lat = models.FloatField(_('Долгота'), blank=True, null=True)

    class Meta:
        verbose_name = _('Склад поставщика')
        verbose_name_plural = _('Склады поставщиков')

    def __str__(self):
        return self.code

    def __init__(self, *args, **kwargs):
        super(VendorWarehouse, self).__init__(*args, **kwargs)
        self._address = self.address

    def address_changed(self):
        return self._address != self.address


class VendorPart(models.Model):
    vendor = models.ForeignKey(Vendor, related_name='parts', verbose_name=_('Поставщик'))
    warehouse = models.ForeignKey(VendorWarehouse, verbose_name=_('Склад'), null=True, blank=True)
    part = models.ForeignKey(Part, related_name='vendors_parts', verbose_name=_('Запчасть'), null=True, blank=True)
    price = models.DecimalField(_('Цена'), max_digits=14, decimal_places=2, blank=True, null=True)
    article = models.CharField(_('Артикул поставщика'), max_length=255, blank=True, null=True, db_index=True)
    name = models.CharField(_('Название'), max_length=255)
    serial_number = models.CharField(_('Серийный номер'), max_length=255, blank=True, null=True)
    color = models.ForeignKey(DeviceColor, verbose_name=_('Цвет запчасти'), default=None, blank=True, null=True)
    exists = models.BooleanField(default=False)
    last_imported_at = models.DateTimeField(null=True)

    class Meta:
        verbose_name = _('Запчасть от поставщика')
        verbose_name_plural = _('Запчасти от поставщиков')

    def __str__(self):
        return self.name


class PartArticle(models.Model):
    article = models.CharField(_('Артикул'), max_length=255, blank=True, null=True, db_index=True)
    vendor_article = models.CharField(_('Артикул поставщика'), max_length=255, blank=True, null=True, db_index=True)
    vendor = models.ForeignKey(Vendor, verbose_name=_('Поставщик'))

    class Meta:
        verbose_name = _('Соответствия артикула поставщика')
        verbose_name_plural = _('Соответствия артикулов поставщиков')

    def __str__(self):
        return self.article


@receiver(post_save, sender=VendorWarehouse)
def update_warehouse_coordinates(sender, instance, created, **kwargs):
    from .tasks import update_warehouse_coordinates
    if not instance.address:
        return
    if created:
        update_warehouse_coordinates.apply_async(countdown=3, args=(instance.id, ))
    elif instance.address_changed():
        update_warehouse_coordinates.apply_async(countdown=3, args=(instance.id, ))
