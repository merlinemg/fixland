import logging

from core.time_utils import utc_now
from integrations.soap1s import bl as soap1s
from ..models import VendorPart, VendorWarehouse

log = logging.getLogger(__name__)


def save_parts(result, vendor, vendor_parts):
    from masters.bl import safe_price_to_decimal
    vendor_parts_by_article = {
        part.article: part for part in vendor_parts
    }
    for part in result['parts']:
        if part['article'] not in vendor_parts_by_article:
            continue
        vendor_part = vendor_parts_by_article[part['article']]
        if part['warehouse'] != 'НЕТ':
            warehouse, _ = VendorWarehouse.objects.get_or_create(vendor=vendor, code=part['warehouse'])
            new_vendor_part, created = VendorPart.objects.get_or_create(
                vendor=vendor,
                warehouse=warehouse,
                part=vendor_part.part,
                article=part['article'],
                defaults=dict(
                    price=safe_price_to_decimal(part['cost']),
                    name=vendor_part.name,
                    serial_number=vendor_part.serial_number,
                    color=vendor_part.color,
                    exists=part['exists'],
                    last_imported_at=utc_now(),
                )
            )
            if not created:
                new_vendor_part.price = safe_price_to_decimal(part['cost'])
                new_vendor_part.exists = part['exists']
                new_vendor_part.last_imported_at = utc_now()
                new_vendor_part.save()
        else:
            VendorPart.objects.filter(
                vendor=vendor,
                part=vendor_part.part,
                article=part['article'],
            ).update(exists=False)
    articles = {part['article'] for part in result['parts']}
    for vendor_part in vendor_parts:
        if vendor_part.article not in articles:
            vendor_part.exists = False
            vendor_part.save()


def order_parts(part_list):
    by_vendors = group_by_vendors(part_list)
    order_result = []
    for vendor_parts in by_vendors.values():
        vendor = vendor_parts[0]['part'].vendor
        order = get_order_for_vendor_parts(vendor_parts)
        if not vendor.is_soap_integrated():
            log.error('Soap not integrated for vendor: %s', vendor)
            order_result.append({
                'vendor_id': vendor.id,
                'success': False,
                'error': 'No credentials'
            })
            continue
        result = soap1s.make_order(
            vendor.integration_soap1s_url,
            vendor.integration_soap1s_username,
            vendor.integration_soap1s_password,
            vendor.integration_soap1s_api_key,
            order,
        )
        order_result.append({
            'vendor_id': vendor.id,
            'success': result['success'],
            'parts': result['parts'],
            'orders': result['orders'],
        })
    return order_result


def get_order_for_vendor_parts(vendor_parts):
    order = []
    for order_part in vendor_parts:
        part = order_part['part']
        order.append({
            'warehouse': part.warehouse.code,
            'article': part.article,
            'qty': order_part['qty'],
        })
    return order


def group_by_vendors(part_list):
    by_vendors = {}
    for order_part in part_list:
        part = order_part['part']
        if part.vendor_id not in by_vendors:
            by_vendors[part.vendor_id] = []
        by_vendors[part.vendor_id].append(order_part)
    return by_vendors
