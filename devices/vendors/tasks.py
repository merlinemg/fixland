from fixland.celery import celery_app

from constance import config
from integrations.soap1s import bl as soap1s
from ..models import Vendor, VendorPart
from .bl import save_parts
import logging

log = logging.getLogger(__name__)

@celery_app.task
def update_availability_for_vendors(vendor_list, part_list):
    vendors = Vendor.objects.filter(id__in=vendor_list)
    for vendor in vendors:
        if not vendor.is_soap_integrated():
            continue
        vendor_parts = VendorPart.objects.filter(part_id__in=part_list, vendor=vendor)
        group_by_qty = lambda arr, n: [arr[i:i + n] for i in range(0, len(arr), n)]
        articles = list({vendor.article for vendor in vendor_parts})
        for articles_group in group_by_qty(articles, config.GET_SHEET_QUANTITY):
            vendor_parts_items = [i for i in vendor_parts if i.article in articles_group]
            try:
                result = soap1s.get_parts_availability(
                    vendor.integration_soap1s_url,
                    vendor.integration_soap1s_username,
                    vendor.integration_soap1s_password,
                    vendor.integration_soap1s_api_key,
                    articles_group,
                )
                if result['success']:
                    save_parts(result, vendor, vendor_parts_items)
            except Exception:
                log.warning('Vendor soap not response', extra={'vendor': vendor})
