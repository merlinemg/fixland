# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0022_auto_20150812_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vendor',
            name='email',
            field=models.EmailField(max_length=255, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='vendor',
            name='last_imported_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
