# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0006_aded_text_color_field_to_devices'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelSell',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=14)),
                ('priority', models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще цвета"')),
                ('device_model', models.ForeignKey(related_name='model_sell', to='devices.DeviceModel')),
            ],
            options={
                'ordering': ('-priority',),
                'verbose_name': 'Состояние модели',
                'verbose_name_plural': 'Состояния моделей',
            },
        ),
        migrations.CreateModel(
            name='Memory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('memory', models.IntegerField(verbose_name='Объем памяти')),
                ('slug', models.SlugField(help_text='То что будет отоброжаться в URL. Должно быть уникально.', unique=True, max_length=255)),
            ],
            options={
                'verbose_name': 'Объем памяти модели',
                'verbose_name_plural': 'Объемы памяти моделей',
            },
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='То что будет отоброжаться в URL. Должно быть уникально.', unique=True, max_length=255)),
                ('description', models.TextField(verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Состояние',
                'verbose_name_plural': 'Состояния',
            },
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(help_text='То что будет отоброжаться в URL. Должно быть уникально.', unique=True, max_length=255)),
            ],
            options={
                'verbose_name': 'Версия модели',
                'verbose_name_plural': 'Версии моделей',
            },
        ),
        migrations.AddField(
            model_name='devicemodelsell',
            name='memory',
            field=models.ForeignKey(related_name='model_memory_sell', to='devices.Memory', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='devicemodelsell',
            name='status',
            field=models.ForeignKey(to='devices.Status'),
        ),
        migrations.AddField(
            model_name='devicemodelsell',
            name='version',
            field=models.ForeignKey(related_name='model_version_sell', to='devices.Version', null=True, blank=True),
        ),
    ]
