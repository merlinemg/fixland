# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0013_auto_20150702_1411'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelRepairPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(verbose_name='Количество', default=0)),
                ('model_repair', models.ForeignKey(to='devices.DeviceModelRepair')),
            ],
        ),
        migrations.CreateModel(
            name='Part',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=255, verbose_name='Код', null=True, blank=True)),
                ('article', models.CharField(max_length=255, verbose_name='Артикул', null=True, blank=True)),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('serial_number', models.CharField(max_length=255, verbose_name='Серийный номер', null=True, blank=True)),
                ('price', models.DecimalField(blank=True, verbose_name='Цена', max_digits=14, null=True, decimal_places=2)),
            ],
            options={
                'verbose_name': 'Запчасть',
                'verbose_name_plural': 'Запчасти',
            },
        ),
        migrations.AddField(
            model_name='devicemodelrepairpart',
            name='part',
            field=models.ForeignKey(to='devices.Part'),
        ),
        migrations.AddField(
            model_name='devicemodelrepair',
            name='parts',
            field=models.ManyToManyField(verbose_name='Запчасти', to='devices.Part', through='devices.DeviceModelRepairPart'),
        ),
    ]
