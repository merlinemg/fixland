# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0002_auto_20150430_1345'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще цвета"')),
                ('device_color', models.ForeignKey(to='devices.DeviceColor')),
            ],
            options={
                'verbose_name': 'Цвет модели',
            },
        ),
        migrations.RemoveField(
            model_name='devicemodel',
            name='colors',
        ),
        migrations.AddField(
            model_name='device',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке.', default=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='devicemodel',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке.', default=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='devicemodelcolor',
            name='device_model',
            field=models.ForeignKey(to='devices.DeviceModel'),
        ),
        migrations.AddField(
            model_name='devicemodel',
            name='model_colors',
            field=models.ManyToManyField(to='devices.DeviceColor', through='devices.DeviceModelColor'),
        ),
    ]
