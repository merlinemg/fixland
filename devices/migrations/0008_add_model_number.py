# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0007_auto_20150526_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicemodel',
            name='model_number',
            field=models.CharField(help_text='Список моделей через запятую для подсказки пользователям', max_length=255, blank=True, verbose_name='Модели'),
        ),
    ]
