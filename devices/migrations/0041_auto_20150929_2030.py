# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def copy_price_to_device_model_sell_price(apps, schema_editor):
    DeviceModelSell = apps.get_model("devices", "DeviceModelSell")
    DeviceModelSellPrice = apps.get_model("devices", "DeviceModelSellPrice")
    Region = apps.get_model("accounts", "Region")
    first = Region.objects.get(dadata_name='Москва')
    second = Region.objects.get(dadata_name='Московская')
    for item in DeviceModelSell.objects.all():
        for region in [first, second]:
            DeviceModelSellPrice.objects.create(
                device_model_sell=item,
                price=item.price,
                region=region
            )


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0030_auto_20150922_1805'),
        ('devices', '0040_auto_20150929_1433'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelSellPrice',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('price', models.DecimalField(decimal_places=2, max_digits=14)),
            ],
            options={
                'verbose_name_plural': 'Цены продаж моделей',
                'verbose_name': 'Цена продажи модели',
            },
        ),
        migrations.AlterField(
            model_name='devicemodelsell',
            name='price',
            field=models.DecimalField(editable=False, decimal_places=2, max_digits=14),
        ),
        migrations.AddField(
            model_name='devicemodelsellprice',
            name='device_model_sell',
            field=models.ForeignKey(verbose_name='Состояние модели', related_name='prices', to='devices.DeviceModelSell'),
        ),
        migrations.AddField(
            model_name='devicemodelsellprice',
            name='region',
            field=models.ForeignKey(to='accounts.Region', verbose_name='Регион'),
        ),
        migrations.RunPython(copy_price_to_device_model_sell_price),
    ]
