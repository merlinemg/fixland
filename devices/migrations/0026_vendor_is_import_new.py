# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0025_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor',
            name='is_import_new',
            field=models.BooleanField(default=False, verbose_name='Заливать новые запчасти?'),
        ),
    ]
