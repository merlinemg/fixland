# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0012_auto_20150624_1049'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicecolor',
            name='name_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='devicecolor',
            name='name_ru',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='repair',
            name='name_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='repair',
            name='name_ru',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='status',
            name='description_en',
            field=models.TextField(help_text='Каждый пункт писать с новой строки.', verbose_name='Описание', null=True),
        ),
        migrations.AddField(
            model_name='status',
            name='description_ru',
            field=models.TextField(help_text='Каждый пункт писать с новой строки.', verbose_name='Описание', null=True),
        ),
        migrations.AddField(
            model_name='status',
            name='name_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='status',
            name='name_ru',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='status',
            name='title_en',
            field=models.CharField(verbose_name='Заголовок', null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='status',
            name='title_ru',
            field=models.CharField(verbose_name='Заголовок', null=True, max_length=255),
        ),
    ]
