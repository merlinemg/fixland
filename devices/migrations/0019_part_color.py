# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0018_auto_20150720_1739'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='color',
            field=models.ForeignKey(default=None, to='devices.DeviceColor', verbose_name='Цвет запчасти', null=True, blank=True),
        ),
    ]
