# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0032_part_colors'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='part',
            name='color',
        ),
    ]
