# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0025_auto_20150826_1017'),
        ('devices', '0025_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='VendorRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('city', models.ForeignKey(to='accounts.City', verbose_name='Город', blank=True, null=True)),
                ('region', models.ForeignKey(to='accounts.Region', verbose_name='Регион')),
                ('vendor', models.ForeignKey(to='devices.Vendor', related_name='regions')),
            ],
            options={
                'verbose_name_plural': 'Регионы поставщиков',
                'verbose_name': 'Регион поставщика',
            },
        ),
    ]
