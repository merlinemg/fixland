# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0003_auto_20150501_0929'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelRepair',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('price', models.DecimalField(max_digits=14, decimal_places=2)),
                ('priority', models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще цвета"')),
            ],
            options={
                'ordering': ('-priority',),
                'verbose_name_plural': 'Поломки моделей',
                'verbose_name': 'Поломка модели',
            },
        ),
        migrations.CreateModel(
            name='Repair',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Поломки',
                'verbose_name': 'Поломка',
            },
        ),
        migrations.AlterModelOptions(
            name='device',
            options={'ordering': ('-priority',)},
        ),
        migrations.AlterModelOptions(
            name='devicemodel',
            options={'ordering': ('-priority',)},
        ),
        migrations.AlterModelOptions(
            name='devicemodelcolor',
            options={'ordering': ('-priority',), 'verbose_name_plural': 'Цвет моделей', 'verbose_name': 'Цвет модели'},
        ),
        migrations.AddField(
            model_name='devicemodelrepair',
            name='device_model',
            field=models.ForeignKey(to='devices.DeviceModel'),
        ),
        migrations.AddField(
            model_name='devicemodelrepair',
            name='repair',
            field=models.ForeignKey(to='devices.Repair'),
        ),
        migrations.AddField(
            model_name='devicemodel',
            name='repairs',
            field=models.ManyToManyField(through='devices.DeviceModelRepair', to='devices.Repair'),
        ),
    ]
