# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0008_add_model_number'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='devicemodelsell',
            options={'verbose_name': 'Состояние модели', 'verbose_name_plural': 'Состояния моделей'},
        ),
        migrations.RemoveField(
            model_name='devicemodelsell',
            name='priority',
        ),
        migrations.RemoveField(
            model_name='memory',
            name='memory',
        ),
        migrations.AddField(
            model_name='memory',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще"', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='status',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще статусы"', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='version',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще версии"', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='devicemodelrepair',
            name='priority',
            field=models.IntegerField(help_text='Чем выше - тем выше в списке. (меньше 50 - прячется под "еще поломки"'),
        ),
        migrations.AlterField(
            model_name='status',
            name='description',
            field=models.TextField(help_text='Каждый пункт писать с новой строки.', verbose_name='Описание'),
        ),
    ]
