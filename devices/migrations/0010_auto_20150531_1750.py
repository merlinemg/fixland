# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0009_auto_20150528_1647'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='memory',
            options={'verbose_name': 'Объем памяти модели', 'verbose_name_plural': 'Объемы памяти моделей', 'ordering': ('-priority',)},
        ),
        migrations.AlterModelOptions(
            name='status',
            options={'verbose_name': 'Состояние', 'verbose_name_plural': 'Состояния', 'ordering': ('-priority',)},
        ),
    ]
