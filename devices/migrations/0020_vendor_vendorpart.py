# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0019_part_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Поставщик',
                'verbose_name_plural': 'Поставщики',
            },
        ),
        migrations.CreateModel(
            name='VendorPart',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('price', models.DecimalField(max_digits=14, null=True, decimal_places=2, blank=True, verbose_name='Цена')),
                ('article', models.CharField(null=True, max_length=255, blank=True, verbose_name='Артикул')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('serial_number', models.CharField(null=True, max_length=255, blank=True, verbose_name='Серийный номер')),
                ('color', models.ForeignKey(blank=True, null=True, verbose_name='Цвет запчасти', to='devices.DeviceColor', default=None)),
                ('part', models.ForeignKey(blank=True, null=True, verbose_name='Запчасть', to='devices.Part', related_name='vendors_parts')),
                ('vendor', models.ForeignKey(verbose_name='Поставщик', to='devices.Vendor', related_name='parts')),
            ],
            options={
                'verbose_name': 'Запчасть от поставщика',
                'verbose_name_plural': 'Запчасти от поставщиков',
            },
        ),
    ]
