# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0034_part_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartArticle',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('article', models.CharField(db_index=True, blank=True, max_length=255, null=True, verbose_name='Артикул')),
                ('vendor_article', models.CharField(db_index=True, blank=True, max_length=255, null=True, verbose_name='Артикул')),
                ('vendor', models.ForeignKey(to='devices.Vendor', verbose_name='Поставщик')),
            ],
            options={
                'verbose_name_plural': 'Соответствия артикулов поставщиков',
                'verbose_name': 'Соответствия артикула поставщика',
            },
        ),
        migrations.AlterField(
            model_name='part',
            name='color',
            field=models.ForeignKey(editable=False, verbose_name='Цвет запчасти', default=None, blank=True, null=True, to='devices.DeviceColor'),
        ),
    ]
