# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0017_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='name_en',
            field=models.CharField(max_length=255, null=True, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='part',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='devicemodelcolor',
            name='device_model',
            field=models.ForeignKey(related_name='device_model_colors', to='devices.DeviceModel'),
        ),
        migrations.AlterField(
            model_name='devicemodelrepairpart',
            name='part',
            field=models.ForeignKey(related_name='part_repairs', to='devices.Part'),
        ),
    ]
