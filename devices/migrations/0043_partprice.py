# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0031_region_geoip_name'),
        ('devices', '0042_auto_20151002_1622'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartPrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.DecimalField(blank=True, verbose_name='Цена', max_digits=14, decimal_places=2, null=True)),
                ('part', models.ForeignKey(to='devices.Part', related_name='prices', verbose_name='Запчасть')),
                ('region', models.ForeignKey(verbose_name='Регион', to='accounts.Region')),
            ],
            options={
                'verbose_name': 'Цена запчасти',
                'verbose_name_plural': 'Цены запчастей',
            },
        ),
        migrations.AlterField(
            model_name='part',
            name='price',
            field=models.DecimalField(verbose_name='Цена', decimal_places=2, editable=False, blank=True, null=True, max_digits=14),
        ),
    ]
