# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_region_to_exist_comissions(apps, schema_editor):
    Region = apps.get_model("accounts", "Region")
    DeviceModelRepairCommission = apps.get_model("devices", "DeviceModelRepairCommission")
    first = Region.objects.get(dadata_name='Москва')
    second = Region.objects.get(dadata_name='Московская')
    for item in DeviceModelRepairCommission.objects.all():
        DeviceModelRepairCommission.objects.create(
            model_repair=item.model_repair,
            status=item.status,
            region=second,
            commission=item.commission,
            percentage=item.percentage,
        )
        item.region = first
        item.save()


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0038_auto_20150929_1219'),
    ]

    operations = [
        migrations.RunPython(set_region_to_exist_comissions),
    ]
