# coding: utf-8
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DevicesAppConfig(AppConfig):
    name = 'devices'
    verbose_name = _('Устройства')
