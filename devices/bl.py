import logging
from collections import defaultdict
from django.db.models import Count, Q

from accounts.models import Region
from orders.models import RepairOrder
from devices.models import DeviceModel, Status, Vendor, VendorWarehouse

log = logging.getLogger(__name__)


def get_devices():
    models = DeviceModel.objects.select_related('device').all()
    devices = defaultdict(list)
    for model in models:
        devices[model.device].append(model)
    devices_final = []
    for device, models in devices.items():
        devices_final.append(
            {
                'name': device.name,
                'slug': device.slug,
                'key': device.id,
                'models': [
                    {
                        'name': model.name, 'slug': model.slug,
                        'key': model.id,
                        'model_number': model.model_number,
                    }
                    for model in models
                ]
            }
        )
    return devices_final


def get_devices_for_repair():
    models = DeviceModel.objects.select_related('device').prefetch_related('device_model_colors', 'device_model_colors__device_color', 'model_colors', 'model_repairs', 'model_repairs__repair', 'model_repairs__prices', 'model_repairs__prices__region', 'model_repairs__repairs').all()
    devices = defaultdict(list)
    for model in models:
        devices[model.device].append(model)
    devices_final = []
    defaut_region = Region.get_default()
    for device, models in devices.items():
        devices_final.append(
            {
                'name': device.name,
                'slug': device.slug,
                'key': device.id,
                'models': [
                    {
                        'name': model.name, 'slug': model.slug,
                        'key': model.id,
                        'model_number': model.model_number,
                        'colors': [
                            {
                                'name': model_color.device_color.name,
                                'hex': model_color.device_color.hex,
                                'slug': model_color.device_color.slug,
                                'text_color': model_color.device_color.text_color,
                                'key': model_color.device_color.id,
                                'model_photo': model_color.photo_url
                            } for model_color in model.device_model_colors.all()
                        ],
                        'repairs': [
                            {
                                'name': model_repair.repair.name,
                                'slug': model_repair.repair.slug,
                                'prices': [
                                    {
                                        'region_id': i.region_id,
                                        'region_name': i.region.dadata_name,
                                        'price': i.price,
                                        'default': True if defaut_region.id == i.region_id else False
                                    } for i in model_repair.prices.all()
                                ],
                                'priority': model_repair.priority,
                                'key': model_repair.id,
                                'repair_id': model_repair.repair.id
                            } for model_repair in filter(lambda obj: obj.visible, model.model_repairs.all()) if model_repair.repair_id
                        ],
                        'grouped_repairs': [
                            {
                                'prices': [
                                    {
                                        'region_id': i.region_id,
                                        'region_name': i.region.dadata_name,
                                        'price': i.price,
                                        'default': True if defaut_region.id == i.region_id else False
                                    } for i in model_repair.prices.all()
                                ],
                                'priority': model_repair.priority,
                                'key': model_repair.id,
                                'repair_list': [i.id for i in model_repair.repairs.all()]
                            } for model_repair in filter(lambda obj: obj.visible, model.model_repairs.all()) if not model_repair.repair_id and len(model_repair.repairs.all())
                        ],
                    }
                    for model in models if model.visible is True
                ]
            }
        )
    return devices_final


def get_devices_for_sell():
    models = DeviceModel.objects.select_related('device').prefetch_related('model_sell', 'model_sell__memory', 'model_sell__version', 'model_sell__status').annotate(model_sell_count=Count('model_sell')).exclude(model_sell_count=0)
    devices = defaultdict(list)
    for model in models:
        devices[model.device].append(model)
    devices_final = []
    for device, models in devices.items():
        devices_final.append(
            {
                'name': device.name,
                'slug': device.slug,
                'key': device.id,
                'models': [
                    {
                        'name': model.name, 'slug': model.slug,
                        'key': model.id,
                        'model_number': model.model_number,
                        'model_photo': model.get_model_photo(),
                        'statuses': [
                            {
                                'name': status.name,
                                'slug': status.slug,
                                'key': status.id
                            } for status in list(set([model_sell.status for model_sell in model.model_sell.all() if model_sell.memory]))
                        ],
                        'memory_list': [
                            {
                                'name': memory.name,
                                'slug': memory.slug,
                                'key': memory.id
                            } for memory in list(set([model_sell.memory for model_sell in model.model_sell.all() if model_sell.memory]))
                        ],
                        'versions': [
                            {
                                'name': version.name,
                                'slug': version.slug,
                                'key': version.id
                            } for version in list(set([model_sell.version for model_sell in model.model_sell.all() if model_sell.version]))
                        ],
                        'sell': [
                            {
                                'status': model_sell.status.slug,
                                'version': model_sell.version.slug if model_sell.version else None,
                                'memory': model_sell.memory.slug if model_sell.memory else None,
                                'price': model_sell.price,
                                'key': model_sell.id
                            } for model_sell in model.model_sell.all()
                        ],
                    }
                    for model in models
                ]
            }
        )
    return devices_final


def get_statuses():
    statuses = Status.objects.all()
    statuses_final = []
    for status in statuses:
        statuses_final.append({
            'name': status.name,
            'title': status.title,
            'slug': status.slug,
            'description_list': status.description.splitlines()
        })
    return statuses_final


def get_warehouses_by_master_regions(master):
    queryset = VendorWarehouse.objects.filter(
        region__in=[i.region for i in master.places.all()]
    )
    queryset = queryset.filter(
        Q(city__isnull=True) | (Q(city__isnull=False) & Q(city__in=[i.city for i in master.places.all() if i.city]))
    )
    return queryset.values_list('id')


def get_warehouses_by_order_number(order_number):
    repair_order = RepairOrder.objects.filter(number=order_number).first()
    if not repair_order:
        return []
    if repair_order.address:
        queryset = VendorWarehouse.objects.filter(
            region__dadata_name=repair_order.address.region
        )
        queryset = queryset.filter(
            Q(city__isnull=True) | Q(city__dadata_name=repair_order.address.city)
        )
    else:
        log.warning('Repair order (%s) has no address', repair_order)
        queryset = VendorWarehouse.objects.none()
    return queryset.values_list('id')


def get_vendors_by_warehouses(warehouses):
    return Vendor.objects.filter(warehouses=warehouses).distinct()


def get_vendor_by_email(email):
    vendor = Vendor.objects.filter(email=email).first()
    if not vendor:
        log.warning('Vendor with email (%s) deos not exist', email)
    return vendor


def get_repair_price_for_region_name(device_model_repair, region_name, return_default=False):
    price = device_model_repair.prices.filter(region__dadata_name=region_name).first()

    if price:
        return price.price
    if return_default:
        return (
            device_model_repair.prices
            .filter(region=Region.get_default())
            .values('price')
            .first()
            .get('price')
        )
    return 0


def get_sell_order_price_by_region(device_model_sell, region_name, return_default=False):
    price = device_model_sell.prices.filter(region__dadata_name=region_name).first()
    if price:
        return price.price
    if return_default:
        price = (
            device_model_sell.prices
            .filter(region=Region.get_default())
            .values('price')
            .first()
        )
        return price.get('price') if price else 0
    return 0


def get_part_price_by_region_or_default(part, region_name):
    price = part.prices.filter(region__dadata_name=region_name).first()
    if price:
        return price.price
    else:
        price = (
            part.prices
            .filter(region=Region.get_default())
            .values('price')
            .first()
        )
        return price.get('price') if price else 0
