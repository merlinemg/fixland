# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from grappelli_nested.admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline, NestedInlineModelAdmin

from .models import Device, DeviceModel, DeviceColor, DeviceModelColor
from .models import DeviceModelRepair, Repair, DeviceModelSell, Status, Memory, Version, Part, DeviceModelRepairCommission, VendorPart, Vendor, VendorRegion, VendorWarehouse, PartArticle, DeviceModelRepairPrice, DeviceModelSellPrice, PartPrice


class DeviceModelColorAdmin(NestedTabularInline):
    model = DeviceModelColor
    extra = 0


class ModerRepairPartInline(NestedTabularInline):
    model = DeviceModelRepair.parts.through
    extra = 0
    raw_id_fields = ('part',)
    autocomplete_lookup_fields = {
        'fk': ['part'],
    }


class DeviceModelRepairCommissionInline(NestedInlineModelAdmin):
    model = DeviceModelRepairCommission
    extra = 0


class DeviceModelRepairPriceInline(NestedInlineModelAdmin):
    model = DeviceModelRepairPrice
    extra = 1


class DeviceModelRepairAdmin(NestedTabularInline):
    model = DeviceModelRepair
    extra = 0
    inlines = [DeviceModelRepairPriceInline, ModerRepairPartInline, DeviceModelRepairCommissionInline]

    def get_queryset(self, request):
        qs = super(DeviceModelRepairAdmin, self).get_queryset(request)
        return qs.prefetch_related('commissions')


class DeviceModelSellPriceInline(NestedInlineModelAdmin):
    model = DeviceModelSellPrice
    extra = 0


class DeviceModelSellAdmin(NestedStackedInline):
    model = DeviceModelSell
    inlines = (DeviceModelSellPriceInline, )
    extra = 0


class DeviceModelAdmin(NestedModelAdmin):
    list_display = ('name', 'device', 'slug', 'priority', 'model_number', 'visible')
    list_editable = ('priority', 'visible')
    list_filter = ('device', )
    model = DeviceModel
    inlines = (DeviceModelColorAdmin, DeviceModelRepairAdmin, DeviceModelSellAdmin)

    def get_queryset(self, request):
        qs = super(DeviceModelAdmin, self).get_queryset(request)
        return qs.select_related('device')


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'priority')
    list_editable = ('priority', )


class RepairAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'visible')
    list_editable = ('visible', )


class DeviceColorAdmin(admin.ModelAdmin):
    list_display = ('name', 'hex', 'slug')


class PartPriceInline(admin.TabularInline):
    model = PartPrice
    extra = 0


class PartAdmin(admin.ModelAdmin):
    list_display = ('name', 'article', 'default_price', '_colors')
    search_fields = ('name', 'article')
    inlines = (PartPriceInline, )

    def _colors(self, obj):
        return ', '.join(obj.colors.values_list('name', flat=True).all())
    _colors.short_description = _('Цвета')


class VendorRegionInline(admin.TabularInline):
    model = VendorRegion
    extra = 1


class VendorWarehouseInline(admin.TabularInline):
    model = VendorWarehouse
    extra = 0


class VendorAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'last_imported_at')
    inlines = (VendorRegionInline, VendorWarehouseInline, )


class VendorPartAdmin(admin.ModelAdmin):
    list_display = ('article', 'name', 'vendor', 'warehouse', 'exists', 'last_imported_at', 'part_article')
    list_filter = ('vendor', 'warehouse', )
    search_fields = ('article', 'name', 'warehouse__code', 'serial_number')
    readonly_fields = ('part_article',)
    fields = ('vendor', 'warehouse', 'part_article', 'part', 'price', 'article', 'name', 'serial_number', 'color', 'exists', 'last_imported_at')

    def part_article(self, object):
        if object.part:
            return object.part.article

    part_article.short_description = _('Артикул нашей запчасти')


class VendorWarehouseAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'vendor', 'region', 'city')
    list_filter = ('vendor', 'region', 'city')
    search_fields = ('code', 'name', 'vendor__name')


class PartArticleAdmin(admin.ModelAdmin):
    list_display = ('article', 'vendor_article', 'vendor',)
    list_filter = ('article', 'vendor_article', 'vendor')
    search_fields = ('article', 'vendor_article')


admin.site.register(Device, DeviceAdmin)
admin.site.register(DeviceModel, DeviceModelAdmin)
admin.site.register(DeviceColor, DeviceColorAdmin)
admin.site.register(Repair, RepairAdmin)
admin.site.register(Status)
admin.site.register(Memory)
admin.site.register(Version)
admin.site.register(Part, PartAdmin)
admin.site.register(VendorPart, VendorPartAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(VendorWarehouse, VendorWarehouseAdmin)
admin.site.register(PartArticle, PartArticleAdmin)
admin.site.register(DeviceModelRepair)
admin.site.register(DeviceModelRepairPrice)
admin.site.register(DeviceModelSell)
admin.site.register(DeviceModelSellPrice)
