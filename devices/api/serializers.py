from rest_framework import serializers

from ..models import Part, DeviceModelRepairPart, Vendor, VendorPart, VendorWarehouse


class PartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Part


class ModelRepairPartSerializer(serializers.ModelSerializer):
    part = PartSerializer(many=False)

    class Meta:
        model = DeviceModelRepairPart
        fields = ('quantity', 'part')


class VendorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vendor


class VendorWarehouseSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField()
    region = serializers.SerializerMethodField()

    class Meta:
        model = VendorWarehouse

    def get_city(self, obj):
        return obj.city.name if obj.city else None

    def get_region(self, obj):
        return obj.region.name if obj.region else None


class VendorPartSerializer(serializers.ModelSerializer):
    vendor = VendorSerializer(many=False)
    warehouse = VendorWarehouseSerializer(many=False)

    class Meta:
        model = VendorPart


class VendorWarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorWarehouse
        fields = ('vendor', 'code', 'name', 'address', )
