import factory
from decimal import Decimal

from .models import Device, DeviceColor, Repair, DeviceModel, DeviceModelColor, DeviceModelRepair, Part, DeviceModelRepairPart, DeviceModelRepairCommission, Vendor


class VendorFactory(factory.DjangoModelFactory):
    class Meta:
        model = Vendor

    name = factory.Sequence(lambda n: 'Vendor {}'.format(n))
    integration_soap1s_api_key = 'testapikey'


class DeviceFactory(factory.DjangoModelFactory):
    class Meta:
        model = Device

    name = factory.Sequence(lambda n: 'Device {}'.format(n))
    priority = 1
    slug = factory.Sequence(lambda n: 'slug {}'.format(n))


class DeviceColorFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceColor

    name = factory.Sequence(lambda n: 'Device color {}'.format(n))
    slug = factory.Sequence(lambda n: 'slug {}'.format(n))


class RepairFactory(factory.DjangoModelFactory):
    class Meta:
        model = Repair

    name = factory.Sequence(lambda n: 'Repair {}'.format(n))
    slug = factory.Sequence(lambda n: 'slug {}'.format(n))


class DeviceModelFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceModel

    name = factory.Sequence(lambda n: 'Device model {}'.format(n))
    device = factory.SubFactory(DeviceFactory)
    priority = 1
    slug = factory.Sequence(lambda n: 'slug {}'.format(n))


class DeviceModelColorFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceModelColor

    device_model = factory.SubFactory(DeviceModelFactory)
    device_color = factory.SubFactory(DeviceColorFactory)
    priority = 1


class DeviceModelRepairFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceModelRepair

    device_model = factory.SubFactory(DeviceModelFactory)
    repair = factory.SubFactory(RepairFactory)
    price = Decimal(1)
    priority = 1


class PartFactory(factory.DjangoModelFactory):
    class Meta:
        model = Part

    name = factory.Sequence(lambda n: 'Part {}'.format(n))
    price = Decimal(1)


class DeviceModelRepairPartFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceModelRepairPart

    model_repair = factory.SubFactory(DeviceModelRepairFactory)
    part = factory.SubFactory(PartFactory)


class DeviceModelRepairCommissionFactory(factory.DjangoModelFactory):
    class Meta:
        model = DeviceModelRepairCommission

    model_repair = factory.SubFactory(DeviceModelRepairFactory)
    commission = 2
    region = factory.SubFactory('accounts.factories.RegionFactory')
