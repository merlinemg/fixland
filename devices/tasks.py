import logging
from fixland.celery import celery_app

from core import utils as core_utils
from .models import VendorWarehouse

log = logging.getLogger(__name__)


@celery_app.task
def sync_parts_availability(repair_order_id):
    ...


@celery_app.task(bind=True)
def update_warehouse_coordinates(self, warehouse_id):
    try:
        warehouse = VendorWarehouse.objects.get(id=warehouse_id)
        coordinates = core_utils.get_coordinates(warehouse.address)
        warehouse.lat = coordinates.split(' ')[0]
        warehouse.lon = coordinates.split(' ')[1]
        warehouse.save()
    except Exception as exc:
        log.exception('Update VendorWarehouse Coordinates Exception')
        raise self.retry(exc=exc, countdown=10, max_retries=3)
