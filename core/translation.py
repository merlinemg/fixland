''' Translations for core models '''

from modeltranslation.translator import translator, TranslationOptions

from .models import LandingReview, LandingPicture, LandingFeature, MyMetaData, StaticText


class LandingReviewTranslationOptions(TranslationOptions):
    fields = ('first_name', 'last_name', 'post', 'company', 'review')


class LandingPictureTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


class LandingFeatureTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


class MyMetaDataTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'description',
        'keywords',
        'author',
        'og:title',
        'og:description',
        'twitter:title',
        'twitter:description',
    )


class StaticTextTranslationOptions(TranslationOptions):
    fields = ('text', )


translator.register(LandingReview, LandingReviewTranslationOptions)
translator.register(LandingFeature, LandingFeatureTranslationOptions)
translator.register(LandingPicture, LandingPictureTranslationOptions)
translator.register(MyMetaData, MyMetaDataTranslationOptions)
translator.register(StaticText, StaticTextTranslationOptions)
