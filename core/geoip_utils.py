import GeoIP
from django.conf import settings

gi = GeoIP.open(settings.GEO_IP_CITY_DB, GeoIP.GEOIP_STANDARD)


def get_geoip_record_by_ip(ip_address):
    return gi.record_by_addr(ip_address)
