# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20150715_1343'),
    ]

    operations = [
        migrations.CreateModel(
            name='StaticText',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('type', models.CharField(choices=[('offer', 'Оферта'), ('payment_rules', 'Правила платежей')], max_length=255, unique=True, verbose_name='Тип')),
                ('text', tinymce.models.HTMLField(verbose_name='Текст')),
            ],
            options={
                'verbose_name_plural': 'Статические тексты',
                'verbose_name': 'Статический текст',
            },
        ),
    ]
