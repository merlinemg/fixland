# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_auto_20151002_1341'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metadata',
            name='allmetadata_ptr',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.CharField(verbose_name='Тип', max_length=255, null=True, choices=[('repair_order_commission', 'Комиссия заказа продажы'), ('sell_order_commission', 'Комиссия заказа покупки'), ('reverse', 'Сторно'), ('partner_comission', 'Комиссия партнера'), ('warranty_order_compensation', 'Компенсация за гарантийный ремонт')], blank=True),
        ),
        migrations.DeleteModel(
            name='MetaData',
        ),
    ]
