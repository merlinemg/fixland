# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20150814_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='parent',
            field=models.ForeignKey(to='core.Transaction', blank=True, null=True, verbose_name='Родительская транзакция'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.CharField(blank=True, max_length=255, choices=[('repair_order_commission', 'Комиссия заказа продажы'), ('sell_order_commission', 'Комиссия заказа покупки'), ('sell_order_commission', 'Сторно')], null=True),
        ),
    ]
