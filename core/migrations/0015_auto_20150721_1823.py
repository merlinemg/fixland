# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20150721_1700'),
    ]

    operations = [
        migrations.AddField(
            model_name='statictext',
            name='text_en',
            field=tinymce.models.HTMLField(verbose_name='Текст', null=True),
        ),
        migrations.AddField(
            model_name='statictext',
            name='text_ru',
            field=tinymce.models.HTMLField(verbose_name='Текст', null=True),
        ),
    ]
