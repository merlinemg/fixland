# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_auto_20150914_1032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.CharField(null=True, choices=[('repair_order_commission', 'Комиссия заказа продажы'), ('sell_order_commission', 'Комиссия заказа покупки'), ('reverse', 'Сторно'), ('partner_comission', 'Комиссия партнера')], blank=True, max_length=255, verbose_name='Тип'),
        ),
    ]
