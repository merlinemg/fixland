# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_auto_20150819_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='parent',
            field=models.ForeignKey(to='core.Transaction', blank=True, null=True, verbose_name='Родительская транзакция', related_name='child'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.CharField(null=True, verbose_name='Тип', blank=True, choices=[('repair_order_commission', 'Комиссия заказа продажы'), ('sell_order_commission', 'Комиссия заказа покупки'), ('reverse', 'Сторно')], max_length=255),
        ),
    ]
