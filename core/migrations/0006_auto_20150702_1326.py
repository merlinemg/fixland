# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20150624_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='landingreview',
            name='company_en',
            field=models.CharField(verbose_name='Компания', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='company_ru',
            field=models.CharField(verbose_name='Компания', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='first_name_en',
            field=models.CharField(verbose_name='Имя', max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='first_name_ru',
            field=models.CharField(verbose_name='Имя', max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='last_name_en',
            field=models.CharField(verbose_name='Фамилия', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='last_name_ru',
            field=models.CharField(verbose_name='Фамилия', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='post_en',
            field=models.CharField(verbose_name='Должность', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='post_ru',
            field=models.CharField(verbose_name='Должность', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='review_en',
            field=models.TextField(verbose_name='Отзыв', null=True),
        ),
        migrations.AddField(
            model_name='landingreview',
            name='review_ru',
            field=models.TextField(verbose_name='Отзыв', null=True),
        ),
    ]
