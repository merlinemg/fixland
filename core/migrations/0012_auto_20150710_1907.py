# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_auto_20150710_1523'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='transaction',
            options={'verbose_name_plural': 'Транзакции', 'verbose_name': 'Транзакция'},
        ),
        migrations.AlterField(
            model_name='transaction',
            name='wmi_order_id',
            field=models.CharField(verbose_name='Id на WalletOne', max_length=255, null=True, blank=True),
        ),
    ]
