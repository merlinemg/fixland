# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import simple_seo.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150702_1356'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyMetaData',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('view_name', models.CharField(db_index=True, max_length=250, unique=True)),
                ('title', simple_seo.fields.TitleTagField(max_length=68)),
                ('title_ru', simple_seo.fields.TitleTagField(max_length=68)),
                ('title_en', simple_seo.fields.TitleTagField(max_length=68)),
                ('keywords', simple_seo.fields.KeywordsTagField(null=True, blank=True, max_length=255)),
                ('keywords_ru', simple_seo.fields.KeywordsTagField(null=True, blank=True, max_length=255)),
                ('keywords_en', simple_seo.fields.KeywordsTagField(null=True, blank=True, max_length=255)),
                ('description', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('description_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('description_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('author', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('author_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('author_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:title', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:title_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:title_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:type', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:image', simple_seo.fields.ImageMetaTagField(null=True, blank=True, upload_to='seo/images/')),
                ('og:url', simple_seo.fields.URLMetaTagField(null=True, blank=True)),
                ('og:description', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:description_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:description_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('og:admins', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:title', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:title_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:title_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:card', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:image', simple_seo.fields.ImageMetaTagField(null=True, blank=True, upload_to='seo/images/')),
                ('twitter:description', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:description_ru', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
                ('twitter:description_en', simple_seo.fields.MetaTagField(null=True, blank=True, max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Мета информации для страниц',
                'abstract': False,
                'verbose_name': 'Мета информация для страниц',
            },
        ),
        # migrations.RemoveField(
        #     model_name='metadata',
        #     name='allmetadata_ptr',
        # ),
        # migrations.DeleteModel(
        #     name='MetaData',
        # ),
    ]
