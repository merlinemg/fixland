# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150624_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='landingreview',
            name='company_photo',
            field=models.ImageField(null=True, upload_to='landing_review', blank=True),
        ),
    ]
