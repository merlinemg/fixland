# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20150624_1807'),
    ]

    operations = [
        migrations.CreateModel(
            name='LandingLogo',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Название', blank=True, null=True)),
                ('picture', models.ImageField(upload_to='landing_logos')),
                ('visible', models.BooleanField(verbose_name='Отображать?', default=True)),
                ('position', models.IntegerField(help_text='По этому полю отзывы сортируются', verbose_name='Позиция')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Логотип для лендинга',
                'verbose_name_plural': 'Логотипы для лендинга',
            },
        ),
    ]
