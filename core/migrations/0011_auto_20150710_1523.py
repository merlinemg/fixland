# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20150710_1029'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='wmi_order_id',
            field=models.IntegerField(null=True, verbose_name='Id на WalletOne', blank=True),
        ),
    ]
