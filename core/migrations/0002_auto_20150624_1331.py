# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='landingreview',
            options={'verbose_name': 'Отзыв для лендинга', 'verbose_name_plural': 'Отзывы для лендинга'},
        ),
        migrations.AddField(
            model_name='landingreview',
            name='post',
            field=models.CharField(verbose_name='Должность', blank=True, max_length=255, null=True),
        ),
    ]
