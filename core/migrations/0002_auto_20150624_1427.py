# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LandingFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(verbose_name='Картинка', upload_to='landing_features')),
                ('title', models.CharField(verbose_name='Название', max_length=255)),
                ('description', models.TextField(verbose_name='Текст')),
                ('visible', models.BooleanField(verbose_name='Отоброжать?', default=True)),
                ('position', models.IntegerField(verbose_name='Позиция', help_text='По этому полю преимущества сортируются')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Преимущество для лендинга',
                'verbose_name_plural': 'Преимущества для лендинга',
            },
        ),
        migrations.CreateModel(
            name='LandingPicture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(verbose_name='Картинка', upload_to='landing_picture')),
                ('title', models.CharField(verbose_name='Слоган', max_length=255)),
                ('description', models.TextField(verbose_name='Описание')),
                ('visible', models.BooleanField(verbose_name='Отоброжать?', default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Картинка для лендинга',
                'verbose_name_plural': 'Картинки для лендинга',
            },
        ),
        migrations.AlterModelOptions(
            name='landingreview',
            options={'verbose_name': 'Отзыв для лендинга', 'verbose_name_plural': 'Отзывы для лендинга'},
        ),
    ]
