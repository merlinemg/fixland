from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.admin import helpers
from .forms import ReverseTransactionForm
from .utils import create_reverse_transaction


def reverse_transaction(modeladmin, request, queryset):
    form = None
    if len(queryset) > 1:
        modeladmin.message_user(request, _('Выберете только одну транзакцию'))
        return HttpResponseRedirect(request.get_full_path())
    transaction = queryset.first()
    if 'reverse' in request.POST:
        form = ReverseTransactionForm(request.POST)
        form.max_amount = transaction.amount
        if form.is_valid():
            create_reverse_transaction(transaction, form.cleaned_data['description'], form.cleaned_data['amount'])

            modeladmin.message_user(request, _('Успешно'))
            return HttpResponseRedirect(request.get_full_path())
    if not form:
        form = ReverseTransactionForm(initial={
            '_selected_action': request.POST.getlist(helpers.ACTION_CHECKBOX_NAME),
            'amount': transaction.amount,
            'description': _('Возврат денег по заказу №{}'.format(getattr(transaction.content_object, 'number', 'unknown')))
        })
        form.max_amount = transaction.amount
    return render(request, 'admin/reverse_transaction.html', {'transaction': transaction, 'form': form, 'title': _('Сторнирование')})
reverse_transaction.short_description = _('Сторнировать')
