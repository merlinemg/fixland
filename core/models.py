# coding: utf-8
from tinymce.models import HTMLField
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.dispatch import receiver
from django.db.models.signals import post_save
from simple_seo.models import _post_init_field_populate
from simple_seo.fields import (
    TitleTagField,
    KeywordsTagField,
    MetaTagField,
    ImageMetaTagField,
    URLMetaTagField,
    BaseTagField)


class LandingReview(models.Model):
    first_name = models.CharField(_('Имя'), max_length=255)
    last_name = models.CharField(_('Фамилия'), max_length=255, blank=True, null=True)
    post = models.CharField(_('Должность'), max_length=255, blank=True, null=True)
    company = models.CharField(_('Компания'), max_length=255, blank=True, null=True)
    review = models.TextField(_('Отзыв'))
    photo = models.ImageField(upload_to='landing_review')
    company_photo = models.ImageField(upload_to='landing_review', blank=True, null=True)
    visible = models.BooleanField(_('Отображать?'), default=True)
    position = models.IntegerField(_('Позиция'), help_text=_('По этому полю отзывы сортируются'))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Отзыв для лендинга')
        verbose_name_plural = _('Отзывы для лендинга')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def short_review(self):
        return '{}...'.format(self.review[:50]) if len(self.review) > 50 else self.review


class LandingLogo(models.Model):
    name = models.CharField(_('Название'), max_length=255, blank=True, null=True)
    picture = models.ImageField(upload_to='landing_logos')
    visible = models.BooleanField(_('Отображать?'), default=True)
    position = models.IntegerField(_('Позиция'), help_text=_('По этому полю отзывы сортируются'))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Логотип для лендинга')
        verbose_name_plural = _('Логотипы для лендинга')

    def __str__(self):
        return self.name


class LandingPicture(models.Model):
    photo = models.ImageField(_('Картинка'), upload_to='landing_picture')
    title = models.CharField(_('Слоган'), max_length=255)
    description = models.TextField(_('Описание'))

    visible = models.BooleanField(_('Отображать?'), default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Картинка для лендинга')
        verbose_name_plural = _('Картинки для лендинга')

    def __str__(self):
        return self.title


class LandingFeature(models.Model):
    photo = models.ImageField(_('Картинка'), upload_to='landing_features')
    title = models.CharField(_('Название'), max_length=255)
    description = models.TextField(_('Текст'))

    visible = models.BooleanField(_('Отображать?'), default=True)
    position = models.IntegerField(_('Позиция'), help_text=_('По этому полю преимущества сортируются'))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Преимущество для лендинга')
        verbose_name_plural = _('Преимущества для лендинга')

    def __str__(self):
        return self.title


class MyMetaData(models.Model):
    """
    My Seo Model
    """
    id = models.AutoField(primary_key=True)
    view_name = models.CharField(max_length=250, null=False, blank=False, choices=(), unique=True, db_index=True)
    title = TitleTagField(null=False, blank=False)
    keywords = KeywordsTagField(null=False, blank=False)
    description = MetaTagField(null=False, blank=False)
    author = MetaTagField(null=False, blank=False)
    og_title = MetaTagField(name='og:title', max_length=95, null=True, blank=True, populate_from='title')
    og_type = MetaTagField(name='og:type', max_length=15, null=True, blank=True)
    og_image = ImageMetaTagField(name='og:image', upload_to='seo/images/', null=True, blank=True)
    og_url = URLMetaTagField(name='og:url', null=True, blank=True)
    og_description = MetaTagField(name='og:description', max_length=297, null=True, blank=True,
                                  populate_from='description')
    og_admins = MetaTagField(name='og:admins', max_length=297, null=True, blank=True)
    twitter_title = MetaTagField(name='twitter:title', max_length=70, null=True, blank=True, populate_from='og:title')
    twitter_card = MetaTagField(name='twitter:card', max_length=15, null=True, blank=True)
    twitter_image = ImageMetaTagField(name='twitter:image', upload_to='seo/images/', null=True, blank=True,
                                      populate_from='og:image')
    twitter_description = MetaTagField(name='twitter:description', max_length=200, null=True, blank=True,
                                       populate_from='og:description')

    def __init__(self, *args, **kwargs):
        super(MyMetaData, self).__init__(*args, **kwargs)
        models.signals.post_init.connect(_post_init_field_populate, sender=self.__class__)

    class Meta:
        abstract = False
        app_label = 'core'
        verbose_name = _('Мета информация для страниц')
        verbose_name_plural = _('Мета информации для страниц')

# register(MetaData)


class Transaction(models.Model):
    HOLD = 'hold'
    SUCCESS = 'success'
    STATUSES = (
        (HOLD, _('Заморожен')),
        (SUCCESS, _('Завершен'))
    )

    REPAIR_ORDER_COMMISSION = 'repair_order_commission'
    SELL_ORDER_COMMISSION = 'sell_order_commission'
    REVERSE = 'reverse'
    PARTNER_COMMISSION = 'partner_comission'
    WARRANTY_ORDER_COMPENSATION = 'warranty_order_compensation'
    TYPES = (
        (REPAIR_ORDER_COMMISSION, _('Комиссия заказа продажы')),
        (SELL_ORDER_COMMISSION, _('Комиссия заказа покупки')),
        (REVERSE, _('Сторно')),
        (PARTNER_COMMISSION, _('Комиссия партнера')),
        (WARRANTY_ORDER_COMPENSATION, _('Компенсация за гарантийный ремонт')),
    )

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    type = models.CharField(_('Тип'), max_length=255, choices=TYPES, blank=True, null=True)

    wmi_order_id = models.CharField(_('Id на WalletOne'), max_length=255, blank=True, null=True)
    sender = models.ForeignKey('accounts.User', verbose_name=_('Отправитель'), blank=True, null=True, related_name='sender_transactions')
    recipient = models.ForeignKey('accounts.User', verbose_name=_('Получатель'), blank=True, null=True, related_name='recipient_transactions')
    amount = models.DecimalField(max_digits=20,
                                 decimal_places=2,
                                 verbose_name=_('Сумма'))
    currency = models.CharField(_('Валюта'), max_length=255, choices=settings.CURRENCIES, default=settings.CURRENCY_RUB)
    status = models.CharField(_('Статус'), max_length=255, choices=STATUSES)
    is_credit_money = models.BooleanField(_('Кредитные?'), default=False, help_text=_('Оплата проводилась кредитными стредствами?'))
    description = models.TextField(verbose_name=_('Описание'))

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    parent = models.ForeignKey('self', related_name='child', verbose_name=_('Родительская транзакция'), blank=True, null=True)

    class Meta:
        verbose_name = _('Транзакция')
        verbose_name_plural = _('Транзакции')


class StaticText(models.Model):
    TYPE_OFFER = 'offer'
    TYPE_PAYMENT_RULES = 'payment_rules'
    TYPE_CHOICES = (
        (TYPE_OFFER, _('Оферта')),
        (TYPE_PAYMENT_RULES, _('Правила платежей'))
    )
    type = models.CharField(_('Тип'), max_length=255, choices=TYPE_CHOICES, unique=True)
    text = HTMLField(_('Текст'))

    class Meta:
        verbose_name = _('Статический текст')
        verbose_name_plural = _('Статические тексты')

    def __str__(self):
        return self.get_type_display()


@receiver(post_save, sender=Transaction)
def close_master_credit(sender, instance, created, **kwargs):
    if instance.recipient and instance.recipient.is_master() and instance.recipient.master.take_credit_date and instance.recipient.master.balance >= 0:
        master = instance.recipient.master
        master.take_credit_date = None
        master.save()
