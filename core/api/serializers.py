from rest_framework import serializers

from ..models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%d-%m-%Y')
    updated_at = serializers.DateTimeField(format='%d-%m-%Y')
    repair_order_id = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ('sender', 'recipient', 'amount', 'description', 'currency', 'created_at', 'updated_at', 'repair_order_id')

    def get_repair_order_id(self, obj):
        if (obj.content_type and obj.content_type.model == 'repairorder' and obj.content_object):
            return obj.content_object.number
        return None
