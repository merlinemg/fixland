import requests
import logging
from django.conf import settings
from django.core.exceptions import MultipleObjectsReturned
import simplejson
import urllib
from urllib.request import urlopen
from urllib.parse import quote
from accounts.models import MetroLine, MetroStation, City
from accounts.utils import update_address_metro
from .utils import csv_file_to_arr
from django.utils.encoding import smart_str

log = logging.getLogger(__name__)


def update_metro_stations_from_file(file):
    csv_arr = csv_file_to_arr(file.read())
    created_metro_city_count = 0
    created_metro_line_count = 0
    created_metro_count = 0
    for row in csv_arr:
        row_metro = row[0]
        row_line = row[1]
        row_city = row[2]

        city, created_city = City.objects.get_or_create(name__iexact=row_city)
        metro_line, created_metro_line = MetroLine.objects.get_or_create(name__iexact=row_line, city=city)
        try:
            metro_station, created_metro = MetroStation.objects.get_or_create(name__iexact=row_metro.lower(),
                                                                              line=metro_line)
        except MultipleObjectsReturned:
            log.exception('Multiple metro station %s', row_metro.get('name'))
        if created_city:
            created_metro_city_count += 1
        if created_metro_line:
            created_metro_line_count += 1
        if created_metro:
            created_metro_count += 1
    return {
        'created_city': created_metro_city_count,
        'created_line': created_metro_line_count,
        'created_metro': created_metro_count,
    }


def parse_and_update_address(address):

    response = get_address_all_details(address.raw)
    if not response:
        return {}
    suggestions = response.get('suggestions')
    if not suggestions:
        return {}
    house_type = response.get('house_type')
    flat_type = response.get('flat_type') if response.get('flat_type') else ''
    block_type = response.get('block_type')
    block = response.get('block')
    house = response.get('house')
    house_block_array = [house_type, house, block_type, block]
    house_block = ' '.join([i for i in house_block_array if i])
    address.country = response.get('country')
    address.region = response.get('region')
    address.city = response.get('city') or response.get('settlement') or ''
    address.zip_code = response.get('postal_code')
    address.street = response.get('street')
    address.house = house_block
    address.appartment = '{} {}'.format(flat_type, response.get('flat')) if response.get('flat') else ''
    address.lon=response.get('lng')
    address.lat=response.get('lat')
    address.save()
    update_address_metro(address.id)


def get_address_all_details(address):
    url_quote = quote(smart_str(address))
    url = settings.GOOGLE_ADDRESS_API_URL % url_quote
    response = urlopen(url).read()
    results = simplejson.loads(response)
    lat = str(results['results'][0]['geometry']['location']['lat'])
    lng = str(results['results'][0]['geometry']['location']['lng'])
    get_results = results['results']
    address_details = {}
    address_details['lat'] = lat
    address_details['lng'] = lng
    for result in get_results:
        for component in result["address_components"]:
            address_details['suggestions'] = True
            if 'administrative_area_level_1' in component["types"]:
                region = component["long_name"]
                address_details['region'] = region
            if 'country' in component["types"]:
                country = component["long_name"]
                address_details['country'] = country
            if 'sublocality' in component["types"] or 'route' in component["types"]:
                street = component["long_name"]
                address_details['street'] = street
            if 'locality' in component["types"]:
                city = component["long_name"]
                address_details['city'] = city
            if 'postal_code' in component["types"]:
                zip_code = component["long_name"]
                address_details['postal_code'] = zip_code
    return address_details
