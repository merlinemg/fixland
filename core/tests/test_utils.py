import httpretty
from django.conf import settings
from django.test import TestCase

from core.utils import GetMetroException
from .. import utils

# Taras: These tests because of patching socket library - broke another tests
# class UtilsTestCase(TestCase):
#     def setUp(self):
#         self.address = 'Москва, улица Строителей, 3'
#         self.bad_address = 'Москadasdasdва, улица Строителейaaaaasdasdaa, 31312313asd2'
#         self.response_coordinates = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"Москва, улица Строителей, 3","found":"3","results":"1"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Москва, улица Строителей, 3","precision":"exact","AddressDetails":{"Country":{"AddressLine":"Москва, улица Строителей, 3","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"улица Строителей","Premise":{"PremiseNumber":"3"}}}}}}}}},"description":"Москва, Россия","name":"улица Строителей, 3","boundedBy":{"Envelope":{"lowerCorner":"37.529079 55.680473","upperCorner":"37.545536 55.689771"}},"Point":{"pos":"37.537308 55.685122"}}}]}}}'
#         self.response_coordinates_bad_address = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"Москadasdasdва, улица Строителейaaaaasdasdaa, 31312313asd2","found":"0","results":"1"}},"featureMember":[]}}}'
#         self.response_metro = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"37.537308 55.685122","found":"196","results":"1","boundedBy":{"Envelope":{"lowerCorner":"37.287307 55.434324","upperCorner":"37.787309 55.934316"}},"Point":{"pos":"37.537308 55.685122"},"kind":"metro"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"metro","text":"Россия, Москва, Сокольническая линия, метро Университет","precision":"other","AddressDetails":{"Country":{"AddressLine":"Москва, Сокольническая линия, метро Университет","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"Сокольническая линия","Premise":{"PremiseName":"метро Университет"}}}}}}}}},"description":"Сокольническая линия, Москва, Россия","name":"метро Университет","boundedBy":{"Envelope":{"lowerCorner":"37.526304 55.687792","upperCorner":"37.542761 55.697087"}},"Point":{"pos":"37.534532 55.692440"}}}]}}}';

#     @httpretty.activate
#     def test_get_coordinates(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, body=self.response_coordinates, content_type="application/json")
#         coordinates = utils.get_coordinates(self.address)
#         self.assertEqual(coordinates, '37.537308 55.685122')
#         httpretty.disable()

#     @httpretty.activate
#     def test_get_coordinater_bad_address_should_return_none(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, body=self.response_coordinates_bad_address, content_type="application/json")
#         self.assertRaises(GetMetroException, utils.get_coordinates, self.bad_address)
#         httpretty.disable()

#     @httpretty.activate
#     def test_get_metro(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, responses=[
#                                httpretty.Response(body=self.response_coordinates),
#                                httpretty.Response(body=self.response_metro)])
#         metro = utils.get_metro_by_address(self.address)
#         self.assertEqual(metro.get('name'), 'метро Университет')
#         self.assertEqual(metro.get('line'), 'Сокольническая линия')
#         httpretty.disable()

#     @httpretty.activate
#     def test_get_metro_bad_address_should_return_none(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, body=self.response_coordinates_bad_address, content_type="application/json")
#         self.assertRaises(GetMetroException, utils.get_metro_by_address, self.bad_address)
