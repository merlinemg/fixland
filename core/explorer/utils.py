from django.http import HttpResponse
from django.db import DatabaseError

from explorer.utils import get_filename_for_title, write_csv


def build_stream_response(query):
    data = csv_report(query)
    response = HttpResponse(data, content_type='text')
    return response


def build_download_response(query):
    data = csv_report(query)
    response = HttpResponse(data, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % (
        get_filename_for_title(query.title)
    )
    # response['Content-Length'] = len(data)
    return response


def csv_report(query):
    try:
        res = query.execute()
        data = []
        for row in res.data:
            data.append([i.decode('utf8') if isinstance(i, bytes) else i for i in row])
        return write_csv(res.headers, data)
    except DatabaseError as e:
        return str(e)
