from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_POST, require_GET

from explorer.views import view_permission, change_permission
from explorer.models import Query
from explorer.utils import url_get_params

from .utils import build_download_response, build_stream_response


@view_permission
@require_GET
def download_query(request, query_id):
    return _csv_response(request, query_id, False)


@view_permission
@require_GET
def view_csv_query(request, query_id):
    return _csv_response(request, query_id, True)


def _csv_response(request, query_id, stream=False):
    query = get_object_or_404(Query, pk=query_id)
    query.params = url_get_params(request)
    return build_stream_response(query) if stream else build_download_response(query)


@change_permission
@require_POST
def download_csv_from_sql(request):
    sql = request.POST.get('sql')
    return build_download_response(Query(sql=sql, title="Playground", params=url_get_params(request)))
