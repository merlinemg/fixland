from decimal import Decimal
import factory

from devices import factories as devices_factories
from .models import RepairOrder, RepairOrderItem, RejectedOrder, RepairOrderNotifiedMaster


class RepairOrderFactory(factory.DjangoModelFactory):
    class Meta:
        model = RepairOrder

    user = factory.SubFactory('accounts.factories.UserFactory')
    address = factory.SubFactory('accounts.factories.AddressFactory')
    device_model_color = factory.SubFactory('devices.factories.DeviceModelColorFactory')


class RepairOrderItemFactory(factory.DjangoModelFactory):
    class Meta:
        model = RepairOrderItem

    repair_order = factory.SubFactory(RepairOrderFactory)
    device_model_repair = factory.SubFactory('devices.factories.DeviceModelRepairFactory')
    price = Decimal(1)


class RejectedOrderFactory(factory.DjangoModelFactory):
    class Meta:
        model = RejectedOrder

    repair_order = factory.SubFactory(RepairOrderFactory)
    master = factory.SubFactory('masters.factories.MasterFactory')
    comment = 'Test'


class RepairOrderNotifiedMasterFactory(factory.DjangoModelFactory):
    class Meta:
        model = RepairOrderNotifiedMaster

    repair_order = factory.SubFactory(RepairOrderFactory)
    master = factory.SubFactory('masters.factories.MasterFactory')


def create_order(master):
    order = RepairOrderFactory()

    dmr1 = devices_factories.DeviceModelRepairFactory(
        device_model=order.device_model_color.device_model,
        price=Decimal(1001)
    )
    dmr2 = devices_factories.DeviceModelRepairFactory(
        device_model=order.device_model_color.device_model,
        price=Decimal(2002)
    )

    devices_factories.DeviceModelRepairCommissionFactory(
        model_repair=dmr1,
        status=master.get_status()
    )
    devices_factories.DeviceModelRepairCommissionFactory(
        model_repair=dmr2,
        status=master.get_status()
    )

    RepairOrderItemFactory(repair_order=order, device_model_repair=dmr1)
    RepairOrderItemFactory(repair_order=order, device_model_repair=dmr2)
    return order
