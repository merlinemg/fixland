# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0019_auto_20150819_1108'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='coupon',
            field=models.ForeignKey(to='orders.Coupon', null=True, blank=True, verbose_name='Купон'),
        ),
    ]
