# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0023_auto_20150825_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='number',
            field=models.CharField(null=True, max_length=255, verbose_name='Номер заказа'),
        ),
    ]
