# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def delete_duplicate_order_parts(apps, schema_editor):
    RepairOrderItemPart = apps.get_model("orders", "RepairOrderItemPart")
    RepairOrder = apps.get_model("orders", "RepairOrder")
    order_colors = {i.id: i.device_model_color.device_color for i in RepairOrder.objects.all()}
    order_parts = RepairOrderItemPart.objects.filter(part__colors__isnull=False)
    for order_part in order_parts:
        order_color = order_colors[order_part.repair_order_item.repair_order.id]
        if order_color not in order_part.part.colors.all():
            order_part.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0036_repairorder_partner'),
    ]

    operations = [
        migrations.RunPython(delete_duplicate_order_parts),
    ]
