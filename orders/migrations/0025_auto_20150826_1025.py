# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import orders.utils


def generate_numbers(apps, schema_editor):
    RepairOrder = apps.get_model("orders", "RepairOrder")
    for order in RepairOrder.objects.values('id'):
        RepairOrder.objects.filter(id=order['id']).update(number=orders.utils.generate_repair_order_number())


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0024_auto_20150826_1025'),
    ]

    operations = [
        migrations.RunPython(generate_numbers),
    ]
