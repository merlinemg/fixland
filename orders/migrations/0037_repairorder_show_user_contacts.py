# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0036_repairorder_partner'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='show_user_contacts',
            field=models.BooleanField(default=False, verbose_name='Показывать контакты'),
        ),
    ]
