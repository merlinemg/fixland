# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_add_feedback'),
        ('orders', '0005_remove_repairorder_ip_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='master',
            field=models.ForeignKey(verbose_name='Мастер', null=True, blank=True, to='masters.Master'),
        ),
    ]
