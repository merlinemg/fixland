# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0016_merge'),
        ('orders', '0016_auto_20150811_1443'),
    ]

    operations = [
    ]
