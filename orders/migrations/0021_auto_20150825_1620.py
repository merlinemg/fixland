# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0023_auto_20150817_1458'),
        ('orders', '0020_auto_20150819_1343'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='auto_appliable',
            field=models.BooleanField(default=False, verbose_name='Применять автоматически?'),
        ),
        migrations.AddField(
            model_name='coupon',
            name='device_models',
            field=models.ManyToManyField(blank=True, to='devices.DeviceModel', verbose_name='Модели'),
        ),
        migrations.AddField(
            model_name='coupon',
            name='discount_percent',
            field=models.IntegerField(blank=True, null=True, verbose_name='Скидка в процентах'),
        ),
        migrations.AddField(
            model_name='coupon',
            name='expiration_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Срок действия'),
        ),
        migrations.AddField(
            model_name='coupon',
            name='repairs',
            field=models.ManyToManyField(blank=True, to='devices.Repair', verbose_name='Поломки'),
        ),
        migrations.AddField(
            model_name='coupon',
            name='use_count',
            field=models.IntegerField(blank=True, null=True, verbose_name='Количество раз использования'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='discount',
            field=models.IntegerField(blank=True, null=True, verbose_name='Скидка фиксированная'),
        ),
    ]
