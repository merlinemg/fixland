# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0014_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='repairorder',
            options={'verbose_name_plural': 'Заказы ремонта', 'verbose_name': 'Заказ ремонта', 'ordering': ('-starttime',)},
        ),
        migrations.AlterModelOptions(
            name='sellorder',
            options={'verbose_name_plural': 'Заказы продажи', 'verbose_name': 'Заказ продажи', 'ordering': ('-starttime',)},
        ),
    ]
