# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0014_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='repairorder',
            options={'ordering': ('-starttime',), 'verbose_name': 'Заказ ремонта', 'verbose_name_plural': 'Заказы ремонта'},
        ),
        migrations.AlterModelOptions(
            name='sellorder',
            options={'ordering': ('-starttime',), 'verbose_name': 'Заказ продажи', 'verbose_name_plural': 'Заказы продажи'},
        ),
    ]
