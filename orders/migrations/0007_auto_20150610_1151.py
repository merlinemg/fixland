# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_auto_20150609_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='status',
            field=models.CharField(max_length=255, choices=[('new', 'Новый'), ('done', 'Выполнен')], verbose_name='Статус', default='new'),
        ),
    ]
