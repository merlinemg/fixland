# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0028_auto_20150828_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='description',
            field=models.CharField(max_length=255, verbose_name='Описание', blank=True, null=True),
        ),
    ]
