# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0037_auto_20150924_1724'),
        ('orders', '0037_repairorder_show_user_contacts'),
    ]

    operations = [
    ]
