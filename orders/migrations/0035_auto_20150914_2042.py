# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def create_device_model_repair_parts(apps, schema_editor):
    RepairOrderItemPart = apps.get_model("orders", "RepairOrderItemPart")
    RepairOrderItem = apps.get_model("orders", "RepairOrderItem")
    for repair_order_item in RepairOrderItem.objects.all():
        order = repair_order_item.repair_order
        for repair_part in repair_order_item.device_model_repair.parts_quantity.all():
            if repair_part.part.color and repair_part.part.color != order.device_model_color.device_color:
                continue
            RepairOrderItemPart.objects.create(
                repair_order_item=repair_order_item,
                part=repair_part.part,
                price=repair_part.part.price,
                quantity=repair_part.quantity
            )


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0034_auto_20150914_2037'),
    ]

    operations = [
        migrations.RunPython(create_device_model_repair_parts),
    ]
