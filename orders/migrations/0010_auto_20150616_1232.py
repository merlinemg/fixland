# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_auto_20150616_1221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorderitem',
            name='repair_order',
            field=models.ForeignKey(to='orders.RepairOrder', verbose_name='Заказ', related_name='repair_order_items'),
        ),
    ]
