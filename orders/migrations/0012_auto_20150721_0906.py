# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_auto_20150626_1402'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='repairorder',
            options={'verbose_name_plural': 'Заказы ремонта', 'ordering': ('-pk',), 'verbose_name': 'Заказ ремонта'},
        ),
        migrations.AlterModelOptions(
            name='sellorder',
            options={'verbose_name_plural': 'Заказы продажи', 'ordering': ('-pk',), 'verbose_name': 'Заказ продажи'},
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='as_soon_as_possible',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Как можно быстрее'),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='starttime',
            field=models.DateTimeField(db_index=True, null=True, blank=True, verbose_name='Удобное время от'),
        ),
    ]
