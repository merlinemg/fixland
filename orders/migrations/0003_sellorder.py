# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0006_auto_20150520_1318'),
        ('devices', '0007_auto_20150526_1338'),
        ('orders', '0002_auto_20150525_1528'),
    ]

    operations = [
        migrations.CreateModel(
            name='SellOrder',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('price', models.DecimalField(max_digits=14, verbose_name='Цена', decimal_places=2)),
                ('starttime', models.DateTimeField(null=True, verbose_name='Удобное время от', blank=True)),
                ('endtime', models.DateTimeField(null=True, verbose_name='Удобное время до', blank=True)),
                ('as_soon_as_possible', models.BooleanField(default=False, verbose_name='Как можно быстрее')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Заказ создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Заказ обновлен')),
                ('address', models.ForeignKey(to='accounts.Address', verbose_name='Адресс')),
                ('device_model_sell', models.ForeignKey(to='devices.DeviceModelSell')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Заказ продажи',
                'verbose_name_plural': 'Заказы продажи',
            },
        ),
    ]
