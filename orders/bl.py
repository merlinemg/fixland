
import simplejson
import urllib
from urllib.request import urlopen
from urllib.parse import quote
import uuid
import logging
from decimal import Decimal
from dateutil import parser
from datetime import timedelta
from constance import config
from django.utils.encoding import smart_str
from geopy.geocoders import Nominatim
from django.db.models import Q, Count, F
from django.utils.timezone import make_aware
from django.utils.timezone import now as utc_now
from django.core.exceptions import MultipleObjectsReturned
from accounts.models import Address, User, Phone, Region
from core.bl import parse_and_update_address
from core.utils import clean_phone, create_reverse_transaction
from core.models import Transaction
from devices.models import DeviceModelColor, DeviceModelRepair, DeviceModelSell, DeviceModelRepairCommission
from devices.bl import get_repair_price_for_region_name, get_sell_order_price_by_region, get_part_price_by_region_or_default
from .models import RepairOrder, RepairOrderItem, SellOrder, RejectedOrder, Coupon, RepairOrderItemPart
from . import tasks
from .permissions import can_use_coupon
from masters.models import Master
from masters.bl import charge_off_master_parts_for_order
from orders import tasks as orders_tasks

log = logging.getLogger(__name__)


def create_order(data, order_type='repair'):
    repair_order = None
    if order_type == 'warranty':
        repair_order = RepairOrder.objects.filter(number=data.get('number')).first() if data.get('number') else None
        if not repair_order:
            return
    email = data.get('userEmail')
    name = data.get('userName')
    parsed_name = data.get('parsedName', {})
    gender = data.get('userGender', None)
    phone_number = data.get('userPhone')
    phone_type = data.get('phoneType')
    raw_address = data.get('userAddress')
    parsed_address = data.get('parsedAddress', {})
    model_slug = data.get('userModel')
    model_color = data.get('userColor')
    coupon_code = data.get('coupon').get('code') if data.get('coupon') else None
    use_repeated_order_coupon = data.get('use_repeated_order_coupon')
    as_soon_as_possible = bool(data.get('isSoonAsPossible', False))
    partner = data.get('partner', None)
    try:
        starttime = get_date_from_string(data.get('startDateTime'))
    except Exception:
        starttime = utc_now()

    try:
        endtime = get_date_from_string(data.get('endDateTime'))
    except Exception:
        endtime = starttime + timedelta(hours=2)
    if as_soon_as_possible:
        starttime = utc_now()
        endtime = utc_now() + timedelta(hours=1)
    if repair_order:
        user = repair_order.user
    else:
        user = get_or_create_user(email, name, parsed_name, phone_number, phone_type, gender)
        get_or_create_phone(user, phone_number, phone_type)
    address = get_or_create_address(user, raw_address, parsed_address)
    if order_type == 'repair':
        return create_repair_order(model_slug, model_color, data.get('selectedRepairs'), user, address, as_soon_as_possible, starttime, endtime, coupon_code, use_repeated_order_coupon, partner)
    elif order_type == 'sell':
        device_model_sell = DeviceModelSell.objects.get(id=data['modelSell'])
        price = get_sell_order_price_by_region(device_model_sell, get_address_details(address)['region'])
        order = SellOrder.objects.create(
            device_model_sell=device_model_sell,
            price=price,
            user=user,
            address=address,
            starttime=starttime,
            endtime=endtime,
            as_soon_as_possible=as_soon_as_possible,
        )
        tasks.process_sell_order.delay(order.id)
    elif order_type == 'warranty':
        create_warranty_order(repair_order, data.get('selectedRepairs'), user, address, as_soon_as_possible, starttime, endtime)


def get_or_create_user(email, name, parsed_name, phone_number, phone_type, gender):
    phone_number = clean_phone(phone_number)
    user = None
    if phone_type == Phone.OWN_PHONE:
        user_phone = Phone.objects.filter(number=phone_number, phone_type=Phone.OWN_PHONE).first()
        if user_phone and user_phone.user:
            user = user_phone.user
    if not user:
        user = User.objects.filter(Q(username=email) | Q(email=email), email__isnull=False).exclude(email='').first()
    if not user:
        if email:
            username = email
        elif phone_type == Phone.OWN_PHONE and phone_number:
            username = phone_number
        else:
            username = uuid.uuid4()
        user = User.objects.create(
            username=username,
            email=email,
            first_name=parsed_name.get('firstName', ''),
            last_name=parsed_name.get('lastName', ''),
            middle_name=parsed_name.get('middleName', ''),
            gender=gender or 'male'
        )
    elif email and not user.email:
        if parsed_name.get('firstName', None) and parsed_name.get('firstName') != user.first_name:
            user.first_name = parsed_name.get('firstName')
        if parsed_name.get('lastName', None) and parsed_name.get('lastName') != user.last_name:
            user.last_name = parsed_name.get('lastName')
        if parsed_name.get('middleName', None) and parsed_name.get('middleName') != user.middle_name:
            user.middle_name = parsed_name.get('middleName')
        user.email = email
        user.save()
    return user


def get_or_create_phone(user, phone_number, phone_type):
    phone_number = clean_phone(phone_number)
    phone, created = Phone.objects.get_or_create(number=phone_number, user=user, defaults={'phone_type': phone_type})
    return phone


def get_or_create_address(user, raw_address, parsed_address):
    try:
        address, created = Address.objects.get_or_create(user=user, raw=raw_address, defaults=parsed_address)
    except MultipleObjectsReturned:
        addresses = Address.objects.filter(user=user, raw=raw_address)
        log.error('There are many (%s) the same addresses for user with id=%s ids of addresses: %s', user.id, addresses.count(), addresses.values_list('id', flat=True))
        address = addresses.first()
    parse_and_update_address(address)
    return address


def create_repair_order(model_slug, model_color, selected_model_repairs, user, address, as_soon_as_possible, starttime, endtime, coupon_code, use_repeated_order_coupon, partner):
    device_model_color = (
        DeviceModelColor.objects
        .select_related('device_model')
        .get(device_model__slug=model_slug, device_color__slug=model_color)
    )
    order = RepairOrder.objects.create(
        device_model_color=device_model_color,
        user=user,
        address=address,
        as_soon_as_possible=as_soon_as_possible,
        starttime=starttime,
        endtime=endtime,
    )

    if partner:
        order.partner = partner
        order.save()

    add_repairs_to_order(order, selected_model_repairs)
    repeated_order_coupon = get_repeated_order_coupon() if use_repeated_order_coupon else None
    coupon = Coupon.objects.filter(code=coupon_code).first() if coupon_code else None
    if repeated_order_coupon and coupon:
        repeated_order_coupon_discount = order.get_discount_for_coupon(repeated_order_coupon)
        coupon_discount = order.get_discount_for_coupon(coupon)
        if repeated_order_coupon_discount > coupon_discount:
            coupon = repeated_order_coupon
    elif repeated_order_coupon:
        coupon = repeated_order_coupon
    if coupon and can_use_coupon(coupon, order):
        order.coupon = coupon
        order.save()
        coupon.accept()
    tasks.process_repair_order.apply_async(countdown=config.NEW_REPAIR_ORDER_NOTIFY_DELAY * 60, args=(order.id, ))
    return order


def create_warranty_order(repair_order, selected_model_repairs, user, address, as_soon_as_possible, starttime, endtime):
    warranty_order = RepairOrder.objects.create(
        parent=repair_order,
        is_warranty=True,
        status=RepairOrder.STATUS_PROCESSING,
        device_model_color=repair_order.device_model_color,
        user=user,
        address=address,
        as_soon_as_possible=as_soon_as_possible,
        starttime=starttime,
        endtime=endtime,
    )
    add_repairs_to_order(warranty_order, selected_model_repairs)
    tasks.new_warranty_order_to_admin.apply_async(countdown=5, args=(warranty_order.id, ))


def add_repairs_to_order(repair_order, selected_model_repairs):
    selected_repairs = [i['repair_id'] for i in selected_model_repairs]
    repairs_by_group_repair = repair_order.device_model_color.device_model.get_grouped_repairs()
    if repairs_by_group_repair:
        for repair in repairs_by_group_repair:
            if all(i in selected_repairs for i in repair['repairs']):
                selected_repairs = [x for x in selected_repairs if x not in repair['repairs']]
                selected_repairs.append(repair['id'])
                selected_model_repairs.append({
                    'repair_id': repair['id'],
                    'key': repair['id'],
                })
    model_repair_list = [model_repair['key'] for model_repair in selected_model_repairs if model_repair['repair_id'] in selected_repairs]
    device_model_repairs = DeviceModelRepair.objects.filter(id__in=model_repair_list)
    address_component = get_address_details(repair_order.address)
    for device_model_repair in device_model_repairs:
        repair_order_item = RepairOrderItem.objects.create(
            repair_order=repair_order,
            device_model_repair=device_model_repair,
            price=get_repair_price_for_region_name(device_model_repair, address_component['region'], True)
        )
        for repair_part in repair_order_item.device_model_repair.parts_quantity.all():
            if repair_part.part.colors.exists() and repair_order.device_model_color.device_color not in repair_part.part.colors.all():
                continue
            price = get_part_price_by_region_or_default(repair_part.part, repair_order.address.region)
            RepairOrderItemPart.objects.create(
                repair_order_item=repair_order_item,
                part=repair_part.part,
                price=price,
                quantity=repair_part.quantity
            )


MONTHS = (
    ('Январь', 'January'),
    ('Февраль', 'February'),
    ('Март', 'March'),
    ('Апрель', 'April'),
    ('Май', 'May'),
    ('Июнь', 'June'),
    ('Июль', 'July'),
    ('Август', 'August'),
    ('Сентябрь', 'September'),
    ('Октябрь', 'October'),
    ('Ноябрь', 'November'),
    ('Декабрь', 'December'),
)

def get_address_details(address):
    url = quote(smart_str(address))
    url = 'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false' % url
    response = urlopen(url).read()
    results = simplejson.loads(response)
    results = results['results']
    region = ""
    contex = {}
    for result in results:
        for component in result["address_components"]:
            if 'administrative_area_level_1' in component["types"]:
                region = component["long_name"]
    contex={'region':region}
    return contex


def get_date_from_string(date_string):
    if not date_string:
        return
    for (ru_month, en_month) in MONTHS:
        date_string = date_string.replace(ru_month, en_month)
    date = parser.parse(date_string)
    return make_aware(date)


def reject_repair_order(order, master, comment):
    rejected_order = RejectedOrder.objects.create(
        repair_order=order,
        master=master,
        comment=comment
    )
    orders_tasks.cancel_parts_orders_for_order.delay(order.id, order.master.id)
    order.master = None
    order.save()

    base_query = Transaction.objects.filter(
        object_id=order.id,
        type=Transaction.REPAIR_ORDER_COMMISSION,
        sender=master.user
    )
    last_transaction = base_query.filter(is_credit_money=False).order_by('-id').first()
    if last_transaction:
        last_transaction_credit = base_query.filter(is_credit_money=True, parent_id=last_transaction.id).order_by('-id').first()
        description = 'Возврат денег по заказу #{}, отказ мастера'.format(order.number)
        create_reverse_transaction(last_transaction, description)
    else:
        last_transaction_credit = base_query.filter(is_credit_money=True).order_by('-id').first()
    if last_transaction_credit:
        description = 'Возврат кридитных денег по заказу #{}, отказ мастера'.format(order.number)
        create_reverse_transaction(last_transaction_credit, description)

    tasks.master_reject_repair_order_to_admin.delay(rejected_order.id)


def get_repeated_order_coupon():
    queryset = Coupon.objects.filter(
        Q(expiration_date__isnull=True) | Q(expiration_date__gt=utc_now()),
        type=Coupon.REPEATED_ORDER_TYPE,
        expired=False,
    )
    queryset = queryset.annotate(orders_count=Count('orders')).filter(
        Q(use_count__isnull=True) | Q(orders_count__lt=F('use_count')))
    return queryset.first()


def get_master_orders(type, master):
    queryset = RepairOrder.objects.select_related(
        'master',
        'device_model_color',
        'device_model_color__device_model',
        'device_model_color__device_model__device',
        'address'
    ).prefetch_related(
        'repair_order_items',
        'repair_order_items__device_model_repair__repair',
    )
    if type == 'new':
        if not master.places.exists():
            return queryset.none()
        minutes = config.LAST_NEW_ORDER_NOTIFICATION_TIME
        queryset = queryset.filter(
            (Q(master__isnull=True) | Q(created_at__gt=(utc_now() - timedelta(hours=2)))) &
            (Q(created_at__lt=utc_now() - timedelta(minutes=minutes)) | Q(notified_masters__master=master))
        ).distinct()
        metro_stations = []
        city_names = []
        region_names = []
        master = Master.objects.prefetch_related('places', 'places__city', 'places__region', 'places__metro_stations').get(id=master.id)
        for place in master.places.all():
            if place.city:
                place_metro_station_ids = [m.get('id') for m in place.metro_stations.all().values('id')]
                metro_stations += place_metro_station_ids
                if not place_metro_station_ids:
                    city_names.append(place.city.dadata_name)
            else:
                region_names.append(place.region.dadata_name)
        queryset = queryset.filter(
            (Q(address__metro__isnull=False) & Q(address__metro__in=metro_stations)) | Q(address__city__in=city_names) | Q(address__region__in=region_names),
            device_model_color__device_model__in=master.certified_models.all()
        )
    else:
        queryset = queryset.filter(master=master).all()
    return queryset.exclude(status=RepairOrder.STATUS_CANCELLED)


def get_order_masters(repair_order):
    masters = Master.objects.filter(
        (Q(send_sms_repair_order_notification=True) | Q(send_email_repair_order_notification=True)),
        certified_models=repair_order.device_model_color.device_model
    ).order_by('-rating')
    query = (
        (Q(places__metro_stations__isnull=True) | Q(places__all_metro=True)) & Q(places__city__dadata_name=repair_order.address.city)
    ) | (
        Q(places__city__isnull=True) & Q(places__region__dadata_name=repair_order.address.region)
    )
    if repair_order.address.metro:
        query = Q(places__metro_stations=repair_order.address.metro) | query
    masters = masters.filter(query)
    return masters.distinct()


def notify_masters_about_new_order(repair_order):
    notification_masters = get_order_masters(repair_order)
    if not notification_masters:
        return
    # first
    for master in notification_masters[:config.FIRST_NEW_ORDER_NOTIFICATION_QUEUE]:
        orders_tasks.notify_master_about_new_repair_order.apply_async(countdown=5, args=(repair_order.id, master.id, True, ))
    # second
    step = config.FIRST_NEW_ORDER_NOTIFICATION_QUEUE + config.SECOND_NEW_ORDER_NOTIFICATION_QUEUE
    second_notification_masters_ids = [item.id for item in notification_masters[config.FIRST_NEW_ORDER_NOTIFICATION_QUEUE:step]]
    second_minutes = config.SECOND_NEW_ORDER_NOTIFICATION_TIME
    second_seconds = second_minutes * 60
    if len(second_notification_masters_ids):
        orders_tasks.notify_about_new_repair_order_if_not_selected.apply_async(countdown=second_seconds, args=(repair_order.id, second_notification_masters_ids, second_minutes, ))
    # last
    last_notification_masters_ids = [item.id for item in notification_masters[step:]]
    last_minutes = config.LAST_NEW_ORDER_NOTIFICATION_TIME
    last_seconds = last_minutes * 60
    if len(last_notification_masters_ids):
        orders_tasks.notify_about_new_repair_order_if_not_selected.apply_async(countdown=last_seconds, args=(repair_order.id, last_notification_masters_ids, last_minutes, ))

    # to admin
    orders_tasks.notify_admin_about_new_repair_order_if_not_selected.apply_async(countdown=second_seconds, args=(repair_order.id, second_minutes, ))
    orders_tasks.notify_admin_about_new_repair_order_if_not_selected.apply_async(countdown=last_seconds, args=(repair_order.id, last_minutes, ))


def create_reverse_transactions_for_void_repair_order(repair_order):
    transactions = Transaction.objects.filter(
        object_id=repair_order.id,
        sender__isnull=False,
    ).exclude(
        child__type=Transaction.REVERSE
    )
    for transaction in transactions:
        create_reverse_transaction(transaction)


def get_repair_order_comission(repair_order, master=None):
    if not master:
        master = repair_order.master
    if not master:
        return 0
    return get_repair_order_comission_by_status(repair_order, master.get_status())


def get_repair_order_comission_by_status(repair_order, status):
    commission_amount = 0
    comissions = DeviceModelRepairCommission.objects.filter(
        Q(region__dadata_name=repair_order.address.region) | Q(region=Region.get_default()),
        model_repair__in=repair_order.device_model_repair.all(),
        status=status
    )
    comissions_by_model_repair = {i.model_repair_id: i for i in comissions}
    for item in repair_order.repair_order_items.all():
        commission = comissions_by_model_repair.get(item.device_model_repair_id)
        if not commission:
            continue
        if commission.percentage:
            commission_amount += item.price * (Decimal(commission.commission) / 100)
        else:
            commission_amount += commission.commission
    return commission_amount


def send_partner_comission(repair_order):
    if not repair_order.partner:
        return
    Transaction.objects.create(
        recipient=repair_order.partner.user,
        type=Transaction.PARTNER_COMMISSION,
        amount=repair_order.partner.commission,
        status=Transaction.SUCCESS,
        content_object=repair_order,
        description='Оплата комиссии партнеру за заказ №{}'.format(repair_order.number),
    )


def get_repair_orders_by_phone(phone, email=None):
    phone = clean_phone(phone)
    orders = RepairOrder.objects.filter(user__phones__number=phone, user__phones__phone_type=Phone.OWN_PHONE)
    if email:
        orders = orders.filter(user__email=email)
    return orders


def process_order_done(repair_order):
    if repair_order.partner:
        send_partner_comission(repair_order)
    log.warning('post_save - scheduled create_and_send_order_review_invite task: %s', repair_order.id)
    if repair_order.is_warranty:
        tasks.pay_warranty_order_compensation.delay(repair_order.id)
    tasks.create_and_send_order_review_invite.delay(repair_order.id)
    tasks.cancel_parts_orders_for_order.apply_async(countdown=config.CANCEL_PARTS_ORDER_TIME * 60, args=(repair_order.id, repair_order.master_id))
    charge_off_parts = charge_off_master_parts_for_order(repair_order.master, repair_order)
    if repair_order.master.parts_control:
        log.warning('post_save - scheduled parts_control task for order_id: %s, after: %s mins', repair_order.id, config.PARTS_CONTROL_AFTER)
        tasks.parts_control.apply_async(countdown=config.PARTS_CONTROL_AFTER * 60, args=(repair_order.id, charge_off_parts))
