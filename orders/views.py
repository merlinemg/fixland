import json
import logging
import math

from datetime import timedelta
from constance import config
from django.contrib import messages
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from annoying.decorators import ajax_request
from django.http import Http404, HttpResponseForbidden, HttpResponse, HttpResponseBadRequest
from django.utils.translation import ugettext as _
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.core.signing import Signer, BadSignature

from masters.decorators import master_required
from masters.bl import create_master_review
from .models import RepairOrder, OrderReviewInvite, Coupon
from . import bl
from . import permissions
from core import utils, time_utils
from core.time_utils import utc_now, unix_utc_now, datetime_to_unix_time
from .forms import OrderReviewForm
from partners.utils import return_partner_if_link_is_ok, clear_partner_session_data

log = logging.getLogger(__name__)
signer = Signer()


@require_http_methods(["POST"])
@ajax_request
def create_repair_order(request):
    data = json.loads(request.body.decode('utf-8'))
    partner_form_code = return_partner_if_link_is_ok(request=request)
    if request.user and request.user.is_anonymous() or not request.user.is_partner() and not partner_form_code:
        if request.session.get('has_repeated_order_coupon_expired_at') and request.session.get('use_repeated_order_coupon_expired_at'):
            if request.session['has_repeated_order_coupon_expired_at'] > unix_utc_now() and request.session['use_repeated_order_coupon_expired_at'] > unix_utc_now():
                data['use_repeated_order_coupon'] = True
            else:
                del request.session['has_repeated_order_coupon_expired_at']
                del request.session['use_repeated_order_coupon_expired_at']
        elif Coupon.objects.filter(type=Coupon.REPEATED_ORDER_TYPE).exists():
            request.session['has_repeated_order_coupon_expired_at'] = datetime_to_unix_time(utc_now() + timedelta(minutes=config.REPEATED_ORDER_MINUTES))
    if request.user and not request.user.is_anonymous() and request.user.is_partner():
        data['partner'] = request.user.partner
    if partner_form_code:
        data['partner'] = partner_form_code
        clear_partner_session_data(request=request)
    repair_order = bl.create_order(data)
    comment_key = signer.sign(repair_order.number)
    context = {
        'success': True,
        'repeated_order_coupon': True if request.session.get('has_repeated_order_coupon_expired_at') else False,
        'comment_key': comment_key
    }
    return context


def repeate_repair(request):
    if 'has_repeated_order_coupon_expired_at' in request.session and request.session['has_repeated_order_coupon_expired_at'] > unix_utc_now() and not request.session.get('use_repeated_order_coupon_expired_at'):
        request.session['use_repeated_order_coupon_expired_at'] = datetime_to_unix_time(utc_now() + timedelta(minutes=config.REPEATED_ORDER_MINUTES))
    return redirect(reverse('repair'))


@require_http_methods(["POST"])
@ajax_request
def create_sell_order(request):
    data = json.loads(request.body.decode('utf-8'))
    bl.create_order(data, 'sell')
    return {'success': True}


@require_http_methods(["POST"])
@ajax_request
def create_warranty_order(request):
    data = json.loads(request.body.decode('utf-8'))
    bl.create_order(data, 'warranty')
    return {'success': True}


@login_required
@master_required
def select_repair_order(request, order_id):
    with transaction.atomic():
        order = RepairOrder.objects.select_for_update().filter(number=order_id).first()
        if not order or order.master:
            error = _('Заказ уже не действителен')
            messages.add_message(request, messages.INFO, error)
            return HttpResponseForbidden(
                json.dumps({'result': 'order_error', 'errors': [error]}),
                content_type="application/json")
        master = request.user.master
        commisson = order.get_master_commission(master)
        if commisson > master.balance_with_credit or not master.can_use_credit():
            error = _('Не достаточно денег на счету')
            messages.add_message(request, messages.INFO, error)
            amount = commisson if not master.used_credit_amount else commisson + master.used_credit_amount
            return HttpResponseForbidden(
                json.dumps({'result': 'no_money', 'amount': math.ceil(amount), 'errors': [error]}),
                content_type="application/json")
        utils.fee_repair_order_comission(order, master)
        order.master = master
        order.save()
        return HttpResponse(json.dumps({'result': 'ok', 'errors': []}), content_type="application/json")


@require_http_methods(["POST"])
@login_required
@master_required
def reject_repair_order(request, order_id):
    data = json.loads(request.body.decode('utf-8'))
    if not data.get('comment', None):
        return HttpResponseForbidden(_('Комментарий пустой'))
    order = get_object_or_404(RepairOrder, number=order_id)
    if not permissions.can_reject_order(order, request.user.master):
        return HttpResponseForbidden()
    bl.reject_repair_order(order, request.user.master, data.get('comment'))
    return HttpResponse()


def order_review(request, code):
    review_invite = OrderReviewInvite.objects.filter(code=code, expired=False).first()
    if not review_invite:
        return HttpResponseForbidden(_(u'Ваш код не действителен'))
    order_user = review_invite.repair_order.user
    data_for_review = {
        'invite_code': code,
        'bonus_type': config.ORDER_REVIEW_BONUS_TYPE,
        'tel_bonus_amount' : config.ORDER_REVIEW_TEL_BONUS_AMOUNT,
        'own_phone': order_user.get_phone_number() if order_user.has_own_phone() else '',
        'email': order_user.email
    }
    return render(request, 'orders/review.html', {'code': code, 'data_for_review': data_for_review})


@require_http_methods(["POST"])
@ajax_request
def create_order_review(request):
    data = json.loads(request.body.decode('utf-8'))
    form = OrderReviewForm(data)
    if form.is_valid():
        create_master_review(form.cleaned_data)
        messages.add_message(request, messages.INFO, _('Спасибо за отзыв'))
        return HttpResponse(json.dumps({'result': 'ok'}),
            content_type="application/json")
    else:
        return HttpResponseBadRequest(
            json.dumps({'result': False, 'errors': form.errors}),
            content_type="application/json")


@require_http_methods(["GET"])
@ajax_request
def check_coupon(request):
    repairs = map(int, request.GET.get('repairs', None).split(','))
    model_slug = request.GET.get('model_slug', None)
    code = request.GET.get('code', None)
    if not repairs or not model_slug or not code:
        return HttpResponseBadRequest(
            json.dumps({'result': False, 'errors': 'bad data'}),
            content_type="application/json")
    coupon = Coupon.objects.filter(code=code, type=Coupon.REGULAR_TYPE).first() if code else None
    if not coupon or not coupon.is_valid() or not permissions.can_use_coupon_by_order_data(coupon, repairs, model_slug):
        return HttpResponseBadRequest(
            json.dumps({'result': False, 'errors': 'bad code'}),
            content_type="application/json")
    data = {
        'discount': coupon.discount if coupon.discount else coupon.discount_percent,
        'percent': True if not coupon.discount else False,
        'description': coupon.description
    }
    return data


@require_http_methods(["GET"])
@ajax_request
def check_repeated_order_coupon(request):
    repeated_order_coupon = bl.get_repeated_order_coupon()
    if not request.session.get('use_repeated_order_coupon_expired_at') or request.session['use_repeated_order_coupon_expired_at'] < unix_utc_now() or not repeated_order_coupon or not repeated_order_coupon.is_valid():
        return HttpResponseBadRequest(
            json.dumps({'result': False, 'errors': 'bad code'}),
            content_type="application/json")
    data = {
        'discount': repeated_order_coupon.discount if repeated_order_coupon.discount else repeated_order_coupon.discount_percent,
        'percent': True if not repeated_order_coupon.discount else False,
        'description': repeated_order_coupon.description
    }
    return data


@require_http_methods(["POST"])
@ajax_request
def save_comment(request):
    data = json.loads(request.body.decode('utf-8'))
    if not data.get('comment') or not data.get('comment_key'):
        return HttpResponseForbidden()
    try:
        order_number = signer.unsign(data.get('comment_key'))
    except BadSignature:
        return HttpResponseForbidden()
    repair_order = get_object_or_404(RepairOrder, number=order_number)
    if not repair_order.comment:
        repair_order.comment = data.get('comment')
        repair_order.save()
    return {'result': True}
