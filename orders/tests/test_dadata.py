from django.test import TestCase

from accounts.factories import AddressFactory
from accounts.models import Address
from core.bl import parse_and_update_address


class TestParseAddressCase(TestCase):

    def test_parse_and_update_address(self):
        address = AddressFactory(raw='г Москва, ул Островитянова, д 4, кв 36')
        address = Address.objects.get(id=address.id)
        parse_and_update_address(address)
        address = Address.objects.get(id=address.id)
        self.assertEqual(address.region, 'Москва')
        self.assertEqual(address.city, 'Москва')
        self.assertEqual(address.zip_code, '117437')
        self.assertEqual(address.street, 'Островитянова')
        self.assertEqual(address.house, 'д 4')
        self.assertEqual(address.appartment, 'кв 36')
