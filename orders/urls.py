from django.conf.urls import patterns, url

urlpatterns = patterns(
    'orders.views',
    url(r'^repair/$', 'create_repair_order', name='create_repair_order'),
    url(r'^repair_warranty/$', 'create_warranty_order', name='create_warranty_order'),
    url(r'^sell/$', 'create_sell_order', name='create_sell_order'),
    url(r'^check-coupon/$', 'check_coupon', name='check_coupon'),
    url(r'^check-repeated-order-coupon/$', 'check_repeated_order_coupon', name='check_repeated_order_coupon'),
    url(r'^save-comment/$', 'save_comment', name='save_comment'),
)
