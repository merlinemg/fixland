from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.decorators import list_route, api_view
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.utils.timezone import now as utc_now

from .serializers import RepairOrderSerializer, CompleteRepairOrderSerializer, RepairOrderUserSerializer, RepairOrderForUserSerializer
from .permissions import RepairOrderPermission, CompleteRepairOrderPermissions
from ..models import RepairOrder
from ..bl import get_master_orders, get_repair_orders_by_phone
from ..tasks import email_about_not_finished_order_to_admin


class RepairOrdersView(ReadOnlyModelViewSet):
    serializer_class = RepairOrderSerializer
    queryset = RepairOrder.objects
    permission_classes = (RepairOrderPermission, )
    filter_fields = ('status', )
    ordering_fields = ('starttime',)
    lookup_field = 'number'

    def get_queryset(self, *args, **kwargs):
        type = self.request.query_params.get('type', None)
        queryset = get_master_orders(type, self.request.user.master)
        return queryset

    @list_route(methods=['get'], permission_classes=[AllowAny], url_path='find-orders')
    def find_orders(self, request):
        phone = request.GET.get('phone')
        email = request.GET.get('email')
        if not phone:
            orders = RepairOrder.objects.none()
        else:
            orders = get_repair_orders_by_phone(phone, email).filter(is_warranty=False, status=RepairOrder.STATUS_DONE)
        data = RepairOrderForUserSerializer(orders, many=True).data
        return Response(data)


class CompleteRepairOrderView(mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = RepairOrder.objects.all()
    serializer_class = CompleteRepairOrderSerializer
    permission_classes = (CompleteRepairOrderPermissions,)
    lookup_field = 'number'

    def perform_update(self, serializer):
        return serializer.save(status=RepairOrder.STATUS_DONE, completed_at=utc_now())


class RepairOrderUserView(ReadOnlyModelViewSet):
    serializer_class = RepairOrderUserSerializer
    queryset = RepairOrder.objects
    permission_classes = (RepairOrderPermission, )
    lookup_field = 'number'

    def get_queryset(self, *args, **kwargs):
        type = self.request.query_params.get('type', None)
        queryset = get_master_orders(type, self.request.user.master)
        return queryset


@api_view(['POST'])
def not_finished_order(request):
    if request.data:
        email_about_not_finished_order_to_admin.delay(request.data)
    return Response({'success': True})
