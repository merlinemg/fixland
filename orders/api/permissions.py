from rest_framework import permissions


class RepairOrderPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_master()

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and request.user.is_master()


class CompleteRepairOrderPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_master()

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and request.user.is_master() and obj.master == request.user.master
