from constance import config
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from .models import OrderReviewInvite, Coupon


class OrderReviewForm(forms.Form):
    invite_code = forms.CharField(max_length=128)
    comment = forms.CharField(max_length=256, required=False)
    score = forms.IntegerField(min_value=1, max_value=5)
    as_in_order = forms.BooleanField(required=False)
    phone = forms.CharField(required=False)
    email = forms.CharField(required=False)

    def clean(self):
        if not self.cleaned_data.get('as_in_order'):
            if config.ORDER_REVIEW_BONUS_TYPE == settings.ORDER_REVIEW_BONUS_TEL_KEY and not self.cleaned_data.get('phone'):
                raise forms.ValidationError(_('Требуется телефон'))
            if config.ORDER_REVIEW_BONUS_TYPE == settings.ORDER_REVIEW_BONUS_COUPON_KEY and not self.cleaned_data.get('email'):
                raise forms.ValidationError(_('Требуется email'))

    def clean_invite_code(self):
        review_invite = OrderReviewInvite.objects.filter(code=self.cleaned_data.get('invite_code'), expired=False).first()
        if not review_invite:
            raise forms.ValidationError(_('Ваш код не действителен'))
        else:
            self.cleaned_data['review_invite'] = review_invite


class CouponAdminForm(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = ('code', 'discount', 'discount_percent', 'use_count', 'expiration_date', 'type', 'auto_appliable', 'device_models', 'repairs', 'expired',)

    def clean(self):
        if self.cleaned_data.get('discount') and self.cleaned_data.get('discount_percent'):
            raise forms.ValidationError(_('Введите или фиксированую скидку или скидку в процентах'))
        return self.cleaned_data
