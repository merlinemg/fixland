# coding: utf-8
import logging

from constance import config
from django.conf import settings
from django.core.mail import mail_admins, send_mail
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as __
from django.utils.timezone import make_naive

log = logging.getLogger(__name__)


def new_sell_order_to_admin(order):
    subject = _('Поступил новый заказ на покупку')
    if order.as_soon_as_possible:
        good_time = _('Как можно скорее')
    else:
        good_time = __('{} От: {} До: {}').format(
            make_naive(order.starttime).strftime('%-d %B %Y'),
            make_naive(order.starttime).strftime('%-H:00'),
            make_naive(order.endtime).strftime('%-H:00'),
        )
    context = {
        'admin_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_sellorder_change', args=(order.id, ))
        ),
        'order': order,
        'good_time': good_time,
        'map_link': 'http://maps.google.com/?q={}'.format(order.address),
    }
    message = render_to_string('emails/orders/new_sell_order_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_repair_order_to_admin(order):
    subject = _('Поступил новый заказ на ремонт')
    if order.as_soon_as_possible:
        good_time = _('Как можно скорее')
    else:
        good_time = __('{} От: {} До: {}').format(
            make_naive(order.starttime).strftime('%-d %B %Y'),
            make_naive(order.starttime).strftime('%-H:00'),
            make_naive(order.endtime).strftime('%-H:00'),
        )
    repairs_list = ', '.join([i.title for i in order.repair_order_items.all()])
    context = {
        'good_time': good_time,
        'repairs_list': repairs_list,
        'admin_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_repairorder_change', args=(order.id, ))
        ),
        'coupon_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_coupon_change', args=(order.coupon_id, ))
        ) if order.coupon else None,
        'order': order,
        'map_link': 'http://maps.google.com/?q={}'.format(order.address),
    }
    message = render_to_string('emails/orders/new_repair_order_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_warranty_order_to_admin(order):
    subject = _('Поступил гарантийный заказ')
    if order.as_soon_as_possible:
        good_time = _('Как можно скорее')
    else:
        good_time = __('{} От: {} До: {}').format(
            make_naive(order.starttime).strftime('%-d %B %Y'),
            make_naive(order.starttime).strftime('%-H:00'),
            make_naive(order.endtime).strftime('%-H:00'),
        )
    repairs_list = ', '.join([i.title for i in order.repair_order_items.all()])
    context = {
        'good_time': good_time,
        'repairs_list': repairs_list,
        'admin_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_repairorder_change', args=(order.id, ))
        ),
        'order': order,
        'map_link': 'http://maps.google.com/?q={}'.format(order.address),
    }
    message = render_to_string('emails/orders/new_warranty_order_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def discount_more_then_commision_to_admin(order):
    subject = _('Скидка больше комиссии')
    context = {
        'order': order,
        'admin_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_repairorder_change', args=(order.id, ))
        ),
        'coupon_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_coupon_change', args=(order.coupon_id, ))
        ),
    }
    message = render_to_string('emails/orders/discount_more_then_commision_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_repair_order_to_admin_if_not_selected(order, after_minutes):
    subject = _('Заказ №{order_id} не забрали').format(order_id=order.number)
    context = {
        'admin_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_repairorder_change', args=(order.id, ))
        ),
        'order': order,
        'after_minutes': after_minutes
    }
    message = render_to_string('emails/orders/new_repair_order_to_admin_if_not_selected.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_repair_order_to_master(email, order):
    subject = _('Fixland.ru - Поступил новый заказ на ремонт')
    if order.as_soon_as_possible:
        good_time = _('Как можно скорее')
    else:
        good_time = __('{} От: {} До: {}').format(
            make_naive(order.starttime).strftime('%-d %B %Y'),
            make_naive(order.starttime).strftime('%-H:00'),
            make_naive(order.endtime).strftime('%-H:00'),
        )
    repairs_list = ', '.join(map(str, order.device_model_repair.all()))
    context = {
        'good_time': good_time,
        'repairs_list': repairs_list,
        'order': order,
    }
    message = render_to_string('emails/orders/new_repair_order_to_master.html', context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)


def master_reject_repair_order_to_admin(rejected_order):
    subject = _('Мастер отказался от заказа')
    link = 'http://{}{}'.format(settings.HOST_URL, reverse('admin:orders_repairorder_change', args=(rejected_order.repair_order.id,)))
    context = {
        'link': link,
        'order': rejected_order.repair_order,
        'master': rejected_order.master,
        'comment': rejected_order.comment
    }
    message = render_to_string('emails/orders/master_reject_repair_order_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def repair_order_review_invite(email, order, invite_link):
    subject = _('Fixland.ru - Оставьте отзыв')
    context = {
        'order': order,
        'discount': config.COUPON_DISCOUNT,
        'review_link': 'http://{}{}'.format(
            settings.HOST_URL,
            invite_link,
        )
    }
    message = render_to_string('emails/orders/repair_order_review_invite.html', context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)


def send_coupon(user, email, coupon, order):
    subject = _('Fixland.ru - Купон')
    context = {
        'user': user,
        'coupon': coupon,
        'order': order
    }
    message = render_to_string('emails/orders/coupon.html', context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)


def master_not_taken_parts_to_admin(master, order, parts_orders):
    subject = _('Мастер {} не забрал запчасть').format(master.user.get_full_name())
    link = 'http://{}{}'.format(settings.HOST_URL, reverse('admin:masters_master_change', args=(master.id,)))
    context = {
        'link': link,
        'master': master,
        'order': order,
        'parts_orders': parts_orders
    }
    message = render_to_string('emails/masters/master_not_taken_parts_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_master_review_admin(review):
    subject = _('Поступил новый отзыв о мастере')
    context = {
        'master_full_name': review.master.user.get_full_name(),
        'admin_master_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:masters_master_change', args=(review.master.id, ))
        ),
        'mark': review.get_score_display(),
        'order_number': review.repair_order.__str__(),
        'admin_order_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('admin:orders_repairorder_change', args=(review.repair_order.id, ))
        ),
    }
    message = render_to_string('emails/masters/new_master_review.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def email_about_not_finished_order_to_admin(order_data):
    subject = _('Заказ оформлен клиентом не до конца')
    context = {
        'order_data': order_data,
        'repairs': ', '.join(map(lambda x: x.get('name'), order_data.get('selectedRepairs')))
    }
    message = render_to_string('emails/orders/not_finished_order.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass
