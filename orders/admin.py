# coding: utf-8
from django.contrib import admin
from grappelli_nested.admin import NestedModelAdmin, NestedTabularInline
from django.utils.translation import ugettext_lazy as _

from .models import RepairOrder, RepairOrderItem, SellOrder, RejectedOrder, OrderReviewInvite, Coupon, RepairOrderNotifiedMaster, RepairOrderItemPart
from .forms import CouponAdminForm


class RepairOrderItemPartInline(NestedTabularInline):
    model = RepairOrderItemPart
    extra = 0


class RepairOrderItemInline(NestedTabularInline):
    model = RepairOrderItem
    inlines = (RepairOrderItemPartInline, )
    extra = 0

    def get_formset(self, request, obj=None, **kwargs):
        form = super(RepairOrderItemInline, self).get_formset(request, obj, **kwargs)
        form.form.base_fields['device_model_repair'].queryset = form.form.base_fields['device_model_repair'].queryset.select_related('device_model', 'repair')
        return form


class RepairOrderNotifiedMasterInline(NestedTabularInline):
    model = RepairOrderNotifiedMaster
    extra = 0

    def get_formset(self, request, obj=None, **kwargs):
        form = super(RepairOrderNotifiedMasterInline, self).get_formset(request, obj, **kwargs)
        form.form.base_fields['master'].queryset = form.form.base_fields['master'].queryset.select_related('user')
        return form


class RepairOrderAdmin(NestedModelAdmin):
    list_display = ('number', 'user', 'master', 'status', 'address', 'device_model_color', 'starttime', 'endtime', 'as_soon_as_possible', 'created_at')
    inlines = (RepairOrderItemInline, RepairOrderNotifiedMasterInline, )
    list_display_links = ('number', 'user')
    list_filter = ('status', )
    change_form_template = 'admin_repairorder_change_form.html'
    date_hierarchy = 'created_at'
    search_fields = ('number', 'user__first_name', 'user__last_name', 'master__user__first_name', 'master__user__last_name', 'user__email')
    readonly_fields = ('device_model_color', 'user', 'created_at')
    ordering = ('-id', )

    class Media:
        js = ('js/confirm_save_order.js', )

    def get_form(self, request, obj=None, **kwargs):
        form = super(RepairOrderAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['master'].queryset = form.base_fields['master'].queryset.select_related('user')
        return form

    def get_queryset(self, request):
        qs = super(RepairOrderAdmin, self).get_queryset(request)
        qs = qs.select_related('master', 'master__user', 'device_model_color', 'device_model_color__device_model', 'device_model_color__device_model__device', 'device_model_color__device_color', 'user', 'address')
        return qs

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.status == RepairOrder.STATUS_VOID:
            return self.readonly_fields + ('status',)
        if obj.is_warranty:
            return self.readonly_fields + ('_parent_repairs',)
        return self.readonly_fields

    def _parent_repairs(self, obj):
        if not obj.parent:
            return ''
        return ', '.join(obj.parent.repair_order_items.values_list('device_model_repair__repair__name', flat=True))
    _parent_repairs.short_description = _('Ремонты старого заказа')


class SellOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'address', 'device_model_sell', 'price', 'starttime', 'endtime', 'as_soon_as_possible', 'created_at')
    list_display_links = ('id', 'user')
    change_form_template = 'admin_repairorder_change_form.html'
    ordering = ('-id', )


class RejectedOrderAdmin(admin.ModelAdmin):
    list_display = ('repair_order', 'master', 'comment', 'created_at')


class OrderReviewInviteAdmin(admin.ModelAdmin):
    list_display = ('repair_order', 'created_at')


class RepairOrderInline(admin.TabularInline):
    model = RepairOrder
    fields = ('number', 'user', 'master', 'status')
    extra = 0

    def has_add_permission(self, request):
        return False


class CouponAdmin(admin.ModelAdmin):
    list_display = ('code', 'discount', 'discount_percent', '_used_count', '_binded', 'created_at', 'expired')
    readonly_fields = ('_used_count', '_binded')
    form = CouponAdminForm
    inlines = (RepairOrderInline, )

    def _used_count(self, obj):
        if not obj.use_count:
            return obj.used_count
        else:
            return '{}/{}'.format(obj.used_count, obj.use_count)
    _used_count.short_description = _('Количество использованний купона')

    def _binded(self, obj):
        if obj.repairs.exists() or obj.device_models.exists():
            return '∞'
        return ''
    _binded.short_description = ''


admin.site.register(RepairOrder, RepairOrderAdmin)
admin.site.register(SellOrder, SellOrderAdmin)
admin.site.register(RejectedOrder, RejectedOrderAdmin)
admin.site.register(OrderReviewInvite, OrderReviewInviteAdmin)
admin.site.register(Coupon, CouponAdmin)
