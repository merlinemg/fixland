import json
import uuid
from base64 import b64decode
from django.contrib import messages
from django.contrib.auth import authenticate, login as django_login
from annoying.decorators import ajax_request
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.core.files.base import ContentFile
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from sitegate.models import EmailConfirmation
from sitegate.settings import SIGNUP_VERIFY_EMAIL_ERROR_TEXT, SIGNUP_VERIFY_EMAIL_SUCCESS_TEXT
from django.views.decorators.http import require_POST
from django.utils.translation import ugettext as _

from .models import User, SmsConfirmation, Phone
from .utils import create_and_send_code
from . import tasks
from .forms import MasterRegistrationForm, LoginForm
from .bl import create_master, create_partner


def master_signup(request):
    if request.user.is_authenticated():
        messages.add_message(request, messages.INFO, _('Вы уже авторизованы'))
        return redirect(reverse('index'))
    return render(request, 'master_signup.html')


def partner_signup(request):
    if request.user.is_authenticated():
        messages.add_message(request, messages.INFO, _('Вы уже авторизованы'))
        return redirect(reverse('index'))
    return render(request, 'master_signup.html', {'is_partner': True})


def login_page(request):
    if request.user.is_authenticated():
        messages.add_message(request, messages.INFO, _('Вы уже авторизованы'))
        return redirect(reverse('index'))
    return render(request, 'login.html')


@ajax_request
def login(request):
    data = json.loads(request.body.decode('utf-8'))
    form = LoginForm(data)
    if not form.is_valid():
        return HttpResponseBadRequest(
            json.dumps(form.errors),
            content_type="application/json")

    phone = form.cleaned_data['phone']
    phone_user = Phone.objects.filter(number=phone, phone_type=Phone.OWN_PHONE).first().user
    password = form.cleaned_data['password']
    user = authenticate(username=phone_user.username, password=password)
    if user is not None:
        if user.is_active:
            django_login(request, user)
            return HttpResponse(
                json.dumps({'result': True}),
                content_type="application/json")
        else:
            return HttpResponseForbidden(
                json.dumps([(_('Пользователь не активирован'))]),
                content_type="application/json")
    else:
        return HttpResponseBadRequest(
            json.dumps([_('Неверный телефон или пароль')]),
            content_type="application/json")


@ajax_request
def register_master(request):
    data = json.loads(request.body.decode('utf-8'))
    form = MasterRegistrationForm(data)
    if not form.is_valid():
        return HttpResponseBadRequest(
            json.dumps({'result': True, 'errors': form.errors}),
            content_type="application/json")
    else:
        user = create_master(form)
        return HttpResponse(
            json.dumps({'result': True, 'errors': False, 'user_id': user.id}),
            content_type="application/json")
    return {'success': True}


@ajax_request
def register_partner(request):
    data = json.loads(request.body.decode('utf-8'))
    form = MasterRegistrationForm(data)
    if not form.is_valid():
        return HttpResponseBadRequest(
            json.dumps({'result': True, 'errors': form.errors}),
            content_type="application/json")
    else:
        user = create_partner(form)
        return HttpResponse(
            json.dumps({'result': True, 'errors': False, 'user_id': user.id}),
            content_type="application/json")
    return {'success': True}


@ajax_request
def verify_sms_code(request, code):
    valid_code = SmsConfirmation.get_valid_code(code)
    if valid_code:
        valid_code.accept()
    return {'result': bool(valid_code)}


@ajax_request
def send_new_sms_code(request, user_id):
    user = User.objects.filter(id=user_id).first()
    if not user or user.phone_confirmed:
        return {'result': False}
    try:
        create_and_send_code(user)
    except PermissionDenied:
        return {'result': False}
    return {'result': True}


@ajax_request
def send_new_confirm_email(request):
    if not request.user.email_confirmed:
        tasks.send_activation_email_task.delay(request.user.id)
        return {'result': True}
    else:
        return {'result': False}


def verify_email(request, code, redirect_to=None):
    success = False

    valid_code = EmailConfirmation.is_valid(code)
    if valid_code:
        valid_code.activate()
        user = valid_code.user
        user.email_confirmed = True
        user.save()
        success = True

    if success:
        messages.success(request, SIGNUP_VERIFY_EMAIL_SUCCESS_TEXT, 'success')
    else:
        messages.error(request, SIGNUP_VERIFY_EMAIL_ERROR_TEXT, 'danger error')

    if redirect_to is None:
        redirect_to = '/'

    return redirect(redirect_to)


@require_POST
def change_avatar(request):
    user = get_object_or_404(User, id=request.user.id)
    base64_string = request.body
    filename = "{}.png".format(uuid.uuid4())
    decoded_image = b64decode(base64_string[22:])
    user.avatar = ContentFile(decoded_image, filename)
    user.save()
    return HttpResponse(
        json.dumps({'success': True}),
        content_type="application/json")


def reset_password(request):
    if request.user.is_authenticated():
        messages.add_message(request, messages.INFO, _('Вы уже авторизованы'))
        return redirect(reverse('index'))
    return render(request, 'accounts/reset_password.html')
