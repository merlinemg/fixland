# coding: utf-8
from django import forms
from django.core.validators import EmailValidator
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy

from sitegate.signup_flows import classic
from sitegate.models import BlacklistedDomain
from accounts.models import User, Phone, FeedBack
from core import utils
from .utils import send_new_password_sms

EmailValidator.message = ugettext_lazy('Не верный формат email.')


class MasterRegistrationForm(classic.ClassicWithEmailSignupForm):
    username = None
    password2 = None
    first_name = forms.CharField()
    phone = forms.CharField()
    city = forms.CharField()
    comment = forms.CharField(required=False)
    offer = forms.BooleanField()
    gender = forms.CharField(required=False)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'gender')

    def clean_email(self):
        email = self.cleaned_data['email']
        if BlacklistedDomain.is_blacklisted(email):
            raise forms.ValidationError(_('Регистрация с этим доменом почты не разрешена'))
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Пользователь с таким email присутствует в системе!")
        return email

    def clean_phone(self):
        phone = utils.clean_phone(self.cleaned_data['phone'])
        if Phone.objects.filter(number=phone, phone_type=Phone.OWN_PHONE, user__master__isnull=False).exists():
            raise forms.ValidationError(
                _('Мастер с таким телефоном присутствует в системе!'))
        return phone


class LoginForm(forms.Form):
    phone = forms.CharField()
    password = forms.CharField()

    def clean_phone(self):
        phone = utils.clean_phone(self.cleaned_data['phone'])
        if not Phone.objects.filter(number=phone, phone_type=Phone.OWN_PHONE).exists():
            raise forms.ValidationError(
                _('Пользователь с таким телефоном отсутствует в системе!'))
        return phone


class ResetPasswordForm(forms.Form):
    phone = forms.CharField(max_length=255)

    def save(self):
        UserModel = get_user_model()  # noqa
        phone_val = utils.clean_phone(self.cleaned_data["phone"])
        user_phone = Phone.objects.filter(number=phone_val, phone_type=Phone.OWN_PHONE, user__is_active=True,
                                          user__phone_confirmed=True).first()
        if user_phone is not None:
            user = user_phone.user
            if not user.has_usable_password():
                return
            new_password = User.objects.make_random_password()
            user.set_password(new_password)
            user.save()
            contaxt = {
                'phone': user.get_phone_number(),
                'user': user,
                'password': new_password
            }
            send_new_password_sms(user.get_phone_number(), contaxt)


class FeedbackAdminForm(forms.ModelForm):
    class Meta:
        model = FeedBack
        fields = ('name', 'contact', 'feedback_text', 'feedback_answer')

    def clean(self):
        feedback_answer = self.cleaned_data.get('feedback_answer')
        if feedback_answer and '@' not in self.cleaned_data.get('contact') and len(feedback_answer) > 1000:
            raise forms.ValidationError(_('Длина SMS должна быть короче чем 1000 символов'))
        return self.cleaned_data