# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0016_auto_20150703_1117'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='sex',
        ),
        migrations.AddField(
            model_name='user',
            name='gender',
            field=models.CharField(verbose_name='Пол', max_length=255, default='male', choices=[('male', 'Мужчина'), ('female', 'Женщина')]),
        )
    ]
