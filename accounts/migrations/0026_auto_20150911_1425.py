# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0025_auto_20150826_1017'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='lat',
            field=models.FloatField(null=True, blank=True, verbose_name='Долгота'),
        ),
        migrations.AddField(
            model_name='address',
            name='lon',
            field=models.FloatField(null=True, blank=True, verbose_name='Широта'),
        ),
    ]
