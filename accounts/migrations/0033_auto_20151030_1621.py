# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, connection


def remove_type_from_address_city(apps, schema_editor):
    cursor = connection.cursor()
    cursor.execute("UPDATE accounts_address SET city = replace(city, 'г ', '')")


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0032_auto_20151020_1844'),
    ]

    operations = [
        migrations.RunPython(remove_type_from_address_city)
    ]
