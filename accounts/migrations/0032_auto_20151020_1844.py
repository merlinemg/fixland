# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import connection, migrations


def remove_type_from_address_region(apps, schema_editor):
    cursor = connection.cursor()
    cursor.execute("UPDATE accounts_address SET region = replace(region, 'г ', '')")
    cursor.execute("UPDATE accounts_address SET region = replace(region, ' обл', '')")


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0031_region_geoip_name'),
    ]

    operations = [
        migrations.RunPython(remove_type_from_address_region)
    ]
