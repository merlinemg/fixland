# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0030_auto_20150922_1805'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='geoip_name',
            field=models.CharField(null=True, verbose_name='Название GeoIp', max_length=255, blank=True),
        ),
    ]
