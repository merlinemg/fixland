# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0018_auto_20150817_1034'),
        ('accounts', '0018_merge'),
    ]

    operations = [
    ]
