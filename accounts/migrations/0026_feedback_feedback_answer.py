# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0025_auto_20150826_1017'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='feedback_answer',
            field=models.TextField(blank=True, null=True, verbose_name='Ответ'),
        ),
    ]
