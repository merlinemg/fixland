# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def remove_duplicate_metro(apps, schema_editor):
    MetroStation = apps.get_model("accounts", "MetroStation")
    Address = apps.get_model("accounts", "Address")
    metro_stations = MetroStation.objects.all()
    to_remove = {}
    for metro_station in metro_stations.order_by('-id'):
        if not to_remove.get(metro_station.name.lower()) and metro_stations.filter(name__iexact=metro_station.name, line=metro_station.line).count() > 1:
            to_remove[metro_station.name.lower()] = metro_station.id
            metro = metro_stations.filter(name__iexact=metro_station.name, line=metro_station.line).exclude(id=metro_station.id).get()
            Address.objects.filter(metro=metro_station).update(metro_id=metro)
    if to_remove:
        MetroStation.objects.filter(id__in=to_remove.values()).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0029_merge'),
    ]

    operations = [
        migrations.RunPython(remove_duplicate_metro)
    ]
