# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from django.db import models, migrations


def update_username(apps, schema_editor):
    user = apps.get_model('accounts', 'User')
    for user in user.objects.all():
        if user.email:
            username = user.email
        elif user.phones.first() and user.phones.first().number and user.phones.first().phone_type == 'OWN':
            username = user.phones.first().number
        else:
            username = uuid.uuid4()
        user.username = username
        user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_user_username'),
    ]

    operations = [
        migrations.RunPython(update_username),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(null=True, max_length=254, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
