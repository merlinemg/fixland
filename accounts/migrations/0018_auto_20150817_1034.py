# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0017_auto_20150810_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.CharField(verbose_name='Пол', choices=[('male', 'Мужчина'), ('female', 'Женщина')], null=True, max_length=255, blank=True, default='male'),
        ),
    ]
