# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_dadata_city_names(apps, schema_editor):
    City = apps.get_model("accounts", "City")
    for city in City.objects.all():
        city.dadata_name = city.name
        city.save()


def set_dadata_region_names(apps, schema_editor):
    Region = apps.get_model("accounts", "Region")
    for region in Region.objects.all():
        region.dadata_name = region.name
        region.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0027_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='dadata_name',
            field=models.CharField(default='', max_length=255, verbose_name='Название dadata'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='region',
            name='dadata_name',
            field=models.CharField(default='', max_length=255, verbose_name='Название dadata'),
            preserve_default=False,
        ),
        migrations.RunPython(set_dadata_city_names),
        migrations.RunPython(set_dadata_region_names),
    ]
