# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_auto_20150601_1318'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedBack',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(help_text='Имя пользователя, отправившего отзыв', max_length=255, verbose_name='Имя')),
                ('contact', models.CharField(help_text='Контактная информация пользователя, отправившего отзыв', max_length=255, verbose_name='Контактная информация')),
                ('feedback_text', models.TextField(verbose_name='Текст отзыва')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Отзыв создан')),
            ],
            options={
                'verbose_name_plural': 'Отзывы',
                'verbose_name': 'Отзыв',
            },
        ),
    ]
