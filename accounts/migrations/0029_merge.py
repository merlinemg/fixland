# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0028_remove_metrostation_city'),
        ('accounts', '0028_auto_20150918_1321'),
    ]

    operations = [
    ]
