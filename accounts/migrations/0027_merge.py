# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0026_feedback_feedback_answer'),
        ('accounts', '0026_auto_20150911_1425'),
    ]

    operations = [
    ]
