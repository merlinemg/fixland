# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_address_phone'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name_plural': 'Адресса', 'verbose_name': 'Адресс'},
        ),
        migrations.AlterModelOptions(
            name='phone',
            options={'verbose_name_plural': 'Номера телефонов', 'verbose_name': 'Номер телефона'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name_plural': 'Пользователи', 'verbose_name': 'Пользователь'},
        ),
        migrations.AddField(
            model_name='user',
            name='middle_name',
            field=models.CharField(max_length=255, blank=True, verbose_name='Отчество'),
        ),
        migrations.AlterField(
            model_name='address',
            name='appartment',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Квартира'),
        ),
        migrations.AlterField(
            model_name='address',
            name='city',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Город'),
        ),
        migrations.AlterField(
            model_name='address',
            name='country',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Страна'),
        ),
        migrations.AlterField(
            model_name='address',
            name='house',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Дом'),
        ),
        migrations.AlterField(
            model_name='address',
            name='raw',
            field=models.CharField(verbose_name='В сыром виде', max_length=255),
        ),
        migrations.AlterField(
            model_name='address',
            name='region',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Регион'),
        ),
        migrations.AlterField(
            model_name='address',
            name='street',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Улица'),
        ),
        migrations.AlterField(
            model_name='address',
            name='zip_code',
            field=models.CharField(null=True, max_length=255, blank=True, verbose_name='Индекс'),
        ),
        migrations.AlterField(
            model_name='phone',
            name='number',
            field=models.CharField(verbose_name='Номер телефона', max_length=255),
        ),
        migrations.AlterField(
            model_name='phone',
            name='phone_type',
            field=models.CharField(default='OWN', choices=[('OWN', 'Мой'), ('FRIEND', 'Друга')], verbose_name='Тип', max_length=255),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(verbose_name='Имя', max_length=255),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(verbose_name='Фамилия', max_length=255),
        ),
    ]
