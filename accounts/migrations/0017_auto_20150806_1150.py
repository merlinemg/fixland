# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0016_auto_20150703_1117'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(verbose_name='Название', max_length=255)),
                ('is_metro_related', models.BooleanField(default=False, verbose_name='Есть метро?')),
            ],
            options={
                'verbose_name_plural': 'Города',
                'verbose_name': 'Город',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(verbose_name='Название', max_length=255)),
                ('is_central_region', models.BooleanField(default=False, verbose_name='Центральный регион?')),
            ],
            options={
                'verbose_name_plural': 'Регионы',
                'verbose_name': 'Регион',
            },
        ),
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(to='accounts.Region', verbose_name='Регион', related_name='zone_cities'),
        ),
        migrations.AddField(
            model_name='metrostation',
            name='city',
            field=models.ForeignKey(null=True, to='accounts.City'),
        ),
    ]
