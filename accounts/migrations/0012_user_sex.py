# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_auto_20150608_1200'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='sex',
            field=models.CharField(default='male', choices=[('male', 'Мужчина'), ('famale', 'Женщина')], verbose_name='Пол', max_length=255),
        ),
    ]
