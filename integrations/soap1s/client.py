from suds.client import Client as SoapClient
import logging
import xmltodict

from django.template.loader import render_to_string
from ..models import SoapLog

log = logging.getLogger(__name__)


class Client:
    '''
    Client for making requests to Master Mobile vendor
    '''
    client = None

    def __init__(self, url, username, password, api_key):
        self.url = url
        self.api_key = api_key
        self.username = username
        self.password = password

    def get_sheet(self, article_list):
        '''
        GetSheet - get information about availabilities and
        storages for articles provided
        '''
        context = self.get_base_xml_context()
        context.update({
            'article_list': article_list
        })
        document = render_to_string('soap1s/get_sheet.xml', context)

        result = self.request(document)
        data = xmltodict.parse(result)
        success = data['ФайлОбмена']['Заголовок']['@Операция'] == 'TakeSheet'
        result_parts = []
        resp_parts = data['ФайлОбмена']['Товары']['Товар']
        if not isinstance(resp_parts, list):
            resp_parts = [resp_parts]
        for part in resp_parts:
            result_parts.append({
                'article': part['@Артикул'],
                'warehouse': part['@КодСклада'],
                'cost': part['@Цена'],
                'exists': part['@Наличие'] == 'ДА',
            })
        return {
            'success': success,
            'parts': result_parts,
        }

    def confirm_order(self, order_list):
        '''
        ConfirmOrder - try to confirm parts order
        order_list is a list with dicts in such format:
        [
            {
                'warehouse': 'WAREHOUSECODE',
                'article': 'ARTICLE',
                'qty': 1
            }
        ]
        '''
        context = self.get_base_xml_context()
        context.update({
            'order_list': order_list
        })
        document = render_to_string('soap1s/confirm_order.xml', context)
        soap_log = SoapLog.create_request_log(document, SoapLog.TYPE_CONFIRM_ORDER)
        log.warning('Created request to confirm order', extra={'document': document, 'context': context})
        result = self.request(document)
        soap_log.response = result
        soap_log.save()
        log.warning('Confirm order response', extra={'result': result})
        data = xmltodict.parse(result)
        success = data['ФайлОбмена']['Заголовок']['@Операция'] == 'GoodOrders' or 'Заказы' in data['ФайлОбмена']
        result_parts = []
        orders = []
        if success:
            resp_orders = data['ФайлОбмена']['Заказы']['Заказ']
            if not isinstance(resp_orders, list):
                resp_orders = [resp_orders]
            for part in resp_orders:
                orders.append({
                    'article': part['@Артикул'],
                    'qty': part['@КЗаказу'],
                    'order_number': part['@НомерЗаказа'],
                    'price': part['@Цена'],
                    'warehouse': part['@КодСклада'],
                })
        if 'Товары' in data['ФайлОбмена']:
            resp_parts = data['ФайлОбмена']['Товары']['Товар']
            if not isinstance(resp_parts, list):
                resp_parts = [resp_parts]
            for part in resp_parts:
                qty = int(part['@КЗаказу'] if part['@КЗаказу'] != 'НЕТ' else 0)
                result_parts.append({
                    'article': part['@Артикул'],
                    'qty': qty,
                    'warehouse': part['@КодСклада'] if part['@КодСклада'] != 'НЕТ' else 0,
                    'cost': part['@Цена'] if part['@Цена'] != 'НЕТ' else '0',
                    'exists': qty > 0,
                })

        return {
            'success': success,
            'parts': result_parts,
            'orders': orders,
        }

    def cancel_order(self, order_numbers):
        '''
        CancelOrder - cancel order with provided order_numbers
        '''
        context = self.get_base_xml_context()
        context.update({
            'order_numbers': order_numbers
        })
        document = render_to_string('soap1s/cancel_order.xml', context)
        soap_log = SoapLog.create_request_log(document, SoapLog.TYPE_CANCEL_ORDER)
        result = self.request(document)
        soap_log.response = result
        soap_log.save()
        data = xmltodict.parse(result)
        success = False
        orders = []
        resp_orders = data['ФайлОбмена']['Заказы']['Заказ']
        if not isinstance(resp_orders, list):
                resp_orders = [resp_orders]
        for part in resp_orders:
            success = True
            orders.append({
                'order_number': part['@НомерЗаказа'],
                'comment': part['@Комментарий'],
            })
        return {
            'success': success,
            'orders': orders,
        }

    def cancel_order_item(self, order_list):
        '''
        CancelOrderArticle - cancel order article with provided article_list
        order_list is a list with dicts in such format:
        [
            {
                'order_number': 'ORDER_NUMBER',
                'article': 'ARTICLE'
            }
        ]
        '''
        context = self.get_base_xml_context()
        context.update({
            'order_list': order_list
        })
        document = render_to_string('soap1s/cancel_order_item.xml', context)
        soap_log = SoapLog.create_request_log(document, SoapLog.TYPE_CANCEL_ORDER_ITEM)
        result = self.request(document)
        soap_log.response = result
        soap_log.save()
        data = xmltodict.parse(result)
        success = False
        orders = []
        resp_orders = data['ФайлОбмена']['Заказы']['Заказ']
        if not isinstance(resp_orders, list):
                resp_orders = [resp_orders]
        for part in resp_orders:
            if part['@Комментарий'] != 'Заказ не найден в базе':
                success = True
                orders.append({
                    'order_number': part['@НомерЗаказа'],
                    'article': part['@Артикул'],
                    'comment': part['@Комментарий'],
                })
        return {
            'success': success,
            'orders': orders,
        }

    def request(self, document):
        client = self.get_client()
        return client.service.GetOrder(document)

    def get_client(self):
        if self.client is None:
            self.client = SoapClient(
                self.url,
                username=self.username,
                password=self.password,
                headers={
                    'SOAPAction': 'http://www.w3.org/2001/XMLSchema)#WebTest:GetOrder',
                },
                timeout=300,
            )
        return self.client

    def get_base_xml_context(self):
        return {
            'api_key': self.api_key,
        }
