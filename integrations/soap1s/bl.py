from .client import Client


def make_order(url, username, password, api_key, order):
    client = Client(url, username, password, api_key)
    return client.confirm_order(order)


def get_parts_availability(url, username, password, api_key, articles):
    client = Client(url, username, password, api_key)
    return client.get_sheet(articles)


def cancel_order(url, username, password, api_key, order_numbers):
    client = Client(url, username, password, api_key)
    return client.cancel_order(order_numbers)


def cancel_order_item(url, username, password, api_key, order_list):
    client = Client(url, username, password, api_key)
    return client.cancel_order_item(order_list)
