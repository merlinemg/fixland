import os

from django.test import Client
from django.test import TestCase
from django.core.urlresolvers import reverse

from devices.factories import VendorFactory

BASE = os.path.dirname(os.path.realpath(__file__))


class Soap1SCallback(TestCase):
    def test_should_return_400_for_bad_api_key(self):
        client = Client()
        data = {
            'rwID': open('{}/data.xml'.format(BASE)).read()
        }
        resp = client.post(reverse('soap1s_callback'), data)
        self.assertEqual(resp.status_code, 400)

    def test_success(self):
        VendorFactory()
        client = Client()
        data = {
            'rwID': open('{}/data.xml'.format(BASE)).read()
        }
        resp = client.post(reverse('soap1s_callback'), data)
        self.assertEqual(resp.status_code, 200)
