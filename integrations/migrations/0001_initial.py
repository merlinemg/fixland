# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SoapLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('request', models.TextField(null=True, blank=True)),
                ('response', models.TextField(null=True, blank=True)),
                ('type', models.CharField(max_length=255, choices=[('confirm_order', 'confirm order'), ('cancel_order', 'cancel order'), ('cancel_order_item', 'cancel order item'), ('callback', 'callback')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Soap log',
            },
        ),
    ]
