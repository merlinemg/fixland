from django.contrib import admin

from .models import SoapLog


class SoapLogAdmin(admin.ModelAdmin):
    list_display = ('_created_at', 'type', )
    readonly_fields = ('_created_at', '_updated_at', )
    date_hierarchy = 'created_at'

    def _created_at(self, obj):
        return obj.created_at.strftime("%d %b %Y %H:%M:%S")

    def _updated_at(self, obj):
        return obj.updated_at.strftime("%d %b %Y %H:%M:%S")


admin.site.register(SoapLog, SoapLogAdmin)
