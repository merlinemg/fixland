import requests
import json
import sys

from git import Repo


def notify_newrelic(target):
    room_id = '1628407'
    release = get_release_name() or target
    revision = get_revision() or target
    url = 'https://api.hipchat.com/v2/room/{}/notification?auth_token=7scvsZoa1HP1Mf5mSCQBx0SMbd3zDcJgtxByVnpO'.format(room_id)
    headers = {'Content-Type': 'application/json'}
    data = {
        'color': 'green',
        'message': 'Release <strong>{release}</strong> deployed to <strong>{target}</strong>. (Revision: <a href="http://gitlab.lyapun.com/hq/fixland/commit/{revision}">{revision}</a>)'.format(
            release=release,
            target=target,
            revision=revision,
        ),
        'notify': True,
        'message_format': 'html'
    }
    requests.post(url, data=json.dumps(data), headers=headers).content


def get_release_name():
    try:
        repo = get_repo()
        return repo.active_branch.name
    except Exception:
        return None


def get_revision():
    try:
        repo = get_repo()
        return repo.head.commit.hexsha
    except Exception:
        return None


def get_repo():
    return Repo('.')


if __name__ == '__main__':
    notify_newrelic(sys.argv[1])
